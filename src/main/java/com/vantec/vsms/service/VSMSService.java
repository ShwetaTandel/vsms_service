package com.vantec.vsms.service;

import org.springframework.stereotype.Service;

import com.vantec.vsms.model.ActionModel;
import com.vantec.vsms.model.ApproverModel;
import com.vantec.vsms.model.HazardModel;
import com.vantec.vsms.model.IncidentModel;
import com.vantec.vsms.model.RiskAssessmentModel;
import com.vantec.vsms.model.RiskHazardModel;
import com.vantec.vsms.model.SiteModel;
import com.vantec.vsms.model.UserModel;

@Service
public interface VSMSService {

	public String testService();
	public String saveIncident(IncidentModel reqIncident);
	public String saveUser( UserModel  userReq);
	public String addApprovers(ApproverModel req);
	public String addRecipients(ApproverModel req);
	public String saveSite(SiteModel req);
	public String rejectIncident(Long incidentNumber, Long approverId, String comments,  Long prevApproverId);
	public String deleteIncident(Long incidentNumber, Long requestorId, String comments);
	public String saveAction(ActionModel reqAction);
	public String changeApprover(ApproverModel request);
	public String deleteHazard(Long hazardNumber,Long requestorId, String comments);
	public String saveHazard(HazardModel reqHazard);
	public String saveRiskHazard(RiskHazardModel reqHazard);
	public String saveRiskAssessment(RiskAssessmentModel reqHazard);
	public void updateDeadlineAndTime(Long incidentNumber);
	public String deleteRiskHazard(Long hazardId,Long requestorId);
	public String newVersionRiskAssessment(Long graId, Long userId);
	public String deleteRiskAssessment(Long graId, Long userId);
	public boolean authenticateToken(String ipAddress, Long siteId);
	
}
