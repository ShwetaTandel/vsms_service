package com.vantec.vsms.service;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.vantec.vsms.entity.Action;
import com.vantec.vsms.entity.ActionHistory;
import com.vantec.vsms.entity.Approvers;
import com.vantec.vsms.entity.GRAHistory;
import com.vantec.vsms.entity.GRAStatus;
import com.vantec.vsms.entity.Hazard;
import com.vantec.vsms.entity.HazardHistory;
import com.vantec.vsms.entity.HazardTabIPs;
import com.vantec.vsms.entity.Incident;
import com.vantec.vsms.entity.IncidentDeadlines;
import com.vantec.vsms.entity.IncidentFullInvestigationDetail;
import com.vantec.vsms.entity.IncidentFullInvestigationWhyDetails;
import com.vantec.vsms.entity.IncidentHistory;
import com.vantec.vsms.entity.IncidentInjuredPersonDetail;
import com.vantec.vsms.entity.IncidentPersonDetail;
import com.vantec.vsms.entity.IncidentPersonLicense;
import com.vantec.vsms.entity.Recipients;
import com.vantec.vsms.entity.RiskAssessment;
import com.vantec.vsms.entity.RiskHazard;
import com.vantec.vsms.entity.Site;
import com.vantec.vsms.entity.UserSite;
import com.vantec.vsms.entity.Users;
import com.vantec.vsms.model.ActionModel;
import com.vantec.vsms.model.ApproverModel;
import com.vantec.vsms.model.FIDetailsModel;
import com.vantec.vsms.model.HazardModel;
import com.vantec.vsms.model.IncidentFullInvestigationWhyModel;
import com.vantec.vsms.model.IncidentInjuredPersonModel;
import com.vantec.vsms.model.IncidentLicenseModel;
import com.vantec.vsms.model.IncidentModel;
import com.vantec.vsms.model.IncidentPersonModel;
import com.vantec.vsms.model.RiskAssessmentModel;
import com.vantec.vsms.model.RiskHazardModel;
import com.vantec.vsms.model.SiteModel;
import com.vantec.vsms.model.UserModel;
import com.vantec.vsms.repository.ActionHistoryRepository;
import com.vantec.vsms.repository.ActionRepository;
import com.vantec.vsms.repository.ApproversRepository;
import com.vantec.vsms.repository.GRAHistoryRepository;
import com.vantec.vsms.repository.GRAStatusRepository;
import com.vantec.vsms.repository.HazardHistoryRepository;
import com.vantec.vsms.repository.HazardRepository;
import com.vantec.vsms.repository.HazardTabIPSRepository;
import com.vantec.vsms.repository.IncidentDeadlinesRepository;
import com.vantec.vsms.repository.IncidentFullInvestigationDetailRepository;
import com.vantec.vsms.repository.IncidentFullInvestigationWhyDetailRepository;
import com.vantec.vsms.repository.IncidentHistoryRepository;
import com.vantec.vsms.repository.IncidentInjuredPersonDetailRepository;
import com.vantec.vsms.repository.IncidentInjuredPersonRIDDORFileRepository;
import com.vantec.vsms.repository.IncidentPersonDAFileRepository;
import com.vantec.vsms.repository.IncidentPersonDetailRepository;
import com.vantec.vsms.repository.IncidentPersonLicenseRepository;
import com.vantec.vsms.repository.IncidentRepository;
import com.vantec.vsms.repository.RecipientRepository;
import com.vantec.vsms.repository.RiskAssessmentRepository;
import com.vantec.vsms.repository.RiskHazardRepository;
import com.vantec.vsms.repository.SiteRepository;
import com.vantec.vsms.repository.UsersRepository;
import com.vantec.vsms.repository.UsersSitesRepository;
import com.vantec.vsms.util.SecurityToken;

@Service("VSMSService")
public class VSMSServiceImpl implements VSMSService {
	private static Logger logger = LoggerFactory.getLogger(VSMSServiceImpl.class);

	@Autowired
	private IncidentRepository incidentRepo;
	@Autowired
	private IncidentDeadlinesRepository incidentDeadlinesRepo;

	@Autowired
	private UsersRepository userRepo;
	@Autowired
	private UsersSitesRepository userSiteRepo;

	@Autowired
	private ApproversRepository approversRepo;

	@Autowired
	private RecipientRepository recipientsRepo;

	@Autowired
	private SiteRepository siteRepo;

	@Autowired
	private IncidentHistoryRepository incidentHistRepo;

	@Autowired
	private ActionHistoryRepository actionHistRepo;

	@Autowired
	private ActionRepository actionRepo;

	@Autowired
	private IncidentPersonDetailRepository incidentPersonDetailRepo;

	@Autowired
	private IncidentPersonLicenseRepository incidentPersonLicenseRepo;

	

	@Autowired
	private IncidentPersonDAFileRepository incidentPersonDAFileRepo;

	@Autowired
	private IncidentFullInvestigationDetailRepository incidentFullInvestigationDetailRepo;

	@Autowired
	private IncidentFullInvestigationWhyDetailRepository incidentFullInvestigationWhyDetailRepo;

	@Autowired
	private IncidentInjuredPersonDetailRepository incidentInjuredPersonRepo;

	@Autowired
	private IncidentInjuredPersonRIDDORFileRepository incidentInjuredPersonRIDDORFileRepo;

	@Autowired
	private HazardHistoryRepository hazardHistRepo;

	@Autowired
	private HazardRepository hazardRepo;

	@Autowired
	private RiskHazardRepository riskHazardRepo;
	
	@Autowired
	private RiskAssessmentRepository riskAssessmentRepo;
	
	@Autowired
	private GRAStatusRepository graStatusRepo;
	
	@Autowired
	private GRAHistoryRepository graHistRepo;
	
	@Autowired
	private HazardTabIPSRepository hazardTabRepo;

	public String testService() {
		return "Test Succeeded";
	}

	@Transactional
	public String saveIncident(IncidentModel reqIncident) {

		Incident newIncident = new Incident();

		Users requestor = userRepo.findById(reqIncident.getRequestorId());
		String requestorName = requestor.getFirstName() + " " + requestor.getLastName();
		boolean isL1Save = false, isL2Submit = false, isNew = false, isL2Edit = false, isL2Approve = false,
				isL3Save = false, isL3Submit = false, isL3Edit = false, isL3Approve = false, isL4Save = false,
				isL4Approve = false;

		if (reqIncident.getFilter().equals("L1_SAVED")) {
			isL1Save = true;
		} else if (reqIncident.getFilter().equals("L2_SUBMITTED")) {
			isL2Submit = true;
		} else if (reqIncident.getFilter().equals("NEW")) {
			isNew = true;
		} else if (reqIncident.getFilter().equals("L2_EDITED")) {
			isL2Edit = true;
		} else if (reqIncident.getFilter().equals("L2_APPROVED")) {
			isL2Approve = true;
		} else if (reqIncident.getFilter().equals("L3_SAVED")) {
			isL3Save = true;
		} else if (reqIncident.getFilter().equals("L3_SUBMITTED")) {
			isL3Submit = true;
		} else if (reqIncident.getFilter().equals("L3_EDITED")) {
			isL3Edit = true;
		} else if (reqIncident.getFilter().equals("L3_APPROVED")) {
			isL3Approve = true;
		} else if (reqIncident.getFilter().equals("L4_SAVED")) {
			isL4Save = true;
		} else if (reqIncident.getFilter().equals("L4_APPROVED")) {
			isL4Approve = true;
		}

		logger.info("Reached here" + reqIncident.getFilter());

		if (!isNew) {
			newIncident = incidentRepo.findOne(reqIncident.getIncidentNumber());
			incidentPersonDetailRepo.delete(newIncident.getPersonInvolved());
			incidentPersonDAFileRepo
					.delete(incidentPersonDAFileRepo.findByIncidentNumber(reqIncident.getIncidentNumber()));

		}

		newIncident.setSiteId(reqIncident.getSiteId());
		newIncident.setLocation(reqIncident.getLocation());
		newIncident.setLocationOther(reqIncident.getLocationOther());
		newIncident.setIncidentDateTime(reqIncident.getIncidentDateTime());
		newIncident.setIncidentType(reqIncident.getIncidentType());
		newIncident.setEventSeverity(reqIncident.getEventSeverity());
		newIncident.setIncidentBrief(reqIncident.getIncidentBrief());
		newIncident.setQuestion1Answer(reqIncident.getQuestion1Answer());
		newIncident.setQuestion1Text(reqIncident.getQuestion1Text());
		newIncident.setQuestion2Answer(reqIncident.getQuestion2Answer());
		newIncident.setQuestion2Text(reqIncident.getQuestion2Text());
		newIncident.setQuestion3Answer(reqIncident.getQuestion3Answer());
		newIncident.setQuestion3Text(reqIncident.getQuestion3Text());
		newIncident.setQuestion4Answer(reqIncident.getQuestion4Answer());
		newIncident.setQuestion4Text(reqIncident.getQuestion4Text());
		newIncident.setCategory(reqIncident.getCategory());
		newIncident.setCategoryLevel(reqIncident.getCategoryLevel());
		newIncident.setType(reqIncident.getType());
		newIncident.setActOrCondition(reqIncident.getActOrCondition());
		newIncident.setIsDangerousOccurence(reqIncident.getIsDangerousOccurence());
		newIncident.setHsSummary(reqIncident.getHsSummary());
		newIncident.setClassification(reqIncident.getClassification());

		// UPDATE the Initial Report fields
		if (!isNew) {
			newIncident.setIncidenDetail(reqIncident.getIncidentDetail());
			newIncident.setActionType(reqIncident.getActionType());
			newIncident.setActionTypeOther(reqIncident.getActionTypeOther());
			newIncident.setArea(reqIncident.getArea());
			newIncident.setLighting(reqIncident.getLighting());
			newIncident.setTemperature(reqIncident.getTemperature());
			newIncident.setNoise(reqIncident.getNoise());
			newIncident.setSurfaceCondition(reqIncident.getSurfaceCondition());
			newIncident.setSurfaceType(reqIncident.getSurfaceType());
			newIncident.setIsActivityStandardProcess(reqIncident.getIsActivityStandardProcess());
			newIncident.setStillageType(reqIncident.getStillageType());
			newIncident.setEstimatedCost(reqIncident.getEstimatedCost());
			newIncident.setImmediateCause(reqIncident.getImmediateCause());
			newIncident.setKeyPoints(reqIncident.getKeyPoints());
			newIncident.setMainMessage(reqIncident.getMainMessage());
			newIncident.setGraId(reqIncident.getGraId());
			newIncident.setEstimatedCostOther(reqIncident.getEstimatedCostOther());
			if (!isL3Edit) {
				// Is its L3 EDIT don't overwrite the approver
				newIncident.setCurrApproverId(reqIncident.getCurrApproverId());
			}
			newIncident.setIsPersonInvolved(reqIncident.getIsPersonInvolved());

		}
		if (isNew) {
			newIncident.setStatus("L1_CREATED");
			newIncident.setCreatedAt(new Date());
			newIncident.setCreatedBy(requestorName);
			newIncident.setCreatorUserId(requestor.getId());
		} else if (isL2Submit) {
			newIncident.setStatus("L2_SUBMITTED");
			newIncident.setUpdatedAt(new Date());
			newIncident.setUpdatedBy(requestorName);

		} else if (isL2Edit) {
			newIncident.setStatus("L2_EDITED");
			newIncident.setUpdatedAt(new Date());
			newIncident.setUpdatedBy(requestorName);
		} else if (isL1Save) {
			newIncident.setStatus("L1_SAVED");
			newIncident.setUpdatedAt(new Date());
			newIncident.setUpdatedBy(requestorName);
		} else if (isL2Approve) {
			newIncident.setStatus("L2_APPROVED");
			newIncident.setUpdatedAt(new Date());
			newIncident.setUpdatedBy(requestorName);
		} else if (isL3Save) {
			newIncident.setStatus("L3_SAVED");
			newIncident.setUpdatedAt(new Date());
			newIncident.setUpdatedBy(requestorName);
		} else if (isL3Submit) {
			newIncident.setStatus("L3_SUBMITTED");
			newIncident.setUpdatedAt(new Date());
			newIncident.setUpdatedBy(requestorName);
		} else if (isL3Edit) {
			newIncident.setStatus("L3_EDITED");
			newIncident.setUpdatedAt(new Date());
			newIncident.setUpdatedBy(requestorName);
		} else if (isL3Approve) {
			newIncident.setStatus("L3_APPROVED");
			newIncident.setUpdatedAt(new Date());
			newIncident.setUpdatedBy(requestorName);
		} else if (isL4Save) {
			newIncident.setStatus("L4_SAVED");
			newIncident.setUpdatedAt(new Date());
			newIncident.setUpdatedBy(requestorName);
		} else if (isL4Approve) {
			newIncident.setStatus("L4_APPROVED");
			newIncident.setUpdatedAt(new Date());
			newIncident.setUpdatedBy(requestorName);
		}

		newIncident.setUpdatedAt(new Date());
		newIncident.setUpdatedBy(requestorName);
		Incident savedIncident = incidentRepo.save(newIncident);
		if (!isNew) {
			// Add person Details
			if (reqIncident.getIsPersonInvolved() != null && reqIncident.getIsPersonInvolved()) {
				for (IncidentPersonModel person : reqIncident.getPersonInvolved()) {
					IncidentPersonDetail newPersonDtl = new IncidentPersonDetail();
					newPersonDtl.setPersonName(person.getPersonName().trim());
					newPersonDtl.setPersonDOB(person.getPersonDOB());
					newPersonDtl.setEmploymentStatus(person.getEmploymentStatus());
					newPersonDtl.setEmploymentstartDate(person.getEmploymentstartDate());
					newPersonDtl.setTimeOnJobStartDate(person.getTimeOnJobStartDate());
					newPersonDtl.setAnyPrevIncidents(person.getAnyPrevIncidents());
					newPersonDtl.setPrevIncidentDateReference(person.getPrevIncidentDateReference());
					newPersonDtl.setIsVehicleInvolved(person.getIsVehicleInvolved());
					newPersonDtl.setVehicleType(person.getVehicleType());
					newPersonDtl.setVehicleTypeOther(person.getVehicleTypeOther());
					newPersonDtl.setVehicleRegNo(person.getVehicleRegNo());
					newPersonDtl.setWasPersonremovedFromTruck(person.getWasPersonremovedFromTruck());
					newPersonDtl.setLocationOfInjury(person.getLocationOfInjury());
					newPersonDtl.setIsInjuryInvolved(person.getIsInjuryInvolved());
					newPersonDtl.setTreatementDetails(person.getTreatementDetails());
					newPersonDtl.setFirstAiderName(person.getFirstAiderName());
					newPersonDtl.setPpeWorn(person.getPpeWorn());
					newPersonDtl.setWasPPEOfReqLevel(person.getWasPPEOfReqLevel());
					newPersonDtl.setTreatmentAdministered(person.getTreatmentAdministered());
					newPersonDtl.setDaTestRequired(person.getDaTestRequired());
					newPersonDtl.setDaTestNotReqReason(person.getDaTestNotReqReason());
					newPersonDtl.setDaTestAgreed(person.getDaTestAgreed());
					newPersonDtl.setDaTestedBy(person.getDaTestedBy());
					newPersonDtl.setDaPassed(person.getDaPassed());
					newPersonDtl.setUpdatedAt(new Date());
					newPersonDtl.setUpdatedBy(requestorName);
					newPersonDtl.setUpdatedByUserId(requestor.getId());
					savedIncident.getPersonInvolved().add(newPersonDtl);
					newPersonDtl.setIncident(savedIncident);
					IncidentLicenseModel license = reqIncident.getPersonLicense(person.getPersonName());
					if (license != null) {
						newPersonDtl.setLicenseExpDate(license.getLicenseExpiryDate());
						newPersonDtl.setIsLicenseValid(license.getIsLicenseValid());
					}

					incidentPersonDetailRepo.save(newPersonDtl);
				}
			}
			if (isL1Save || isL2Submit) {
				// delete the Containment actions which are in DB but not in
				// request
				List<Action> dbContainementActions = actionRepo
						.findContainmentActionsByIncident(savedIncident.getIncidentNumber());

				for (Action dbContainment : dbContainementActions) {
					boolean isRecordDeleted = true;
					for (ActionModel action : reqIncident.getActions()) {
						if (action.getId().longValue() == dbContainment.getId().longValue()) {
							isRecordDeleted = false;
						}

					}
					if (isRecordDeleted) {
						actionRepo.delete(dbContainment.getId());
						logger.info("containment action deleted for id " + dbContainment.getId());
					}

				}
				// Add Action details
				for (ActionModel action : reqIncident.getActions()) {
					Action newAction = new Action();
					if (action.getId() != 0) {
						newAction = actionRepo.findOne(action.getId());

					}
					newAction.setAction(action.getAction());
					newAction.setActionOwner(action.getActionOwner());
					newAction.setActionType("Incident-Containment");
					newAction.setDeadline(action.getDeadline());
					newAction.setEstimatedCompletionDate(action.getDeadline());
					newAction.setActionSubType("");
					newAction.setActionSite(action.getActionSite());
					String oldStatus = newAction.getStatus();
					if (oldStatus == null) {
						oldStatus = "";
					}
					logger.info("action id is AC" + action.getId());
					newAction.setStatus(
							getActionStatus(savedIncident.getStatus(), action.getId() == 0, newAction.getStatus()));
					if (newAction.getStatus().equals("ASSIGNED") && oldStatus != null
							&& !oldStatus.equals(newAction.getStatus())) {
						newAction.setAssignedAt(new Date());
						newAction.setAssignedBy(requestorName);

					}
					newAction.setUpdatedAt(new Date());
					newAction.setUpdatedBy(requestorName);
					newAction.setUpdatedByUserId(requestor.getId());
					// savedIncident.getActions().add(newAction);
					newAction.setReportId(savedIncident.getIncidentNumber());
					newAction.setActionSource("Incident");
					Action savedAction = actionRepo.save(newAction);

					ActionHistory actionHist = new ActionHistory();
					actionHist.setComments("Action " + savedAction.getStatus());
					actionHist.setStatus(savedAction.getStatus());
					actionHist.setReportId(savedIncident.getIncidentNumber());
					actionHist.setActionSource("Incident");
					actionHist.setActionNumber(savedAction.getId());
					actionHist.setUpdatedAt(new Date());
					actionHist.setLastUpdatedBy(requestorName);
					actionHist.setUpdatedByUserId(requestor.getId());
					actionHist.setActionOwnerId(savedAction.getActionOwner());
					actionHistRepo.save(actionHist);
				}
			}
			if (reqIncident.getIsPersonInvolved() != null && !reqIncident.getIsPersonInvolved()) {
				incidentPersonLicenseRepo.delete(incidentPersonLicenseRepo.findAllByIncident(savedIncident));
			} else {
				// Delete the the removed licensed details
				for (Long licenseId : reqIncident.getDeletedLicenseIds()) {
					if (licenseId != 0) {
						incidentPersonLicenseRepo.delete(licenseId);
					}
				}
				// Add license details
				for (IncidentLicenseModel license : reqIncident.getLicenseDetails()) {
					IncidentPersonLicense newLicense = new IncidentPersonLicense();
					if (license.getLicenseId() != 0) {
						newLicense = incidentPersonLicenseRepo.findOne(license.getLicenseId());
					}
					newLicense.setPerson(incidentPersonDetailRepo.findByPersonNameAndIncident(license.getPersonName(),
							savedIncident));
					newLicense.setIncident(savedIncident);
					newLicense.setExpiryDate(license.getLicenseExpiryDate());
					newLicense.setIsValid(license.getIsLicenseValid());
					if (!license.getFileName().equals("")) {
						newLicense.setFileName(license.getFileName());
						;
					}
					incidentPersonLicenseRepo.save(newLicense);

				}
			}

		}
		// Add Full Investigation details
		if (isL3Save || isL3Submit || isL3Edit || isL3Approve || isL4Save || isL4Approve) {

			IncidentFullInvestigationDetail fiDetail = new IncidentFullInvestigationDetail();
			FIDetailsModel fiDetailReq = reqIncident.getFiDetails();
			if (fiDetailReq != null) {
				if (fiDetailReq.getId() != null && fiDetailReq.getId().longValue() != 0) {
					fiDetail = incidentFullInvestigationDetailRepo.findOne(fiDetailReq.getId());
				}
				fiDetail.setIncident(savedIncident);
				fiDetail.setEventSummary(fiDetailReq.getEventSummary());
				fiDetail.setIsSSOWSuitable(fiDetailReq.getIsSSOWSuitable());
				fiDetail.setIsMHETrainingRequired(fiDetailReq.getIsMHETrainingRequired());
				fiDetail.setIsRASuitable(fiDetailReq.getIsRASuitable());
				fiDetail.setRaComments(fiDetailReq.getRaComments());
				fiDetail.setSsowComments(fiDetailReq.getSsowComments());
				fiDetail.setPersonConcentraion(fiDetailReq.getPersonConcentraion());
				fiDetail.setPersonExperience(fiDetailReq.getPersonExperience());
				fiDetail.setPersonTraining(fiDetailReq.getPersonTraining());
				fiDetail.setMachineCondition(fiDetailReq.getMachineCondition());
				fiDetail.setMachineFacility(fiDetailReq.getMachineFacility());
				fiDetail.setMachineMaintenance(fiDetailReq.getMachineMaintenance());
				fiDetail.setMethodRiskAssessment(fiDetailReq.getMethodRiskAssessment());
				fiDetail.setMethodRuling(fiDetailReq.getMethodRuling());
				fiDetail.setMethodSSOW(fiDetailReq.getMethodSSOW());
				fiDetail.setEnvironmentCondition(fiDetailReq.getEnvironmentCondition());
				fiDetail.setEnvironmentFloor(fiDetailReq.getEnvironmentFloor());
				fiDetail.setEnvironmentLighting(fiDetailReq.getEnvironmentLighting());
				fiDetail.setVantecWayDecision(fiDetailReq.getVantecWayDecision());
				fiDetail.setVantecWayComments(fiDetailReq.getVantecWayComments());
				fiDetail.setVantecWayDecisionTree(fiDetailReq.getVantecWayDecisionTree());
				fiDetail.setInvestigationTeamNames(fiDetailReq.getInvestigationTeamNames());

				incidentFullInvestigationDetailRepo.save(fiDetail);
				// delete the one which are present in DB but not in request
				List<IncidentFullInvestigationWhyDetails> dbWhys = incidentFullInvestigationWhyDetailRepo
						.findByIncident(savedIncident);
				for (IncidentFullInvestigationWhyDetails dbWhy : dbWhys) {
					boolean recordDeleted = true;
					for (IncidentFullInvestigationWhyModel model : fiDetailReq.getWhyModels()) {
						if (dbWhy.getId().longValue() == model.getId().longValue()) {
							recordDeleted = false;
						}
					}
					if (recordDeleted) {
						List<Action> actionsToDel = actionRepo.findActionsByWhyId(savedIncident.getIncidentNumber(),
								dbWhy.getId());
						actionRepo.delete(actionsToDel);
						logger.info("Actions deleted for  " + dbWhy.getId());
						incidentFullInvestigationWhyDetailRepo.delete(dbWhy.getId());

						logger.info("why deleted for id " + dbWhy.getId());
					}
				}

				for (IncidentFullInvestigationWhyModel model : fiDetailReq.getWhyModels()) {
					IncidentFullInvestigationWhyDetails detail = new IncidentFullInvestigationWhyDetails();
					if (model != null && model.getId() != 0) {
						detail = incidentFullInvestigationWhyDetailRepo.findOne(model.getId());
					}

					detail.setWhy1(model.getWhy1());
					detail.setWhy2(model.getWhy2());
					detail.setWhy3(model.getWhy3());
					detail.setWhy4(model.getWhy4());
					detail.setWhy5(model.getWhy5());

					detail.setRootCauseA(model.getRootCauseA());
					detail.setRootCauseB(model.getRootCauseB());
					detail.setRootCauseC(model.getRootCauseC());
					detail.setIncident(savedIncident);

					IncidentFullInvestigationWhyDetails savedDetail = incidentFullInvestigationWhyDetailRepo
							.save(detail);

					for (ActionModel reqMeasure : model.getCounterMeasures()) {
						Action counterMeasure = new Action();
						String prevStatus = "";
						if (reqMeasure.getId() != 0) {
							counterMeasure = actionRepo.findOne(reqMeasure.getId());
							prevStatus = counterMeasure.getStatus();
						}
						if (reqMeasure.getAction().equals("")) {
							counterMeasure.setAction("");
							counterMeasure.setActionOwner(new Long(-1));
							counterMeasure.setActionSite(new Long(-1));
							counterMeasure.setActionType("");
							counterMeasure.setActionSubType("");
							counterMeasure.setStatus("");
							counterMeasure.setDeadline(null);
						} else {
							counterMeasure.setAction(reqMeasure.getAction());
							counterMeasure.setActionOwner(reqMeasure.getActionOwner());
							counterMeasure.setActionSite(reqMeasure.getActionSite());
							counterMeasure.setActionType("Incident-Countermeasure");
							logger.info("action id is AC" + reqMeasure.getId());
							counterMeasure.setStatus(
									getActionStatus(savedIncident.getStatus(), prevStatus.equals(""), prevStatus));
							if (counterMeasure.getStatus().equals("ASSIGNED")
									&& (!prevStatus.equals(counterMeasure.getStatus()))) {
								counterMeasure.setAssignedAt(new Date());
								counterMeasure.setAssignedBy(requestorName);
							}
							counterMeasure.setActionSubType(reqMeasure.getActionSubType());
							counterMeasure.setDeadline(reqMeasure.getDeadline());
							counterMeasure.setEstimatedCompletionDate(reqMeasure.getDeadline());
						}

						// counterMeasure.setIncident(savedIncident);
						counterMeasure.setReportId(savedIncident.getIncidentNumber());
						counterMeasure.setActionSource("Incident");
						counterMeasure.setCounterMeasureNumber(reqMeasure.getCounterMeasureNumber());
						counterMeasure.setReportSubId(savedDetail.getId());
						counterMeasure.setUpdatedAt(new Date());
						counterMeasure.setUpdatedBy(requestorName);
						counterMeasure.setUpdatedByUserId(requestor.getId());
						// savedIncident.getActions().add(counterMeasure);
						if (reqMeasure.isAddHistory()) {
							Action savedAction = actionRepo.save(counterMeasure);
							ActionHistory actionHist = new ActionHistory();
							if (counterMeasure.getStatus().equals("")) {
								actionHist.setComments("Action SAVED");
								actionHist.setStatus("SAVED");
							} else {
								actionHist.setComments("Action " + counterMeasure.getStatus());
								actionHist.setStatus(counterMeasure.getStatus());
							}

							actionHist.setReportId(savedIncident.getIncidentNumber());
							actionHist.setActionSource("Incident");

							actionHist.setActionNumber(savedAction.getId());
							actionHist.setUpdatedAt(new Date());
							actionHist.setLastUpdatedBy(requestorName);
							actionHist.setUpdatedByUserId(requestor.getId());
							actionHist.setActionOwnerId(savedAction.getActionOwner());
							actionHistRepo.save(actionHist);
						}
					}
				}
				// delete the one which are present in DB but not in request

				List<Action> dbOtherMeasures = actionRepo
						.findOtherCounterMeasuresByIncident(savedIncident.getIncidentNumber());
				for (Action dbOtherMeasure : dbOtherMeasures) {
					boolean recordDeleted = true;
					for (ActionModel reqMeasure : fiDetailReq.getOtherCounterMeasures()) {
						if (reqMeasure.getId().longValue() == dbOtherMeasure.getId().longValue()) {
							recordDeleted = false;

						}

					}
					if (recordDeleted) {
						actionRepo.delete(dbOtherMeasure.getId());
						logger.info("Other measure old action delte for it" + dbOtherMeasure.getId());
					}
				}

				for (ActionModel reqMeasure : fiDetailReq.getOtherCounterMeasures()) {
					Action counterMeasure = new Action();
					String prevStatus = "";
					if (reqMeasure.getId() != 0) {
						counterMeasure = actionRepo.findOne(reqMeasure.getId());
						prevStatus = counterMeasure.getStatus();
					}
					if (reqMeasure.getAction().equals("") == false) {
						counterMeasure.setAction(reqMeasure.getAction());
						counterMeasure.setActionOwner(reqMeasure.getActionOwner());
						counterMeasure.setActionSite(reqMeasure.getActionSite());
						counterMeasure.setActionType("Incident-Countermeasure");
						counterMeasure.setActionSubType(reqMeasure.getActionSubType());
						counterMeasure.setDeadline(reqMeasure.getDeadline());
						counterMeasure.setEstimatedCompletionDate(reqMeasure.getDeadline());
						logger.info("action id is AC" + reqMeasure.getId());
						counterMeasure.setStatus(
								getActionStatus(savedIncident.getStatus(), prevStatus.equals(""), prevStatus));
						if (counterMeasure.getStatus().equals("ASSIGNED")
								&& (!prevStatus.equals(counterMeasure.getStatus()))) {
							counterMeasure.setAssignedAt(new Date());
							counterMeasure.setAssignedBy(requestorName);
						}
						// counterMeasure.setIncident(savedIncident);
						counterMeasure.setReportId(savedIncident.getIncidentNumber());
						counterMeasure.setActionSource("Incident");
						counterMeasure.setCounterMeasureNumber(null);
						counterMeasure.setReportSubId(null);

						counterMeasure.setUpdatedAt(new Date());

						counterMeasure.setUpdatedBy(requestorName);
						counterMeasure.setUpdatedByUserId(requestor.getId());
						// savedIncident.getActions().add(counterMeasure);
						Action savedAction = actionRepo.save(counterMeasure);

						if (reqMeasure.isAddHistory()) {
							ActionHistory actionHist = new ActionHistory();
							actionHist.setComments("Action " + counterMeasure.getStatus());
							actionHist.setStatus(counterMeasure.getStatus());
							actionHist.setReportId(savedIncident.getIncidentNumber());
							actionHist.setActionSource("Incident");

							actionHist.setActionNumber(savedAction.getId());
							actionHist.setUpdatedAt(new Date());
							actionHist.setLastUpdatedBy(requestorName);
							actionHist.setUpdatedByUserId(requestor.getId());
							actionHist.setActionOwnerId(savedAction.getActionOwner());
							actionHistRepo.save(actionHist);
						}
					} else if (reqMeasure.getAction().equals("")) {
						counterMeasure.setAction("");
						counterMeasure.setActionOwner(new Long(-1));
						counterMeasure.setActionSite(new Long(-1));
						counterMeasure.setActionType("");
						counterMeasure.setActionSubType("");
						counterMeasure.setStatus("");
						counterMeasure.setDeadline(null);
					}
				}
			}

		}
		// Add Health and Safety details
		if (isL4Save || isL4Approve) {
			incidentInjuredPersonRIDDORFileRepo
					.delete(incidentInjuredPersonRIDDORFileRepo.findByIncidentNumber(reqIncident.getIncidentNumber()));
			// delete the one which are present in DB but not in request
			List<IncidentInjuredPersonDetail> dbInjuredPersons = incidentInjuredPersonRepo
					.findByIncident(savedIncident);
			for (IncidentInjuredPersonDetail dbInjuredPerson : dbInjuredPersons) {
				boolean recordDeleted = true;
				for (IncidentInjuredPersonModel model : reqIncident.getInjuredPersons()) {
					if (dbInjuredPerson.getInjuredPersonId().longValue() == model.getInjuredPersonId().longValue()) {
						recordDeleted = false;
					}
				}
				if (recordDeleted) {
					incidentInjuredPersonRepo.delete(dbInjuredPerson.getInjuredPersonId());
				}
			}
			for (IncidentInjuredPersonModel injuredPerson : reqIncident.getInjuredPersons()) {
				IncidentInjuredPersonDetail detail = new IncidentInjuredPersonDetail();
				if (injuredPerson != null && injuredPerson.getInjuredPersonId() != null
						&& injuredPerson.getInjuredPersonId() != 0) {
					detail = incidentInjuredPersonRepo.findOne(injuredPerson.getInjuredPersonId());
				}
				detail.setInjuredPersonName(injuredPerson.getInjuredPersonName());
				detail.setDetailsAndTreatment(injuredPerson.getTreatmentAndDetails());
				detail.setDaysLost(injuredPerson.getDaysLost());

				detail.setIsRIDDOR(injuredPerson.getIsRIDDOR());
				detail.setRiddorReference(injuredPerson.getRiddorReference());
				detail.setHseReportedDate(injuredPerson.getHseReportedDate());
				detail.setIncident(savedIncident);
				detail.setUpdatedBy(requestorName);
				detail.setUpdatedAt(new Date());
				detail.setUpdatedByUserId(requestor.getId());
				savedIncident.getInjuredPersonDeails().add(detail);
				incidentInjuredPersonRepo.save(detail);
			}

		}

		IncidentHistory hist = new IncidentHistory();
		hist.setUpdatedAt(new Date());
		hist.setLastUpdatedBy(requestorName);
		hist.setUpdatedByUserId(requestor.getId());
		hist.setIncidentNumber(savedIncident.getIncidentNumber());
		if (isNew) {
			hist.setLevel("L1");
			hist.setComments("New Incident Reported");
			hist.setStatus("CREATED");
		} else if (isL1Save) {
			hist.setStatus("SAVED");
			hist.setComments("Initial Report Saved");
			hist.setLevel("L1");
		} else if (isL2Submit) {
			hist.setStatus("SUBMITTED");
			hist.setComments("Initial Report Submitted");
			hist.setLevel("L2");
		} else if (isL2Edit) {
			hist.setStatus("EDITED");
			hist.setComments("Initial Report Edited and Saved");
			hist.setLevel("L2");
		} else if (isL2Approve) {
			hist.setStatus("APPROVED");
			hist.setComments("Initial Report Approved: " + reqIncident.getApproverComments());
			hist.setLevel("L2");
		} else if (isL3Save) {
			hist.setStatus("SAVED");
			hist.setComments("Full Investigation Saved");
			hist.setLevel("L3");
		} else if (isL3Submit) {
			hist.setStatus("SUBMITTED");
			hist.setComments("Full Investigation Submitted");
			hist.setLevel("L3");
		} else if (isL3Edit) {
			hist.setStatus("EDITED");
			hist.setComments("Full Investigation Edited and Saved");
			hist.setLevel("L3");
		} else if (isL3Approve) {
			hist.setStatus("APPROVED");
			hist.setComments("Full Investigation Approved: " + reqIncident.getApproverComments());
			hist.setLevel("L3");
		} else if (isL4Save) {
			hist.setStatus("SAVED");
			hist.setComments("H&S Details Saved");
			hist.setLevel("L4");
		} else if (isL4Approve) {

			hist.setStatus("APPROVED");
			hist.setComments("H&S Signed off");
			hist.setLevel("L4");
		}
		incidentHistRepo.save(hist);
		if (isNew) {
			updateDeadlines(savedIncident);
		}
		logger.info("OK-" + savedIncident.getIncidentNumber());
		return "OK-" + savedIncident.getIncidentNumber();

	}

	public void updateDeadlineAndTime(Long incidentNumber) {
		Incident incident = incidentRepo.findOne(incidentNumber);
		updateDeadlines(incident);

	}

	/**
	 * 
	 * @param savedIncident
	 */
	private void updateDeadlines(Incident savedIncident) {

		IncidentDeadlines deadlines = incidentDeadlinesRepo.findByIncidentNumber(savedIncident.getIncidentNumber());
		if (deadlines == null) {
			deadlines = new IncidentDeadlines();
		}
		deadlines.setIncidentDateTime(savedIncident.getIncidentDateTime());
		deadlines.setIncidentNumber(savedIncident.getIncidentNumber());
		final int SUN = 1, MON = 2, TUE = 3, WED = 4, THU = 5, FRI = 6, SAT = 7;
		Calendar incidentDate = Calendar.getInstance();
		incidentDate.setTime(savedIncident.getIncidentDateTime());
		int dayOfWeek = incidentDate.get(Calendar.DAY_OF_WEEK);
		logger.info("Incident date time is " + incidentDate.getTime());
		switch (dayOfWeek) {
		case SUN:
			incidentDate.add(Calendar.HOUR_OF_DAY, 8);
			deadlines.setSubmitReminderBy(incidentDate.getTime());
			logger.info("SUBMIT reminder after " + incidentDate.getTime()); // 8

			incidentDate.add(Calendar.HOUR_OF_DAY, 16);
			deadlines.setSubmitBy(incidentDate.getTime());
			logger.info("SUBMIT escalation after " + incidentDate.getTime());// 24

			incidentDate.add(Calendar.HOUR_OF_DAY, 12);
			deadlines.setFiReminder(incidentDate.getTime());
			logger.info("FI submit reminder  after " + incidentDate.getTime());// 36

			incidentDate.add(Calendar.HOUR_OF_DAY, 36);
			deadlines.setFiSubmitBy(incidentDate.getTime());
			logger.info("FI submit escalation is " + incidentDate.getTime());// 72

			incidentDate.add(Calendar.HOUR_OF_DAY, 24);
			deadlines.setGmApprovalReminder(incidentDate.getTime());
			logger.info("GM approval reminder after " + incidentDate.getTime());// 96

			incidentDate.add(Calendar.HOUR_OF_DAY, 72 + 24 + 24);
			deadlines.setGmApprovalBy(incidentDate.getTime());
			logger.info("GM approval  escalation  after  " + incidentDate.getTime());// 168
			break;
		case MON:
			incidentDate.add(Calendar.HOUR_OF_DAY, 8);
			deadlines.setSubmitReminderBy(incidentDate.getTime());
			logger.info("SUBMIT reminder after" + incidentDate.getTime()); // 8

			incidentDate.add(Calendar.HOUR_OF_DAY, 16);
			deadlines.setSubmitBy(incidentDate.getTime());
			logger.info("SUBMIT escalation after " + incidentDate.getTime());// 24

			incidentDate.add(Calendar.HOUR_OF_DAY, 12);
			deadlines.setFiReminder(incidentDate.getTime());
			logger.info("FI submit reminder  after " + incidentDate.getTime());// 36

			incidentDate.add(Calendar.HOUR_OF_DAY, 36);
			deadlines.setFiSubmitBy(incidentDate.getTime());
			logger.info("FI submit escalation is " + incidentDate.getTime());// 72

			incidentDate.add(Calendar.HOUR_OF_DAY, 24);
			deadlines.setGmApprovalReminder(incidentDate.getTime());
			logger.info("GM approval reminder after " + incidentDate.getTime());// 96

			incidentDate.add(Calendar.HOUR_OF_DAY, 72 + 24 + 24);
			deadlines.setGmApprovalBy(incidentDate.getTime());
			logger.info("GM approval  escalation  after  " + incidentDate.getTime());// 168
			break;
		case TUE:
			incidentDate.add(Calendar.HOUR_OF_DAY, 8);
			deadlines.setSubmitReminderBy(incidentDate.getTime());
			logger.info("SUBMIT reminder after" + incidentDate.getTime()); // 8

			incidentDate.add(Calendar.HOUR_OF_DAY, 16);
			deadlines.setSubmitBy(incidentDate.getTime());
			logger.info("SUBMIT escalation after " + incidentDate.getTime());// 24

			incidentDate.add(Calendar.HOUR_OF_DAY, 12);
			deadlines.setFiReminder(incidentDate.getTime());
			logger.info("FI submit reminder  after " + incidentDate.getTime());// 36

			incidentDate.add(Calendar.HOUR_OF_DAY, 36);
			deadlines.setFiSubmitBy(incidentDate.getTime());
			logger.info("FI submit escalation is " + incidentDate.getTime());// 72

			incidentDate.add(Calendar.HOUR_OF_DAY, 24 + 24 + 24);
			deadlines.setGmApprovalReminder(incidentDate.getTime());
			logger.info("GM approval reminder after " + incidentDate.getTime());// 96

			incidentDate.add(Calendar.HOUR_OF_DAY, 72);
			deadlines.setGmApprovalBy(incidentDate.getTime());
			logger.info("GM approval  escalation  after  " + incidentDate.getTime());// 168
			break;

		case WED:
			incidentDate.add(Calendar.HOUR_OF_DAY, 8);
			deadlines.setSubmitReminderBy(incidentDate.getTime());
			logger.info("SUBMIT reminder after" + incidentDate.getTime()); // 8

			incidentDate.add(Calendar.HOUR_OF_DAY, 16);
			deadlines.setSubmitBy(incidentDate.getTime());
			logger.info("SUBMIT escalation after " + incidentDate.getTime());// 24

			incidentDate.add(Calendar.HOUR_OF_DAY, 12);
			deadlines.setFiReminder(incidentDate.getTime());
			logger.info("FI submit reminder  after " + incidentDate.getTime());// 36

			incidentDate.add(Calendar.HOUR_OF_DAY, 36 + 24 + 24);
			deadlines.setFiSubmitBy(incidentDate.getTime());
			logger.info("FI submit escalation is " + incidentDate.getTime());// 72

			incidentDate.add(Calendar.HOUR_OF_DAY, 24);
			deadlines.setGmApprovalReminder(incidentDate.getTime());
			logger.info("GM approval reminder after " + incidentDate.getTime());// 96

			incidentDate.add(Calendar.HOUR_OF_DAY, 72);
			deadlines.setGmApprovalBy(incidentDate.getTime());
			logger.info("GM approval  escalation  after  " + incidentDate.getTime());// 168
			break;

		case THU:
			incidentDate.add(Calendar.HOUR_OF_DAY, 8);
			deadlines.setSubmitReminderBy(incidentDate.getTime());
			logger.info("SUBMIT reminder after " + incidentDate.getTime()); // 8

			incidentDate.add(Calendar.HOUR_OF_DAY, 16);
			deadlines.setSubmitBy(incidentDate.getTime());
			logger.info("SUBMIT escalation after " + incidentDate.getTime());// 24

			incidentDate.add(Calendar.HOUR_OF_DAY, 12 + 24 + 24);
			deadlines.setFiReminder(incidentDate.getTime());
			logger.info("FI submit reminder  after " + incidentDate.getTime());// 36

			incidentDate.add(Calendar.HOUR_OF_DAY, 36);
			deadlines.setFiSubmitBy(incidentDate.getTime());
			logger.info("FI submit escalation is " + incidentDate.getTime());// 72

			incidentDate.add(Calendar.HOUR_OF_DAY, 24);
			deadlines.setGmApprovalReminder(incidentDate.getTime());
			logger.info("GM approval reminder after " + incidentDate.getTime());// 96

			incidentDate.add(Calendar.HOUR_OF_DAY, 72 + 24 + 24);
			deadlines.setGmApprovalBy(incidentDate.getTime());
			logger.info("GM approval  escalation  after  " + incidentDate.getTime());// 168
			break;

		case FRI:
			incidentDate.add(Calendar.HOUR_OF_DAY, 8);
			deadlines.setSubmitReminderBy(incidentDate.getTime());
			logger.info("SUBMIT reminder after " + incidentDate.getTime()); // 8

			incidentDate.add(Calendar.HOUR_OF_DAY, 16 + 24 + 24);
			deadlines.setSubmitBy(incidentDate.getTime());
			logger.info("SUBMIT escalation after " + incidentDate.getTime());// 24

			incidentDate.add(Calendar.HOUR_OF_DAY, 12);
			deadlines.setFiReminder(incidentDate.getTime());
			logger.info("FI submit reminder  after " + incidentDate.getTime());// 36

			incidentDate.add(Calendar.HOUR_OF_DAY, 36);
			deadlines.setFiSubmitBy(incidentDate.getTime());
			logger.info("FI submit escalation is " + incidentDate.getTime());// 72

			incidentDate.add(Calendar.HOUR_OF_DAY, 24);
			deadlines.setGmApprovalReminder(incidentDate.getTime());
			logger.info("GM approval reminder after " + incidentDate.getTime());// 96

			incidentDate.add(Calendar.HOUR_OF_DAY, 72 + 24 + 24);
			deadlines.setGmApprovalBy(incidentDate.getTime());
			logger.info("GM approval  escalation  after  " + incidentDate.getTime());// 168
			break;
		case SAT:
			incidentDate.add(Calendar.HOUR_OF_DAY, 8);
			deadlines.setSubmitReminderBy(incidentDate.getTime());
			logger.info("SUBMIT reminder after " + incidentDate.getTime()); // 8

			incidentDate.add(Calendar.HOUR_OF_DAY, 16 + 24);
			deadlines.setSubmitBy(incidentDate.getTime());
			logger.info("SUBMIT escalation after " + incidentDate.getTime());// 24

			incidentDate.add(Calendar.HOUR_OF_DAY, 12);
			deadlines.setFiReminder(incidentDate.getTime());
			logger.info("FI submit reminder  after " + incidentDate.getTime());// 36

			incidentDate.add(Calendar.HOUR_OF_DAY, 36);
			deadlines.setFiSubmitBy(incidentDate.getTime());
			logger.info("FI submit escalation is " + incidentDate.getTime());// 72

			incidentDate.add(Calendar.HOUR_OF_DAY, 24);
			deadlines.setGmApprovalReminder(incidentDate.getTime());
			logger.info("GM approval reminder after " + incidentDate.getTime());// 96

			incidentDate.add(Calendar.HOUR_OF_DAY, 72 + 24 + 24);
			deadlines.setGmApprovalBy(incidentDate.getTime());
			logger.info("GM approval  escalation  after  " + incidentDate.getTime());// 168
			break;

		}
		incidentDeadlinesRepo.save(deadlines);

	}

	/***
	 * Create/Edit User
	 */
	@Transactional
	public String saveUser(UserModel userReq) {

		Users newUser = new Users();
		boolean isEdit = userReq.getUserId() != null ? userReq.getUserId() != 0 : false;
		Users findUser = userRepo.findByEmailId(userReq.getEmailId());
		if (findUser != null) {

			if (isEdit && findUser.getId().longValue() != userReq.getUserId().longValue()) {
				logger.info("DUPLICATE_EMAIL1 --" + findUser.getEmailId() + "----req--" + userReq.getUserId() + "----"
						+ findUser.getId());
				return "DUPLICATE_EMAIL";
			} else if (!isEdit) {
				logger.info("DUPLICATE_EMAIL2");
				return "DUPLICATE_EMAIL";
			}
		}
		if (isEdit) {
			newUser = userRepo.findOne(userReq.getUserId());
			userSiteRepo.deleteByUserId(newUser.getId());
			if (newUser.getIsApprover() == true && userReq.getIsApprover() == false) {
				approversRepo.delete(approversRepo.findByUserId(newUser.getId()));
			}
		}
		newUser.setFirstName(userReq.getFirstName());
		newUser.setLastName(userReq.getLastName());
		newUser.setManagerName(userReq.getManagerName());
		newUser.setEmailId(userReq.getEmailId());
		newUser.setActive(true);
		newUser.setIsApprover(userReq.getIsApprover());
		newUser.setIsActionOwner(userReq.getIsActionOwner());

		Users requestor = userRepo.findById(userReq.getRequestorId());
		if (!isEdit) {
			newUser.setPassword("test");
			newUser.setCreatedBy(requestor.getFirstName() + " " + requestor.getLastName());
			newUser.setCreatedAt(new Date());
		}

		newUser.setUpdatedBy(requestor.getFirstName() + " " + requestor.getLastName());
		Users savedUser = userRepo.save(newUser);

		// add records in users_site
		for (Long id : userReq.getSiteList()) {

			UserSite newRec = new UserSite();
			newRec.setSiteId(id);
			newRec.setUserId(savedUser.getId());
			userSiteRepo.save(newRec);
		}
		return "OK-" + savedUser.getId();
	}

	/* Add approvers for department */
	public String addApprovers(ApproverModel req) {

		for (String item : req.getApprovers()) {
			// Ex - L1-12
			if (item != null) {
				String level = item.substring(0, item.indexOf('-'));
				String userId = item.substring(item.indexOf('-') + 1);
				Approvers newApprover = new Approvers();
				newApprover.setIsActive(true);
				newApprover.setUserId(Long.valueOf(userId));
				newApprover.setLevel(level);
				newApprover.setSiteId(req.getSiteId());
				newApprover.setCreateAt(new Date());
				newApprover.setCreatedBy(req.getRequestor());
				newApprover.setUpdatedBy(req.getRequestor());
				newApprover.setUpdatedAt(new Date());
				approversRepo.save(newApprover);
			}
		}
		return "OK";

	}

	/* Add recipients to notifications groups */
	public String addRecipients(ApproverModel req) {

		for (String item : req.getApprovers()) {
			// Ex - L1-12
			String group = item.substring(0, item.indexOf('-'));
			String userId = item.substring(item.indexOf('-') + 1);
			Recipients newRecipient = new Recipients();
			newRecipient.setIsActive(true);
			newRecipient.setUserId(Long.valueOf(userId));
			newRecipient.setGroup(group);
			newRecipient.setSiteId(req.getSiteId());
			newRecipient.setCreateAt(new Date());
			newRecipient.setCreatedBy(req.getRequestor());
			newRecipient.setUpdatedBy(req.getRequestor());
			newRecipient.setUpdatedAt(new Date());
			logger.info("Data ===" + group + "---" + req.getSiteId() + "----" + group + " ----" + userId);
			recipientsRepo.save(newRecipient);
		}
		return "OK";

	}

	/***
	 * 
	 * Create a new Site
	 * 
	 * @param request
	 * @return
	 */
	public String saveSite(SiteModel request) {
		boolean isEdit = request.getSiteId() != null ? request.getSiteId() != 0 : false;
		Site newSite = new Site();
		if (isEdit) {
			newSite = siteRepo.findOne(request.getSiteId());
		}
		newSite.setSiteCode(request.getSiteCode());
		newSite.setSiteName(request.getSiteName());
		Users requestor = userRepo.findById(request.getRequestorId());
		if (!isEdit) {
			newSite.setCreatedAt(new Date());
			newSite.setCreatedBy(requestor.getFirstName() + " " + requestor.getLastName());
		}
		newSite.setUpdatedBy(requestor.getFirstName() + " " + requestor.getLastName());
		newSite.setUpdatedAt(new Date());
		Site savedSite = siteRepo.save(newSite);
		return "OK";
	}

	/****
	 * 
	 */
	public String rejectIncident(Long incidentNumber, Long approverId, String comments, Long prevApproverId) {
		Users approver = userRepo.findById(approverId);
		Incident incident = incidentRepo.findOne(incidentNumber);
		String newStatus = incident.getStatus().split("_")[0] + "_REJECTED";
		if (incident.getStatus().equalsIgnoreCase("L3_APPROVED") || incident.getStatus().equalsIgnoreCase("L4_SAVED")) {
			newStatus = "L4_REJECTED";
			incident.setCurrApproverId(prevApproverId);
		} else if (incident.getStatus().equalsIgnoreCase("L4_REJECTED")) {
			newStatus = "L3_REJECTED";
			// incident.setCurrApproverId(prevApproverId);
		}
		incident.setStatus(newStatus);
		incident.setUpdatedAt(new Date());
		incident.setUpdatedBy(approver.getFirstName() + " " + approver.getLastName());
		incidentRepo.save(incident);
		IncidentHistory hist = new IncidentHistory();
		if (incident.getStatus().split("_")[0].equals("L2")) {
			comments = "Initial Report Rejected: " + comments;

		} else if (incident.getStatus().split("_")[0].equals("L3")) {
			comments = "Full Investigation Rejected: " + comments;
		} else if (incident.getStatus().split("_")[0].equals("L4")) {
			comments = "H&S Rejected: " + comments;
		}
		hist.setComments(comments);
		hist.setIncidentNumber(incidentNumber);
		hist.setLevel(incident.getStatus().split("_")[0]);
		hist.setStatus("REJECTED");
		hist.setLastUpdatedBy(approver.getFirstName() + " " + approver.getLastName());
		hist.setUpdatedByUserId(approverId);
		hist.setUpdatedAt(new Date());
		incidentHistRepo.save(hist);
		return "OK";
	}

	/***
	 * 
	 * @param incidentNumber
	 * @param approverId
	 * @param comments
	 * @return
	 */
	public String deleteIncident(Long incidentNumber, Long requestorId, String comments) {
		Users requestor = userRepo.findById(requestorId);
		Incident incident = incidentRepo.findOne(incidentNumber);
		incident.setStatus("_DELETED");
		incident.setUpdatedAt(new Date());
		incident.setUpdatedBy(requestor.getFirstName() + " " + requestor.getLastName());
		incidentRepo.save(incident);
		IncidentHistory hist = new IncidentHistory();
		hist.setComments("Incident Deleted: " + comments);
		hist.setIncidentNumber(incidentNumber);
		hist.setStatus("DELETED");
		hist.setLastUpdatedBy(requestor.getFirstName() + " " + requestor.getLastName());
		hist.setUpdatedByUserId(requestorId);
		hist.setUpdatedAt(new Date());
		incidentHistRepo.save(hist);
		return "OK";
	}

	/***
	 * 
	 * @param incidentNumber
	 * @param approverId
	 * @param comments
	 * @return
	 */
	public String deleteHazard(Long hazardNumber, Long requestorId, String comments) {
		Users requestor = userRepo.findById(requestorId);
		Hazard hazard = hazardRepo.findOne(hazardNumber);
		hazard.setStatus("_DELETED");
		hazard.setUpdatedAt(new Date());
		hazard.setUpdatedBy(requestor.getFirstName() + " " + requestor.getLastName());
		hazardRepo.save(hazard);
		HazardHistory hist = new HazardHistory();
		hist.setComments("Hazard Deleted: " + comments);
		hist.setHazardId(hazardNumber);
		hist.setStatus("DELETED");
		hist.setLastUpdatedBy(requestor.getFirstName() + " " + requestor.getLastName());
		hist.setUpdatedByUserId(requestorId);
		hist.setUpdatedAt(new Date());
		hazardHistRepo.save(hist);
		return "OK";
	}

	/***
	 * 
	 * 
	 */
	public String saveRiskHazard(RiskHazardModel reqHazard) {

		 
		RiskHazard riskHazard = new RiskHazard();
		Users requestor = userRepo.findById(reqHazard.getUserId());
		String reqName = requestor.getFirstName() + " " + requestor.getLastName();
		
		String response  = saveRiskAssessment(reqHazard.getRaModel());
		reqHazard.setGraId(Long.valueOf(response.split("-")[1]));
		RiskAssessment gra = riskAssessmentRepo.findByGraId(reqHazard.getGraId());
		if (reqHazard.getHazardId()!=null &&  reqHazard.getHazardId() != 0) {
			riskHazard = riskHazardRepo.findById(reqHazard.getHazardId());
		} else {
			riskHazard.setCreatedAt(new Date());
			riskHazard.setCreatedBy(reqName);
		}
		riskHazard.setHazardDetail(reqHazard.getHazardDetails());
		riskHazard.setGraId(reqHazard.getGraId());
		
		riskHazard.setRiskRatingL(reqHazard.getRiskRatingL());
		riskHazard.setRiskRatingS(reqHazard.getRiskRatingS());
		riskHazard.setNewRiskRatingL(reqHazard.getNewRiskRatingL());
		riskHazard.setNewRiskRatingS(reqHazard.getNewRiskRatingS());
		//riskHazard.setImplementedWhen(reqHazard.getImplementedWhen());
		//riskHazard.setImplementedWho(reqHazard.getImplementedWho());
		riskHazard.setPersonsAtRisk(reqHazard.getPersonsAtRisk());
		riskHazard.setIsFurtherActionRequired(reqHazard.getIsFurtherActionsRequired());
		riskHazard.setExistingControls(reqHazard.getExistingControls());
		riskHazard.setUpdatedAt(new Date());
		riskHazard.setUpdatedBy(reqName);

		RiskHazard savedRiskHazard = riskHazardRepo.save(riskHazard);
		
		// delete the one which are present in DB but not in request
		
		List<Action> dbOtherMeasures = actionRepo
				.findActionsForRiskHazard(savedRiskHazard.getId());
		for (Action dbOtherMeasure : dbOtherMeasures) {
			boolean recordDeleted = true;
			for (ActionModel reqMeasure : reqHazard.getFurtherActions()) {
				if (reqMeasure.getId().longValue() == dbOtherMeasure.getId().longValue()) {
					recordDeleted = false;
				}
			}
			if (recordDeleted) {
				actionRepo.delete(dbOtherMeasure.getId());
				logger.info("Risk Hazard action delete for it" + dbOtherMeasure.getId());
			}
		}
		for (ActionModel action : reqHazard.getFurtherActions()) {
			Action newAction = new Action();
			if (action.getId() != 0) {
				newAction = actionRepo.findOne(action.getId());

			}
			newAction.setAction(action.getAction());
			newAction.setActionOwner(action.getActionOwner());
			newAction.setActionType("Countermeasure");
			newAction.setDeadline(action.getDeadline());
			newAction.setEstimatedCompletionDate(action.getDeadline());
			newAction.setActionSubType(action.getActionSubType());
			newAction.setActionSite(action.getActionSite());
			String oldStatus = newAction.getStatus();
			if (oldStatus == null) {
				oldStatus = "";
			}
			logger.info("action id is AC" + action.getId());
			newAction.setStatus(getActionStatusForRA(gra.getStatus(), action.getId() == 0, newAction.getStatus()));
			if (newAction.getStatus().equals("ASSIGNED") && oldStatus != null
					&& !oldStatus.equals(newAction.getStatus())) {
				newAction.setAssignedAt(new Date());
				newAction.setAssignedBy(reqName);

			}
			newAction.setUpdatedAt(new Date());
			newAction.setUpdatedBy(reqName);
			newAction.setUpdatedByUserId(requestor.getId());
			newAction.setReportId(reqHazard.getGraId());
			newAction.setReportSubId(savedRiskHazard.getId());
			newAction.setActionSource("Risk Assessment");
			Action savedAction = actionRepo.save(newAction);

			ActionHistory actionHist = new ActionHistory();
			actionHist.setComments("Action " + savedAction.getStatus());
			actionHist.setStatus(savedAction.getStatus());
			actionHist.setReportId(gra.getGraId());
			actionHist.setActionSource("Risk Assessment");
			actionHist.setActionNumber(savedAction.getId());
			actionHist.setUpdatedAt(new Date());
			actionHist.setLastUpdatedBy(reqName);
			actionHist.setUpdatedByUserId(requestor.getId());
			actionHist.setActionOwnerId(savedAction.getActionOwner());
			actionHistRepo.save(actionHist);
		}

		// Save further actions

		return "OK-"+savedRiskHazard.getId()+"-"+savedRiskHazard.getGraId();
	}

	/**
	 * Return the status of Action based on the Hazard status and various
	 * conditions
	 * 
	 * @param incidentStatus
	 * @param isNew
	 * @param actionStatus
	 * @return
	 */
	private String getHazardActionStatus(String hazardStatus, boolean isNew, String actionStatus) {
		String prevstatus = actionStatus;
		switch (hazardStatus) {
		case "L2_SAVED":
			actionStatus = (isNew == true) ? "CREATED" : actionStatus;
			break;
		case "L2_APPROVED":
			// actionStatus = "ASSIGNED";
			actionStatus = (isNew == true || prevstatus.equals("CREATED")) ? "ASSIGNED" : actionStatus;
			break;
		}
		logger.info("getActionStatus====is new =" + isNew + "   Hazard Sttaus=" + hazardStatus + " Prev status is ="
				+ prevstatus + "   New Action Sttaus =" + actionStatus);
		return actionStatus;
	}

	/**
	 * Return the status of Action based on the Incident status and various
	 * conditions
	 * 
	 * @param incidentStatus
	 * @param isNew
	 * @param actionStatus
	 * @return
	 */
	private String getActionStatus(String incidentStatus, boolean isNew, String actionStatus) {
		String prevstatus = actionStatus;
		switch (incidentStatus) {
		case "L1_SAVED":
			actionStatus = (isNew == true) ? "CREATED" : actionStatus;
			break;
		case "L2_SUBMITTED":
			// actionStatus = "ASSIGNED";
			actionStatus = (isNew == true || prevstatus.equals("CREATED")) ? "ASSIGNED" : actionStatus;
			break;
		case "L2_EDITED":
			actionStatus = (isNew == true) ? "CREATED" : actionStatus;
			break;
		case "L2_APPROVED":
			// actionStatus = "ASSIGNED";
			actionStatus = (isNew == true || prevstatus.equals("CREATED")) ? "ASSIGNED" : actionStatus;
			break;
		case "L3_SAVED":
			actionStatus = (isNew == true) ? "CREATED" : actionStatus;
			break;
		case "L3_SUBMITTED":
			// actionStatus = "ASSIGNED";
			actionStatus = (isNew == true || prevstatus.equals("CREATED")) ? "ASSIGNED" : actionStatus;
			break;
		case "L3_EDITED":
			actionStatus = (isNew == true) ? "CREATED" : actionStatus;
			break;
		case "L3_APPROVED":
			// actionStatus = "ASSIGNED";
			actionStatus = (isNew == true || prevstatus.equals("CREATED")) ? "ASSIGNED" : actionStatus;
			break;
		case "L4_SAVED":
			actionStatus = (isNew == true || prevstatus.equals("CREATED")) ? "ASSIGNED" : actionStatus;
			break;
		case "L4_APPROVED":
			actionStatus = (isNew == true || prevstatus.equals("CREATED")) ? "ASSIGNED" : actionStatus;
			break;
		case "_CLOSED":
			actionStatus = (isNew == true || prevstatus.equals("CREATED")) ? "ASSIGNED" : actionStatus;
			break;
		default:
			break;
		}
		logger.info("getActionStatus====is new =" + isNew + "   Incident Sttaus=" + incidentStatus + " Prev status is ="
				+ prevstatus + "   New Action Sttaus =" + actionStatus);
		return actionStatus;

	}
	
	/**
	 * Return the status of Action based on the RA status and various
	 * conditions
	 * 
	 * @param rastatus
	 * @param isNew
	 * @param actionStatus
	 * @return
	 */
	private String getActionStatusForRA(String raStatus, boolean isNew, String actionStatus) {
		String prevstatus = actionStatus;
		switch (raStatus) {
		case "L1_SAVED":
			actionStatus = (isNew == true) ? "CREATED" : actionStatus;
			break;
		case "L2_SUBMITTED":
			// actionStatus = "ASSIGNED";
			actionStatus = (isNew == true || prevstatus.equals("CREATED")) ? "ASSIGNED" : actionStatus;
			break;
		case "L2_EDITED":
			actionStatus = (isNew == true) ? "CREATED" : actionStatus;
			break;
		case "L2_APPROVED":
			// actionStatus = "ASSIGNED";
			actionStatus = (isNew == true || prevstatus.equals("CREATED")) ? "ASSIGNED" : actionStatus;
			break;
		case "L3_EDITED":
			actionStatus = (isNew == true) ? "CREATED" : actionStatus;
			break;
		case "L3_APPROVED":
			// actionStatus = "ASSIGNED";
			actionStatus = (isNew == true || prevstatus.equals("CREATED")) ? "ASSIGNED" : actionStatus;
			break;
		case "L4_EDITED":
			actionStatus = (isNew == true || prevstatus.equals("CREATED")) ? "ASSIGNED" : actionStatus;
			break;
		case "L4_APPROVED":
			actionStatus = (isNew == true || prevstatus.equals("CREATED")) ? "ASSIGNED" : actionStatus;
			break;
		case "_CLOSED":
			actionStatus = (isNew == true || prevstatus.equals("CREATED")) ? "ASSIGNED" : actionStatus;
			break;
		default:
			break;
		}
		logger.info("getActionStatus====is new =" + isNew + "   Incident Sttaus=" + raStatus + " Prev status is ="
				+ prevstatus + "   New Action Sttaus =" + actionStatus);
		return actionStatus;

	}
	

	/***
	 * 
	 * Perform different action on a ACTION(Countermeasure or a Containment)
	 */
	@Transactional
	public String saveAction(ActionModel actionRequest) {

		String filter = actionRequest.getFilter();
		Action dbAction = new Action();
		Users requestor = userRepo.findOne(actionRequest.getCreatedByUserId());
		String requestorName = requestor.getFirstName() + " " + requestor.getLastName();
		if (!filter.equals("NEW")) {
			dbAction = actionRepo.findOne(actionRequest.getId());
		}
		dbAction.setComments(actionRequest.getComments());
		dbAction.setUpdatedAt(new Date());
		dbAction.setUpdatedBy(requestorName);
		dbAction.setUpdatedByUserId(requestor.getId());
		if (filter.equals("CANCEL")) {
			dbAction.setStatus("CANCELLED");
		} else if (filter.equals("COMPLETE")) {
			dbAction.setStatus("CLOSED");
		} else if (filter.equals("REOPEN")) {
			dbAction.setStatus("RE-OPENED");
			dbAction.setActionOwner(actionRequest.getActionOwner());
			dbAction.setAssignedBy(requestorName);
			dbAction.setActionSite(actionRequest.getActionSite());
			dbAction.setDeadline(actionRequest.getDeadline());
			dbAction.setEstimatedCompletionDate(actionRequest.getDeadline());
			dbAction.setAssignedAt(new Date());
		} else if (filter.equals("CHANGE")) {
			dbAction.setStatus("RE-SCHEDULED");
			dbAction.setEstimatedCompletionDate(actionRequest.getEstimatedCompletionDate());

		} else if (filter.equals("COMMENTS")) {
			dbAction.setComments(actionRequest.getComments());
		} else if (filter.equals("REASSIGNED")) {
			dbAction.setStatus("RE-ASSIGNED");
			dbAction.setActionSite(actionRequest.getActionSite());
			dbAction.setActionOwner(actionRequest.getActionOwner());
			dbAction.setAssignedBy(requestorName);
			dbAction.setAssignedAt(new Date());
		} else {
			dbAction.setAction(actionRequest.getAction());
			dbAction.setActionOwner(actionRequest.getActionOwner());
			dbAction.setActionType(actionRequest.getActionType());
			dbAction.setDeadline(actionRequest.getDeadline());
			dbAction.setEstimatedCompletionDate(actionRequest.getDeadline());
			dbAction.setActionSubType(actionRequest.getActionSubType());
			dbAction.setActionSite(actionRequest.getActionSite());
			logger.info(actionRequest.getActionSource());
			dbAction.setActionSource(actionRequest.getActionSource());
			dbAction.setStatus("ASSIGNED");
			dbAction.setAssignedAt(new Date());
			dbAction.setAssignedBy(requestorName);
			if (actionRequest.getReportId() != 0) {
				// dbAction.setIncident(actionIncident);
				dbAction.setReportId(actionRequest.getReportId());
				dbAction.setActionSource(actionRequest.getActionSource());
			}
		}
		actionRepo.save(dbAction);
		ActionHistory actionHist = new ActionHistory();
		if (dbAction.getComments() != null && !dbAction.getComments().equals("")) {
			actionHist.setComments("Action " + dbAction.getStatus() + ":" + dbAction.getComments());
			if (filter.equals("COMMENTS")) {
				actionHist.setComments("Action Comments:" + dbAction.getComments());
			}
		} else {
			actionHist.setComments("Action " + dbAction.getStatus());
		}
		actionHist.setStatus(dbAction.getStatus());
		actionHist.setReportId(actionRequest.getReportId());
		actionHist.setActionSource(actionRequest.getActionSource());

		actionHist.setActionNumber(dbAction.getId());
		actionHist.setUpdatedAt(new Date());
		actionHist.setLastUpdatedBy(requestorName);
		actionHist.setUpdatedByUserId(requestor.getId());
		actionHist.setActionOwnerId(dbAction.getActionOwner());
		actionHistRepo.save(actionHist);

		// Check if its the last action to be closed or completed

		if (actionRequest.getReportId() != 0 && (filter.equals("COMPLETE") || filter.equals("CANCEL"))) {
			if (actionRequest.getActionSource().equalsIgnoreCase("Incident")) {
				Incident actionIncident = incidentRepo.findOne(actionRequest.getReportId());
				Integer cnt = actionRepo.getCountOfOpenActionsForIncident(actionIncident.getIncidentNumber());

				if (cnt == 0) {
					// Update the incident status and update history

					if (actionIncident.getStatus().equals("L4_APPROVED")) {
						actionIncident.setStatus("_CLOSED");
						actionIncident.setUpdatedAt(new Date());
						actionIncident.setUpdatedBy(requestorName);
						incidentRepo.save(actionIncident);

						IncidentHistory hist = new IncidentHistory();
						hist.setUpdatedAt(new Date());
						hist.setLastUpdatedBy(requestorName);
						hist.setUpdatedByUserId(requestor.getId());
						hist.setIncidentNumber(actionIncident.getIncidentNumber());
						hist.setComments("Incident Report Closed due to last pending action " + filter.toLowerCase());
						hist.setStatus("_CLOSED");

						incidentHistRepo.save(hist);
					}
				}
			} else if (actionRequest.getActionSource().equalsIgnoreCase("Hazard")) {
				Hazard actionHazard = hazardRepo.findOne(actionRequest.getReportId());
				Integer cnt = actionRepo.getCountOfOpenActionsForHazard(actionHazard.getHazardNumber());

				if (cnt == 0) {
					// Update the incident status and update history

					if (actionHazard.getStatus().equals("L2_APPROVED")) {
						actionHazard.setStatus("_CLOSED");
						actionHazard.setUpdatedAt(new Date());
						actionHazard.setUpdatedBy(requestorName);
						hazardRepo.save(actionHazard);

						HazardHistory hist = new HazardHistory();
						hist.setUpdatedAt(new Date());
						hist.setLastUpdatedBy(requestorName);
						hist.setUpdatedByUserId(requestor.getId());
						hist.setHazardId(actionHazard.getHazardNumber());
						hist.setComments("Hazard Report Closed due to last pending action " + filter.toLowerCase());
						hist.setStatus("_CLOSED");

						hazardHistRepo.save(hist);
					}
				}
			} else if (actionRequest.getActionSource().equalsIgnoreCase("GRA")) {
				RiskAssessment gra = riskAssessmentRepo.findByGraId(actionRequest.getReportId());
				Integer cnt = actionRepo.findOpenActionsForRiskAssessment(gra.getGraId());
				if (cnt == 0) {
					// Update the incident status and update history

					if (gra.getStatus().equals("L4_APPROVED")) {
						gra.setStatus("_CLOSED");
						gra.setUpdatedAt(new Date());
						gra.setUpdatedBy(requestorName);
						riskAssessmentRepo.save(gra);

						GRAHistory hist = new GRAHistory();
						hist.setUpdatedAt(new Date());
						hist.setLastUpdatedBy(requestorName);
						hist.setUpdatedByUserId(requestor.getId());
						hist.setGraId(gra.getGraId());
						hist.setComments("Risk Assessment Report Closed due to last pending action " + filter.toLowerCase());
						hist.setStatus("_CLOSED");

						graHistRepo.save(hist);
					}
				}
				
			}

		} else if (actionRequest.getReportId() != 0 && (filter.equals("REOPEN") || filter.equals("NEW"))) {
			// Re-open the Report if closed
			if (actionRequest.getActionSource().equalsIgnoreCase("Incident")) {
				Incident actionIncident = incidentRepo.findOne(actionRequest.getReportId());
				if (actionIncident.getStatus().equals("_CLOSED")) {
					actionIncident.setStatus("L4_APPROVED");
					actionIncident.setUpdatedAt(new Date());
					actionIncident.setUpdatedBy(requestorName);
					incidentRepo.save(actionIncident);

					IncidentHistory hist = new IncidentHistory();
					hist.setUpdatedAt(new Date());
					hist.setLastUpdatedBy(requestorName);
					hist.setUpdatedByUserId(requestor.getId());
					hist.setIncidentNumber(actionIncident.getIncidentNumber());
					if (filter.equals("REOPEN")) {
						hist.setComments("Incident Report Re-Opened due to one of the action re-opened.");
					} else {
						hist.setComments("Incident Report Re-Opened due to new action linked.");
					}
					hist.setStatus("RE-OPENED");
					incidentHistRepo.save(hist);
				}
			} else if (actionRequest.getActionSource().equalsIgnoreCase("Hazard")) {
				Hazard actionHazard = hazardRepo.findOne(actionRequest.getReportId());
				if (actionHazard.getStatus().equals("_CLOSED")) {
					actionHazard.setStatus("L2_APPROVED");
					actionHazard.setUpdatedAt(new Date());
					actionHazard.setUpdatedBy(requestorName);
					hazardRepo.save(actionHazard);

					HazardHistory hist = new HazardHistory();
					hist.setUpdatedAt(new Date());
					hist.setLastUpdatedBy(requestorName);
					hist.setUpdatedByUserId(requestor.getId());
					hist.setHazardId(actionHazard.getHazardNumber());
					if (filter.equals("REOPEN")) {
						hist.setComments("Hazard Report Re-Opened due to one of the action re-opened.");
					} else {
						hist.setComments("Hazard Report Re-Opened due to new action linked.");
					}
					hist.setStatus("RE-OPENED");
					hazardHistRepo.save(hist);
				}
			}else if (actionRequest.getActionSource().equalsIgnoreCase("GRA")) {
				RiskAssessment actionGRA = riskAssessmentRepo.findByGraId(actionRequest.getReportId());
				if (actionGRA.getStatus().equals("_CLOSED")) {
					actionGRA.setStatus("L4_APPROVED");
					actionGRA.setUpdatedAt(new Date());
					actionGRA.setUpdatedBy(requestorName);
					riskAssessmentRepo.save(actionGRA);

					GRAHistory hist = new GRAHistory();
					hist.setUpdatedAt(new Date());
					hist.setLastUpdatedBy(requestorName);
					hist.setUpdatedByUserId(requestor.getId());
					hist.setGraId(actionGRA.getGraId());
					if (filter.equals("REOPEN")) {
						hist.setComments("Risk Assessment Report Re-Opened due to one of the action re-opened.");
					} else {
						hist.setComments("Risk Assessment Report Re-Opened due to new action linked.");
					}
					hist.setStatus("_CLOSED");

					graHistRepo.save(hist);
				}
			}
		}

		return "OK-" + dbAction.getId();

	}

	/***
	 * 
	 * @param actionRequest
	 * @return
	 */
	public String changeApprover(ApproverModel request) {
		Users requestor = userRepo.findById(request.getRequestorId());
		String requestorName = requestor.getFirstName() + " " + requestor.getLastName();
		if (request.getReportType().equalsIgnoreCase("Incident")) {
			Incident newIncident = incidentRepo.findOne(request.getReportId());
			newIncident.setCurrApproverId(request.getCurrApproverId());
			newIncident.setUpdatedAt(new Date());
			newIncident.setUpdatedBy(requestorName);
			incidentRepo.save(newIncident);

			IncidentHistory hist = new IncidentHistory();
			hist.setUpdatedAt(new Date());
			hist.setLastUpdatedBy(requestorName);
			hist.setUpdatedByUserId(requestor.getId());
			hist.setIncidentNumber(newIncident.getIncidentNumber());
			hist.setStatus("RE-ASSIGNED");
			hist.setComments("Approver changed: " + request.getApproverComments());
			incidentHistRepo.save(hist);
		} else if (request.getReportType().equalsIgnoreCase("Hazard")) {
			Hazard newHazard = hazardRepo.findOne(request.getReportId());
			newHazard.setCurrApproverId(request.getCurrApproverId());
			newHazard.setUpdatedAt(new Date());
			newHazard.setUpdatedBy(requestorName);
			hazardRepo.save(newHazard);

			HazardHistory hist = new HazardHistory();
			hist.setUpdatedAt(new Date());
			hist.setLastUpdatedBy(requestorName);
			hist.setUpdatedByUserId(requestor.getId());
			hist.setHazardId(newHazard.getHazardNumber());
			hist.setStatus("RE-ASSIGNED");
			hist.setComments("Approver changed: " + request.getApproverComments());
			hazardHistRepo.save(hist);
		} else if (request.getReportType().equalsIgnoreCase("RiskAssessment")) {
			RiskAssessment gra = riskAssessmentRepo.findByGraId(request.getReportId());
			gra.setCurrApproverId(request.getCurrApproverId());
			gra.setUpdatedAt(new Date());
			gra.setUpdatedBy(requestorName);
			riskAssessmentRepo.save(gra);
			GRAHistory hist = new GRAHistory();
			hist.setUpdatedAt(new Date());
			hist.setLastUpdatedBy(requestorName);
			hist.setUpdatedByUserId(requestor.getId());
			hist.setGraId(gra.getGraId());
			hist.setStatus("RE-ASSIGNED");
			hist.setComments("Approver changed: " + request.getApproverComments());
			graHistRepo.save(hist);
		}
		// sendMail("")

		return "OK";
	}

	public String saveHazard(HazardModel reqHazard) {

		Hazard hazard = new Hazard();
		Users requestor = userRepo.findById(reqHazard.getUserId());
		String requestorName = requestor.getFirstName() + " " + requestor.getLastName();
		String status = "", level = "", histComment = "", histStatus = "";
		if (!reqHazard.getFilter().equalsIgnoreCase("NEW")) {
			hazard = hazardRepo.findOne(reqHazard.getHazardNumber());
		}
		if (reqHazard.getFilter().equalsIgnoreCase("NEW")) {
			histComment = "Hazard Submitted";
			status = "L1_SUBMITTED";
			histStatus = "SUBMITTED";
			level = "L1";
		} else if (reqHazard.getFilter().equalsIgnoreCase("L2_SAVED")) {
			histComment = "Hazard Investigation Saved";
			status = "L2_SAVED";
			histStatus = "SAVED";
			level = "L2";
		} else if (reqHazard.getFilter().equalsIgnoreCase("L2_APPROVED")) {
			histComment = "Hazard Investigation Signed Off";
			status = "L2_APPROVED";
			histStatus = "APPROVED";
			level = "L2";
		}
		hazard.setSiteId(reqHazard.getSiteId());
		hazard.setLocation(reqHazard.getLocation());
		hazard.setLocationOther(reqHazard.getLocationOther());
		hazard.setHazardDateTime(reqHazard.getHazardDateTime());
		hazard.setHazardDescription(reqHazard.getHazardDescription());
		hazard.setAnyWitness(reqHazard.getAnyWitness());
		hazard.setHaveDoneAnythingToMakeItSafe(reqHazard.getMakeItSafe());
		hazard.setReporterDepartment(reqHazard.getReporterDepartment());
		hazard.setReporterName(reqHazard.getReporterName());
		hazard.setImprovementSuggestions(reqHazard.getImprovementSuggestions());
		hazard.setWitnessName(reqHazard.getWitnessName());
		if (reqHazard.getMakeItSafe().equalsIgnoreCase("Yes")) {
			hazard.setWhatIsDone(reqHazard.getMakeItSafeWhyWhat());
		} else {
			hazard.setWhyNotDone(reqHazard.getMakeItSafeWhyWhat());
		}
		hazard.setUpdatedBy(requestor.getFirstName() + " " + requestor.getLastName());
		hazard.setUpdatedAt(new Date());
		hazard.setStatus(status);
		if (reqHazard.getFilter().equalsIgnoreCase("NEW")) {
			hazard.setCreatedBy(requestor.getFirstName() + " " + requestor.getLastName());
			hazard.setCreatedAt(new Date());
			hazard.setCreatorUserId(requestor.getId());
			hazard.setCurrApproverId(reqHazard.getCurrApproverId());
		}

		if (reqHazard.getFilter().equalsIgnoreCase("L2_SAVED")
				|| reqHazard.getFilter().equalsIgnoreCase("L2_APPROVED")) {
			hazard.setActOrCondition(reqHazard.getActOrCondition());
			hazard.setRiskCategory(reqHazard.getRiskCategory());
			hazard.setHazardComments(reqHazard.getHazardComments());
			hazard.setHazardActions(reqHazard.getHazardActions());
			hazard.setFurtherActionNeeded(reqHazard.getFurtherActionsNeeded());
		}
		Hazard savedHazard = hazardRepo.save(hazard);
		if (reqHazard.getFilter().equalsIgnoreCase("L2_SAVED")
				|| reqHazard.getFilter().equalsIgnoreCase("L2_APPROVED")) {
			if (reqHazard.getFurtherActionsNeeded().equalsIgnoreCase("Yes")
					&& !reqHazard.getFilter().equalsIgnoreCase("NEW")) {

				// delete the one which are present in DB but not in request

				List<Action> dbOtherMeasures = actionRepo.findCounterMeasuresByHazard(savedHazard.getHazardNumber());
				for (Action dbOtherMeasure : dbOtherMeasures) {
					boolean recordDeleted = true;
					for (ActionModel reqMeasure : reqHazard.getCounterMeasures()) {
						if (reqMeasure.getId().longValue() == dbOtherMeasure.getId().longValue()) {
							recordDeleted = false;

						}

					}
					if (recordDeleted) {
						actionRepo.delete(dbOtherMeasure.getId());
						logger.info("Hazard countermeasure old action delte for it" + dbOtherMeasure.getId());
					}
				}
				// Add hazard Counter Measures
				for (ActionModel reqMeasure : reqHazard.getCounterMeasures()) {
					Action counterMeasure = new Action();
					String prevStatus = "";
					if (reqMeasure.getId() != 0) {
						counterMeasure = actionRepo.findOne(reqMeasure.getId());
						prevStatus = counterMeasure.getStatus();
					}
					if (reqMeasure.getAction().equals("") == false) {
						counterMeasure.setAction(reqMeasure.getAction());
						counterMeasure.setActionOwner(reqMeasure.getActionOwner());
						counterMeasure.setActionSite(reqMeasure.getActionSite());
						counterMeasure.setActionType("Hazard-Countermeasure");
						counterMeasure.setActionSubType(reqMeasure.getActionSubType());
						counterMeasure.setDeadline(reqMeasure.getDeadline());
						counterMeasure.setEstimatedCompletionDate(reqMeasure.getDeadline());
						logger.info("action id is AC" + reqMeasure.getId());
						counterMeasure.setStatus(
								getHazardActionStatus(savedHazard.getStatus(), prevStatus.equals(""), prevStatus));
						if (counterMeasure.getStatus().equals("ASSIGNED")
								&& (!prevStatus.equals(counterMeasure.getStatus()))) {
							counterMeasure.setAssignedAt(new Date());
							counterMeasure.setAssignedBy(requestorName);
						}
						// counterMeasure.setIncident(savedIncident);
						counterMeasure.setReportId(savedHazard.getHazardNumber());
						counterMeasure.setActionSource("Hazard");
						counterMeasure.setCounterMeasureNumber(null);
						counterMeasure.setReportSubId(null);

						counterMeasure.setUpdatedAt(new Date());

						counterMeasure.setUpdatedBy(requestorName);
						counterMeasure.setUpdatedByUserId(requestor.getId());
						// savedIncident.getActions().add(counterMeasure);
						Action savedAction = actionRepo.save(counterMeasure);

						if (reqMeasure.isAddHistory()) {
							ActionHistory actionHist = new ActionHistory();
							actionHist.setComments("Action " + counterMeasure.getStatus());
							actionHist.setStatus(counterMeasure.getStatus());
							actionHist.setReportId(savedHazard.getHazardNumber());
							actionHist.setActionSource("Hazard");

							actionHist.setActionNumber(savedAction.getId());
							actionHist.setUpdatedAt(new Date());
							actionHist.setLastUpdatedBy(requestorName);
							actionHist.setUpdatedByUserId(requestor.getId());
							actionHist.setActionOwnerId(savedAction.getActionOwner());
							actionHistRepo.save(actionHist);
						}
					} else if (reqMeasure.getAction().equals("")) {
						counterMeasure.setAction("");
						counterMeasure.setActionOwner(new Long(-1));
						counterMeasure.setActionSite(new Long(-1));
						counterMeasure.setActionType("");
						counterMeasure.setActionSubType("");
						counterMeasure.setStatus("");
						counterMeasure.setDeadline(null);
					}
				}

			}
		}

		HazardHistory hist = new HazardHistory();
		hist.setHazardId(savedHazard.getHazardNumber());
		hist.setLevel(level);
		hist.setComments(histComment);
		hist.setStatus(histStatus);
		hist.setLastUpdatedBy(requestorName);
		hist.setUpdatedAt(new Date());
		hist.setUpdatedByUserId(requestor.getId());
		hazardHistRepo.save(hist);

		// Check if any action are open against the hazard while approving
		Integer cnt = actionRepo.getCountOfOpenActionsForHazard(savedHazard.getHazardNumber());

		if (cnt == 0) {
			// Update the incident status and update history

			if (savedHazard.getStatus().equals("L2_APPROVED")) {
				savedHazard.setStatus("_CLOSED");
				savedHazard.setUpdatedAt(new Date());
				savedHazard.setUpdatedBy(requestorName);
				hazardRepo.save(savedHazard);

				HazardHistory hzHist = new HazardHistory();
				hzHist.setUpdatedAt(new Date());
				hzHist.setLastUpdatedBy(requestorName);
				hzHist.setUpdatedByUserId(requestor.getId());
				hzHist.setHazardId(savedHazard.getHazardNumber());
				hzHist.setComments("Hazard Report Closed due no pending actions");
				hzHist.setStatus("_CLOSED");

				hazardHistRepo.save(hzHist);
			}
		}

		return "OK -" + savedHazard.getHazardNumber();
	}

	public String saveRiskAssessment(RiskAssessmentModel reqAssessment) {
		
		Users requestor = userRepo.findById(reqAssessment.getUserId());
		String requestorName = requestor.getFirstName() + " "+ requestor.getLastName();
		
		RiskAssessment riskAssessment = new RiskAssessment();
	
		if(reqAssessment.getGraId()!=null && reqAssessment.getGraId()!=0){
			riskAssessment = riskAssessmentRepo.findByGraId(reqAssessment.getGraId());
			
		}else{
			riskAssessment.setCreatedAt(new Date());
			riskAssessment.setCreatedBy(requestorName);
			riskAssessment.setStatus("L1_SAVED");
		}
		if(reqAssessment.getFilter().equalsIgnoreCase("SAVE") ){
			if(riskAssessment.getStatus().equalsIgnoreCase("L2_SUBMITTED") ){
				riskAssessment.setStatus("L2_EDITED");
			}else if(riskAssessment.getStatus().equalsIgnoreCase("L2_APPROVED")){
				riskAssessment.setStatus("L3_EDITED");
			}else if(riskAssessment.getStatus().equalsIgnoreCase("L3_APPROVED")){
				riskAssessment.setStatus("L4_EDITED");
			}else if(riskAssessment.getStatus().equalsIgnoreCase("L2_REJECTED")){
				riskAssessment.setStatus("L1_SAVED");
			}else if(riskAssessment.getStatus().equalsIgnoreCase("L3_REJECTED")){
				riskAssessment.setStatus("L2_EDITED");
			}else if(riskAssessment.getStatus().equalsIgnoreCase("L4_REJECTED")){
				riskAssessment.setStatus("L3_EDITED");
			}
		
		}else if(reqAssessment.getFilter().equalsIgnoreCase("REJECT") ){
			
			if(riskAssessment.getStatus().equalsIgnoreCase("L2_SUBMITTED") || riskAssessment.getStatus().equalsIgnoreCase("L2_EDITED")){
				riskAssessment.setStatus("L2_REJECTED");
			}else if(riskAssessment.getStatus().equalsIgnoreCase("L3_EDITED") || riskAssessment.getStatus().equalsIgnoreCase("L2_APPROVED")){
				riskAssessment.setStatus("L3_REJECTED");
			}else if(riskAssessment.getStatus().equalsIgnoreCase("L3_APPROVED") || riskAssessment.getStatus().equalsIgnoreCase("L3_EDITED")){
				riskAssessment.setStatus("L4_REJECTED");
			}
			String level = riskAssessment.getStatus().split("_")[0];
			Long newApproverId = graHistRepo.findSubmitterId(riskAssessment.getGraId(), level);
			riskAssessment.setCurrApproverId(newApproverId);
			
		}else if(reqAssessment.getFilter().equalsIgnoreCase("SUBMIT")){
			riskAssessment.setCurrApproverId(reqAssessment.getCurrApproverId());
			//Change the action status to assign
			if(riskAssessment.getStatus().equalsIgnoreCase("L1_SAVED") || riskAssessment.getStatus().equalsIgnoreCase("L2_REJECTED")){
				riskAssessment.setStatus("L2_SUBMITTED");
			}
		}else if(reqAssessment.getFilter().equalsIgnoreCase("APPROVE") ){
			riskAssessment.setCurrApproverId(reqAssessment.getCurrApproverId());
			if(riskAssessment.getStatus().equalsIgnoreCase("L2_SUBMITTED") || riskAssessment.getStatus().equalsIgnoreCase("L2_EDITED") || riskAssessment.getStatus().equalsIgnoreCase("L3_REJECTED")){
				riskAssessment.setStatus("L2_APPROVED");
			}else if(riskAssessment.getStatus().equalsIgnoreCase("L3_EDITED") || riskAssessment.getStatus().equalsIgnoreCase("L2_APPROVED") || riskAssessment.getStatus().equalsIgnoreCase("L4_REJECTED")){
				riskAssessment.setStatus("L3_APPROVED");
			}else if(riskAssessment.getStatus().equalsIgnoreCase("L3_APPROVED") || riskAssessment.getStatus().equalsIgnoreCase("L4_EDITED")){
				riskAssessment.setStatus("L4_APPROVED");
			}
		}
		riskAssessment.setAreaAssessed(reqAssessment.getAreaAssessed());
		riskAssessment.setAssessor(reqAssessment.getAssessor());
		riskAssessment.setOwner(reqAssessment.getOwner());
		riskAssessment.setGraDate(reqAssessment.getAssessmentDate());
		riskAssessment.setReviewDate(reqAssessment.getRevisionDate());
		riskAssessment.setSiteId(reqAssessment.getSiteId());
		riskAssessment.setUpdatedAt(new Date());
		riskAssessment.setUpdatedBy(requestorName);
		
		RiskAssessment savedRA  = riskAssessmentRepo.save(riskAssessment);
		
		if (savedRA.getStatus().equalsIgnoreCase("L2_SUBMITTED") || savedRA.getStatus().equalsIgnoreCase("L2_APPROVED")  
				|| savedRA.getStatus().equalsIgnoreCase("L3_APPROVED") 
				|| savedRA.getStatus().equalsIgnoreCase("L4_APPROVED")  || savedRA.getStatus().equalsIgnoreCase("L4_EDITED") ) {
			
			List<Action> actions = actionRepo.findActionsForRiskAssessment(savedRA.getGraId());
			for(Action action:actions){
				action.setStatus("ASSIGNED");
				action.setAssignedAt(new Date());
				action.setAssignedBy(requestorName);
				action.setUpdatedAt(new Date());
				action.setUpdatedBy(requestorName);
				action.setUpdatedByUserId(requestor.getId());
				actionRepo.save(action);
				
				ActionHistory actionHist = new ActionHistory();
				actionHist.setComments("Action " + action.getStatus());
				actionHist.setStatus(action.getStatus());
				actionHist.setReportId(savedRA.getGraId());
				actionHist.setActionSource("Risk Assessment");
				actionHist.setActionNumber(action.getId());
				actionHist.setUpdatedAt(new Date());
				actionHist.setLastUpdatedBy(requestor.getFirstName() + " "+ requestor.getLastName());
				actionHist.setUpdatedByUserId(requestor.getId());
				actionHist.setActionOwnerId(action.getActionOwner());
				actionHistRepo.save(actionHist);
			}
			
		}
		if(savedRA.getStatus().equalsIgnoreCase("L4_APPROVED")){
			int actions = actionRepo.findOpenActionsForRiskAssessment(savedRA.getGraId());
			if (actions == 0){
				savedRA.setStatus("_CLOSED");
				riskAssessmentRepo.save(savedRA);
			}
			
		}
		
		GRAStatus graStatus = graStatusRepo.findByStatus(riskAssessment.getStatus());
		GRAHistory hist = new GRAHistory();
		hist.setUpdatedAt(new Date());
		hist.setLastUpdatedBy(requestorName);
		hist.setUpdatedByUserId(requestor.getId());
		hist.setGraId(savedRA.getGraId());
		hist.setLevel(graStatus.getDisplayStatus());
		if(reqAssessment.getFilter().equalsIgnoreCase("REJECT")){
			hist.setComments(graStatus.getDescription()+":"+reqAssessment.getRejectComments());
		}else{
			hist.setComments(graStatus.getDescription());	
		}
		
		hist.setStatus(graStatus.getStatus().split("_")[1]);
		graHistRepo.save(hist);
		
		
		return "OK -"+savedRA.getGraId() + "-"+savedRA.getStatus();
		
	}
	/***
	 * 
	 * @param incidentNumber
	 * @param approverId
	 * @param comments
	 * @return
	 */
	public String deleteRiskHazard(Long hazardId, Long requestorId) {
		Users requestor = userRepo.findById(requestorId);
		RiskHazard hazard = riskHazardRepo.findOne(hazardId);
		List<Action> riskActions = actionRepo.findActionsForRiskHazard(hazardId);
		for(Action action : riskActions){
			action.setStatus("CANCELLED");
			action.setUpdatedAt(new Date());
			action.setUpdatedByUserId(requestorId);
			action.setUpdatedBy(requestor.getFirstName() + " "+ requestor.getLastName());
			actionRepo.save(action);
			
			ActionHistory actionHist = new ActionHistory();
			actionHist.setComments("Action " + action.getStatus());
			actionHist.setStatus(action.getStatus());
			actionHist.setReportId(hazard.getGraId());
			actionHist.setActionSource("Risk Assessment");
			actionHist.setActionNumber(action.getId());
			actionHist.setUpdatedAt(new Date());
			actionHist.setLastUpdatedBy(requestor.getFirstName() + " "+ requestor.getLastName());
			actionHist.setUpdatedByUserId(requestor.getId());
			actionHist.setActionOwnerId(action.getActionOwner());
			actionHistRepo.save(actionHist);
		}
	
		riskHazardRepo.delete(hazard);
		return "OK";
	}
	
	public String newVersionRiskAssessment(Long graId, Long userId){
		Users requestor = userRepo.findById(userId);
		String requestorName = requestor.getFirstName() + " "+ requestor.getLastName();
		
		RiskAssessment riskAssessment = new RiskAssessment();
		RiskAssessment oldRisk = riskAssessmentRepo.findByGraId(graId);
		riskAssessment.setAreaAssessed(oldRisk.getAreaAssessed());
		riskAssessment.setAssessor(oldRisk.getAssessor());
		riskAssessment.setGraDate(oldRisk.getGraDate());
		riskAssessment.setReviewDate(new Date());
		riskAssessment.setOwner(oldRisk.getOwner());
		riskAssessment.setSiteId(oldRisk.getSiteId());
		
		riskAssessment.setCreatedAt(new Date());
		riskAssessment.setCreatedBy(requestorName);
		riskAssessment.setUpdatedAt(new Date());
		riskAssessment.setUpdatedBy(requestorName);
		riskAssessment.setStatus("L1_SAVED");
		riskAssessment.setRefGraId(graId);
		riskAssessment.setCurrApproverId(null);
		
		RiskAssessment graSaved = riskAssessmentRepo.save(riskAssessment);
		
		List<RiskHazard> hazards = riskHazardRepo.findByGraId(graId);
		for(RiskHazard hazard : hazards){
			RiskHazard newRiskHazard = new RiskHazard();
			newRiskHazard.setCreatedAt(new Date());
			newRiskHazard.setCreatedBy(requestorName);
			newRiskHazard.setUpdatedAt(new Date());
			newRiskHazard.setUpdatedBy(requestorName);
			
			newRiskHazard.setIsFurtherActionRequired(false);
			
			newRiskHazard.setGraId(graSaved.getGraId());
			newRiskHazard.setExistingControls(hazard.getExistingControls());
			newRiskHazard.setHazardDetail(hazard.getHazardDetail());
			newRiskHazard.setImplementedWhen(hazard.getImplementedWhen());
			newRiskHazard.setImplementedWho(hazard.getImplementedWho());
			newRiskHazard.setNewRiskRatingL(hazard.getNewRiskRatingL());
			newRiskHazard.setNewRiskRatingS(hazard.getNewRiskRatingS());
			newRiskHazard.setRiskRatingL(hazard.getRiskRatingL());
			newRiskHazard.setRiskRatingS(hazard.getRiskRatingS());
			newRiskHazard.setPersonsAtRisk(hazard.getPersonsAtRisk());
			
			riskHazardRepo.save(newRiskHazard);
			
		}
		oldRisk.setIsArchived(true);
		oldRisk.setUpdatedAt(new Date());
		oldRisk.setUpdatedBy(requestorName);
		riskAssessmentRepo.save(oldRisk);
		
		GRAStatus graStatus = graStatusRepo.findByStatus(riskAssessment.getStatus());
		GRAHistory hist = new GRAHistory();
		hist.setUpdatedAt(new Date());
		hist.setLastUpdatedBy(requestorName);
		hist.setUpdatedByUserId(requestor.getId());
		hist.setGraId(graSaved.getGraId());
		hist.setLevel(graStatus.getDisplayStatus());
		hist.setComments("New Version created from RA"+ graId);
		hist.setStatus(graStatus.getStatus().split("_")[1]);
		graHistRepo.save(hist);
		
		GRAStatus oldGraStatus = graStatusRepo.findByStatus(oldRisk.getStatus());
		GRAHistory oldHist = new GRAHistory();
		oldHist.setUpdatedAt(new Date());
		oldHist.setLastUpdatedBy(requestorName);
		oldHist.setUpdatedByUserId(requestor.getId());
		oldHist.setGraId(oldRisk.getGraId());
		oldHist.setLevel(oldGraStatus.getDisplayStatus());
		oldHist.setComments("This RA is archived");
		oldHist.setStatus(oldGraStatus.getStatus().split("_")[1]);
		graHistRepo.save(oldHist);
		
		return "OK-"+graSaved.getGraId();
	
		
	}
	
	public String deleteRiskAssessment(Long graId, Long userId){
		
		//Mark Risk Assessment deleted
		//Mark Actions cancelled
		Users requestor = userRepo.findById(userId);
		String requestorName = requestor.getFirstName() + " "+ requestor.getLastName();
		
		
		RiskAssessment risk = riskAssessmentRepo.findByGraId(graId);
		
		risk.setStatus("_DELETED");
		risk.setUpdatedAt(new Date());
		risk.setUpdatedBy(requestorName);
		riskAssessmentRepo.save(risk);
		
		GRAStatus graStatus = graStatusRepo.findByStatus(risk.getStatus());
		GRAHistory hist = new GRAHistory();
		hist.setUpdatedAt(new Date());
		hist.setLastUpdatedBy(requestorName);
		hist.setUpdatedByUserId(requestor.getId());
		hist.setGraId(risk.getGraId());
		hist.setLevel(graStatus.getDisplayStatus());
		hist.setComments("Deleted "+ graId);
		hist.setStatus(graStatus.getStatus().split("_")[1]);
		graHistRepo.save(hist);
		
		//Updated the references
		riskAssessmentRepo.updateByRefGraId(graId, requestorName);
		
		List<Action> actions = actionRepo.findAllOpenActionsForRiskAssessment(graId);
		for(Action action:actions){
			action.setStatus("CANCELLED");
			action.setUpdatedByUserId(userId);
			action.setUpdatedAt(new Date());
			action.setUpdatedBy(requestorName);
			actionRepo.save(action);
			
			ActionHistory actionHist = new ActionHistory();
			actionHist.setComments("Action " + action.getStatus() + " due to associated RA Deleted");
			actionHist.setStatus(action.getStatus());
			actionHist.setReportId(graId);
			actionHist.setActionSource("Risk Assessment");
			actionHist.setActionNumber(action.getId());
			actionHist.setUpdatedAt(new Date());
			actionHist.setLastUpdatedBy(requestorName);
			actionHist.setUpdatedByUserId(requestor.getId());
			actionHist.setActionOwnerId(action.getActionOwner());
			actionHistRepo.save(actionHist);
			
		}
		
		return "OK";
		
	}
	/**
	 * 
	 * @param ipAddress
	 * @param siteId
	 * @return
	 */
	public boolean authenticateToken(String ipAddress, Long siteId) {
		boolean valid = false;
		try {

			HazardTabIPs hazardIp = hazardTabRepo.findByIpAddressAndSiteId(ipAddress, siteId);

			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");

			if (SecurityToken.getDecrypted(hazardIp.getSecurityToken())
					.startsWith(ipAddress + sdf.format(new Date()))) {
				valid = true;
			} else {
				logger.info("Security Token does not match" + hazardIp.getSecurityToken() + " " + ipAddress + " "
						+ sdf.format(new Date()));
			}
		} catch (Exception e) {
			logger.info("Exception is " + e.getMessage());
		}
		return valid;

	}

}
