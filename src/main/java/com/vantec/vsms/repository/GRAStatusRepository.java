package com.vantec.vsms.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import com.vantec.vsms.entity.GRAStatus;


@Component
public interface GRAStatusRepository extends JpaRepository<GRAStatus, Serializable>{
	
	GRAStatus findByStatus(String status);

}
