package com.vantec.vsms.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import com.vantec.vsms.entity.RiskHazard;


@Component
public interface RiskHazardRepository extends JpaRepository<RiskHazard, Serializable>{
	
	public RiskHazard findById(Long id);
	public List<RiskHazard> findByGraId(Long graId);

}
