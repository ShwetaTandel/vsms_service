package com.vantec.vsms.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.vantec.vsms.entity.RiskAssessment;


@Component
public interface RiskAssessmentRepository extends JpaRepository<RiskAssessment, Serializable>{
	
	public RiskAssessment findByGraId(Long graId);
	
	@Transactional
	@Modifying
	@Query("UPDATE RiskAssessment p SET p.refGraId = null , p.updatedBy = :user , p.updatedAt = now() where p.refGraId = :refGraId")
	Integer updateByRefGraId(@Param("refGraId") Long refGraId, @Param("user")String user);
	
	

}
