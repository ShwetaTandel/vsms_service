package com.vantec.vsms.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import com.vantec.vsms.entity.GRAHistory;


@Component
public interface GRAHistoryRepository extends JpaRepository<GRAHistory, Serializable>{
	
	@Query(value = "SELECT users.id FROM gra_history,users where gra_id =:graId and comments like 'Pending%'  and users.id = gra_history.updated_by_user_id and level =:level order by gra_history.id desc limit 1", nativeQuery = true)
	Long findSubmitterId(@Param("graId")Long graId, @Param("level")String level);

}
