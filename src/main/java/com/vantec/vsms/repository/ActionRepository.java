package com.vantec.vsms.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.vantec.vsms.entity.Action;



@Component
public interface ActionRepository extends JpaRepository<Action, Serializable>{
	
	@Query("SELECT a FROM Action a where a.reportId =:reportId and a.actionType != 'Incident-Containment' and a.reportSubId is null and a.actionSource='Incident'")
	List<Action> findOtherCounterMeasuresByIncident(@Param("reportId") Long  reportId);
	
	
	@Query("SELECT a FROM Action a where a.reportId =:reportId  and a.actionType = 'Incident-Containment' and a.actionSource = 'Incident'")
	List<Action> findContainmentActionsByIncident(@Param("reportId") Long  reportId);
	
	@Query("SELECT count(a) FROM Action a WHERE a.reportId =:reportId and a.actionSource = 'Incident' and a.status not in ('CLOSED', 'CANCELLED','') ")
    public Integer getCountOfOpenActionsForIncident(@Param("reportId") Long  reportId);
	
	@Query("SELECT count(a) FROM Action a WHERE a.reportId =:reportId and a.actionSource = 'Hazard' and a.status not in ('CLOSED', 'CANCELLED','') ")
    public Integer getCountOfOpenActionsForHazard(@Param("reportId") Long  reportId);
	
	
	@Query("SELECT a FROM Action a where a.reportId =:reportId and a.actionType = 'Hazard-Countermeasure' and a.actionSource='Hazard'")
	List<Action> findCounterMeasuresByHazard(@Param("reportId") Long  reportId);
	
	@Query("SELECT a FROM Action a where a.reportId =:reportId  and a.actionType = 'Incident-Countermeasure' and a.actionSource = 'Incident' and a.reportSubId =:whyId")
	List<Action> findActionsByWhyId(@Param("reportId") Long  reportId, @Param("whyId") Long  whyId);
	
	@Query("SELECT a FROM Action a where a.reportSubId =:subReportId and a.actionSource = 'Risk Assessment'")
	List<Action> findActionsForRiskHazard(@Param("subReportId") Long  subReportId);

	@Query("SELECT a FROM Action a where a.reportId =:reportId and a.actionSource = 'Risk Assessment' and a.status ='CREATED'")
	List<Action> findActionsForRiskAssessment(@Param("reportId") Long  reportId);
	
	@Query("SELECT count(a) FROM Action a where a.reportId =:reportId and a.actionSource = 'Risk Assessment' and a.status not in ('CLOSED', 'CANCELLED','')")
	public Integer findOpenActionsForRiskAssessment(@Param("reportId") Long  reportId);
	
	@Query("SELECT a  FROM Action a where a.reportId =:reportId and a.actionSource = 'Risk Assessment' and a.status not in ('CLOSED', 'CANCELLED','')")
	List<Action>  findAllOpenActionsForRiskAssessment(@Param("reportId") Long  reportId);
	
	
	@Transactional
	@Modifying
	@Query("UPDATE Action a SET a.status = 'CANCELLED' , a.updatedBy = :user , a.updatedAt = now(), a.updatedByUserId= :userId where a.reportId =:reportId and a.actionSource = 'Risk Assessment' and a.status not in ('CLOSED', 'CANCELLED','')")
	Integer markActionsCancelledByReportId(@Param("reportId") Long reportId, @Param("user")String user, @Param("userId") Long userId);
}
