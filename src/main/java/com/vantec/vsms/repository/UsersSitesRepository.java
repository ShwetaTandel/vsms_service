package com.vantec.vsms.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Component;

import com.vantec.vsms.entity.UserSite;


@Component
public interface UsersSitesRepository extends JpaRepository<UserSite, Serializable>{
	
	@Modifying
	void deleteByUserId(Long userId);

}
