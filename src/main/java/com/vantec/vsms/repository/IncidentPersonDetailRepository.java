package com.vantec.vsms.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import com.vantec.vsms.entity.Incident;
import com.vantec.vsms.entity.IncidentPersonDetail;


@Component
public interface IncidentPersonDetailRepository extends JpaRepository<IncidentPersonDetail, Serializable>{
	
	IncidentPersonDetail findByPersonNameAndIncident(String persoName, Incident incient);

}
