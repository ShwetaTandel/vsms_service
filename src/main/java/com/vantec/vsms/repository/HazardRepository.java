package com.vantec.vsms.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import com.vantec.vsms.entity.Hazard;


@Component
public interface HazardRepository extends JpaRepository<Hazard, Serializable>{

}
