package com.vantec.vsms.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import com.vantec.vsms.entity.IncidentHistory;


@Component
public interface IncidentHistoryRepository extends JpaRepository<IncidentHistory, Serializable>{

}
