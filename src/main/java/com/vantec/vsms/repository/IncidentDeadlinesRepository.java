package com.vantec.vsms.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import com.vantec.vsms.entity.IncidentDeadlines;



@Component
public interface IncidentDeadlinesRepository extends JpaRepository<IncidentDeadlines, Serializable>{
	
	
	IncidentDeadlines findByIncidentNumber(Long incidentNumber);

}
