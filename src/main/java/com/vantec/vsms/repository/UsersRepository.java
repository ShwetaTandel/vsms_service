package com.vantec.vsms.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import com.vantec.vsms.entity.Users;


@Component
public interface UsersRepository extends JpaRepository<Users, Serializable>{
	Users findById(Long userId);
	Users findByEmailId(String email);

}
