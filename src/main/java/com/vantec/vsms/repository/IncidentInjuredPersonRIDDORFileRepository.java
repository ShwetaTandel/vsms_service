package com.vantec.vsms.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import com.vantec.vsms.entity.IncidentInjuredPersonRIDDORFile;


@Component
public interface IncidentInjuredPersonRIDDORFileRepository extends JpaRepository<IncidentInjuredPersonRIDDORFile, Serializable>{
	
	public List<IncidentInjuredPersonRIDDORFile> findByIncidentNumber( Long number);
	
}
