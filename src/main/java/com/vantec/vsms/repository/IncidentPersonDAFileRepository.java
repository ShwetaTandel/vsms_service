package com.vantec.vsms.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import com.vantec.vsms.entity.IncidentPersonDAFile;


@Component
public interface IncidentPersonDAFileRepository extends JpaRepository<IncidentPersonDAFile, Serializable>{
	
	public List<IncidentPersonDAFile> findByIncidentNumber( Long number);
	
}
