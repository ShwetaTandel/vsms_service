package com.vantec.vsms.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import com.vantec.vsms.entity.Incident;
import com.vantec.vsms.entity.IncidentInjuredPersonDetail;


@Component
public interface IncidentInjuredPersonDetailRepository extends JpaRepository<IncidentInjuredPersonDetail, Serializable>{
	
List<IncidentInjuredPersonDetail> findByIncident(Incident incident);	

}
