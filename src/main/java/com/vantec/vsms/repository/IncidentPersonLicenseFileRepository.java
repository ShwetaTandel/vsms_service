package com.vantec.vsms.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import com.vantec.vsms.entity.IncidentPersonLicenseFile;


@Component
public interface IncidentPersonLicenseFileRepository extends JpaRepository<IncidentPersonLicenseFile, Serializable>{
	
	public List<IncidentPersonLicenseFile> findByLicenseId(Long id);
	
}
