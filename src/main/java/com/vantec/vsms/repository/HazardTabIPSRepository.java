package com.vantec.vsms.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import com.vantec.vsms.entity.HazardTabIPs;



@Component
public interface HazardTabIPSRepository extends JpaRepository<HazardTabIPs, Serializable>{
	public HazardTabIPs findByIpAddressAndSiteId(String ipAddress, Long SiteId);
	
}
