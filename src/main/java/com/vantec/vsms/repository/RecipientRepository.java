package com.vantec.vsms.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import com.vantec.vsms.entity.Approvers;
import com.vantec.vsms.entity.Recipients;


@Component
public interface RecipientRepository extends JpaRepository<Recipients, Serializable>{
	
	List<Approvers> findByUserId(@Param("userId") Long userId);

}
