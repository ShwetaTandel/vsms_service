package com.vantec.vsms.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import com.vantec.vsms.entity.IncidentFullInvestigationDetail;


@Component
public interface IncidentFullInvestigationDetailRepository extends JpaRepository<IncidentFullInvestigationDetail, Serializable>{
	

	
}
