package com.vantec.vsms.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import com.vantec.vsms.entity.Incident;
import com.vantec.vsms.entity.IncidentPersonLicense;


@Component
public interface IncidentPersonLicenseRepository extends JpaRepository<IncidentPersonLicense, Serializable>{
	List<IncidentPersonLicense> findAllByIncident(Incident incident);
	
}
