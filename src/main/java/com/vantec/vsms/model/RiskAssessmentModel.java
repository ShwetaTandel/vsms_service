package com.vantec.vsms.model;

import java.util.Date;

public class RiskAssessmentModel {
	
	private Long graId;
	private Long refGraId;
	private Long siteId;
	private String  areaAssessed;
	private Date assessmentDate;
	private Date revisionDate;
	private String assessor;
	private String owner;
	private String greference;
	private Long currApproverId;
	private Long userId;
	private String status;
	private String filter;
	private String rejectComments;
	
	
	public String getRejectComments() {
		return rejectComments;
	}
	public void setRejectComments(String rejectComments) {
		this.rejectComments = rejectComments;
	}
	public String getFilter() {
		return filter;
	}
	public void setFilter(String filter) {
		this.filter = filter;
	}
	public Long getGraId() {
		return graId;
	}
	public void setGraId(Long graId) {
		this.graId = graId;
	}
	
	
	public Long getRefGraId() {
		return refGraId;
	}
	public void setRefGraId(Long refGraId) {
		this.refGraId = refGraId;
	}
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	public Long getSiteId() {
		return siteId;
	}
	public void setSiteId(Long siteId) {
		this.siteId = siteId;
	}
	public String getAreaAssessed() {
		return areaAssessed;
	}
	public void setAreaAssessed(String areaAssessed) {
		this.areaAssessed = areaAssessed;
	}
	public Date getAssessmentDate() {
		return assessmentDate;
	}
	public void setAssessmentDate(Date assessmentDate) {
		this.assessmentDate = assessmentDate;
	}
	public Date getRevisionDate() {
		return revisionDate;
	}
	public void setRevisionDate(Date revisionDate) {
		this.revisionDate = revisionDate;
	}
	public String getAssessor() {
		return assessor;
	}
	public void setAssessor(String assessor) {
		this.assessor = assessor;
	}
	public String getGreference() {
		return greference;
	}
	public void setGreference(String greference) {
		this.greference = greference;
	}
	public Long getCurrApproverId() {
		return currApproverId;
	}
	public void setCurrApproverId(Long currApproverId) {
		this.currApproverId = currApproverId;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	

}
