package com.vantec.vsms.model;

import java.util.Date;

public class IncidentLicenseModel {

	private Long licenseId;
	private String personName;
	private Date licenseExpiryDate;
	private Boolean isLicenseValid;
	
	private String fileName;
	
	public String getPersonName() {
		return personName;
	}
	public void setPersonName(String personName) {
		this.personName = personName;
	}
	public Date getLicenseExpiryDate() {
		return licenseExpiryDate;
	}
	public void setLicenseExpiryDate(Date licenseExpiryDate) {
		this.licenseExpiryDate = licenseExpiryDate;
	}
	public Boolean getIsLicenseValid() {
		return isLicenseValid;
	}
	public void setIsLicenseValid(Boolean isLicenseValid) {
		this.isLicenseValid = isLicenseValid;
	}
	public Long getLicenseId() {
		return licenseId;
	}
	public void setLicenseId(Long licenseId) {
		this.licenseId = licenseId;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
}
