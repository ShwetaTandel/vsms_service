package com.vantec.vsms.model;

import java.util.ArrayList;
import java.util.List;

public class ApproverModel {

	private Long siteId;
	private String requestor;
	private Long reportId;
	private String reportType;
	private String approverComments;
	private Long currApproverId;
	private Long requestorId;
	
	
	public Long getRequestorId() {
		return requestorId;
	}
	public void setRequestorId(Long requestorId) {
		this.requestorId = requestorId;
	}
	public Long getCurrApproverId() {
		return currApproverId;
	}
	public void setCurrApproverId(Long currApproverId) {
		this.currApproverId = currApproverId;
	}
	public Long getReportId() {
		return reportId;
	}
	public void setReportId(Long reportId) {
		this.reportId = reportId;
	}
	public String getReportType() {
		return reportType;
	}
	public void setReportType(String reportType) {
		this.reportType = reportType;
	}
	public String getApproverComments() {
		return approverComments;
	}
	public void setApproverComments(String approverComments) {
		this.approverComments = approverComments;
	}
	// Level - User Id format
	private List<String> approvers = new ArrayList<>();
	public Long getSiteId() {
		return siteId;
	}
	public void setSiteId(Long siteId) {
		this.siteId = siteId;
	}

	public String getRequestor() {
		return requestor;
	}
	public void setRequestor(String requestor) {
		this.requestor = requestor;
	}
	public List<String> getApprovers() {
		return approvers;
	}
	public void setApprovers(List<String> levelUsersList) {
		this.approvers = levelUsersList;
	}
	
}


