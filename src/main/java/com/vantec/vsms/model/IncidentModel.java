package com.vantec.vsms.model;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

public class IncidentModel {
	
	private String filter;
	
	private Long incidentNumber;
	private Long siteId;
	private Long graId;
	private String location;
	private String locationOther;
	private Date incidentDateTime;
	private String incidentType;
	private String incidentBrief;
	private String incidentDetail;
	private String actionType;
	private String actionTypeOther;
	private String area;
	private String lighting;
	private String temperature;
	private String noise;
	private String surfaceType;
	private String surfaceCondition;
	private Boolean isPersonInvolved;
	private Boolean isActivityStandardProcess;
	private String stillageType;
	private Float estimatedCost;
	private String immediateCause;
	private String keyPoints;
	private String mainMessage;
	private String question1Text;
	private String question1Answer;
	private String question2Text;
	private String question2Answer;
	private String question3Text;
	private String question3Answer;
	private String question4Text;
	private String question4Answer;
	private Long currApproverId;
	private List<Long> deletedLicenseIds;
	private String status;
	private Long requestorId;
	private String approverComments;
	private FIDetailsModel fiDetails;
	private String category;
	private String categoryLevel;
	private String type;
	private String actOrCondition;
	private Boolean isDangerousOccurence;
	private Boolean isClosed;
	private String hsSummary;
	private String classification;
	private String eventSeverity;
	private Float estimatedCostOther;


	
	
	public String getEventSeverity() {
		return eventSeverity;
	}
	public void setEventSeverity(String eventSeverity) {
		this.eventSeverity = eventSeverity;
	}
	public Float getEstimatedCostOther() {
		return estimatedCostOther;
	}
	public void setEstimatedCostOther(Float estimatedCostOther) {
		this.estimatedCostOther = estimatedCostOther;
	}
	public Long getGraId() {
		return graId;
	}
	public void setGraId(Long graId) {
		this.graId = graId;
	}
	public String getClassification() {
		return classification;
	}
	public void setClassification(String classification) {
		this.classification = classification;
	}
	private List<IncidentInjuredPersonModel> injuredPersons = new ArrayList<IncidentInjuredPersonModel>();
	public FIDetailsModel getFiDetails() {
		return fiDetails;
	}
	public void setFiDetails(FIDetailsModel fiDetails) {
		this.fiDetails = fiDetails;
	}
	private List<IncidentPersonModel>  personInvolved = new ArrayList<IncidentPersonModel>();
	private List<ActionModel>  actions = new ArrayList<ActionModel>();
	
	private List<IncidentLicenseModel>  licenseDetails = new ArrayList<IncidentLicenseModel>();
	
	
	
	
	public String getIncidentBrief() {
		return incidentBrief;
	}
	public void setIncidentBrief(String incidentBrief) {
		this.incidentBrief = incidentBrief;
	}
	public String getHsSummary() {
		return hsSummary;
	}
	public void setHsSummary(String hsSummary) {
		this.hsSummary = hsSummary;
	}
	public Boolean getIsClosed() {
		return isClosed;
	}
	public void setIsClosed(Boolean isClosed) {
		this.isClosed = isClosed;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getCategoryLevel() {
		return categoryLevel;
	}
	public void setCategoryLevel(String categoryLevel) {
		this.categoryLevel = categoryLevel;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getActOrCondition() {
		return actOrCondition;
	}
	public void setActOrCondition(String actOrCondition) {
		this.actOrCondition = actOrCondition;
	}
	public Boolean getIsDangerousOccurence() {
		return isDangerousOccurence;
	}
	public void setIsDangerousOccurence(Boolean isDangerousOccurence) {
		this.isDangerousOccurence = isDangerousOccurence;
	}
	public List<IncidentInjuredPersonModel> getInjuredPersons() {
		return injuredPersons;
	}
	public void setInjuredPersons(List<IncidentInjuredPersonModel> injuredPersons) {
		this.injuredPersons = injuredPersons;
	}
	public Boolean getIsPersonInvolved() {
		return isPersonInvolved;
	}
	public void setIsPersonInvolved(Boolean isPersonInvolved) {
		this.isPersonInvolved = isPersonInvolved;
	}
	public String getApproverComments() {
		return approverComments;
	}
	public void setApproverComments(String approverComments) {
		this.approverComments = approverComments;
	}
	
	public List<IncidentLicenseModel> getLicenseDetails() {
		return licenseDetails;
	}
	public void setLicenseDetails(List<IncidentLicenseModel> licenseDetails) {
		this.licenseDetails = licenseDetails;
	}
	
	
	
	public List<Long> getDeletedLicenseIds() {
		return deletedLicenseIds;
	}
	public void setDeletedLicenseIds(List<Long> deletedLicenseIds) {
		this.deletedLicenseIds = deletedLicenseIds;
	}
	public String getFilter() {
		return filter;
	}
	public void setFilter(String filter) {
		this.filter = filter;
	}
	public String getIncidentDetail() {
		return incidentDetail;
	}
	public void setIncidentDetail(String incidentDetail) {
		this.incidentDetail = incidentDetail;
	}
	public List<IncidentPersonModel> getPersonInvolved() {
		return personInvolved;
	}
	public void setPersonInvolved(List<IncidentPersonModel> personInvolved) {
		this.personInvolved = personInvolved;
	}
	public List<ActionModel> getActions() {
		return actions;
	}
	public void setActions(List<ActionModel> actions) {
		this.actions = actions;
	}
	public Long getIncidentNumber() {
		return incidentNumber;
	}
	public void setIncidentNumber(Long incidentNumber) {
		this.incidentNumber = incidentNumber;
	}
	
	public Long getSiteId() {
		return siteId;
	}
	public void setSiteId(Long siteId) {
		this.siteId = siteId;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getLocationOther() {
		return locationOther;
	}
	public void setLocationOther(String locationOther) {
		this.locationOther = locationOther;
	}
	public Date getIncidentDateTime() {
		return incidentDateTime;
	}
	public void setIncidentDateTime(Date incidentDateTime) {
		this.incidentDateTime = incidentDateTime;
	}
	public String getIncidentType() {
		return incidentType;
	}
	public void setIncidentType(String incidentType) {
		this.incidentType = incidentType;
	}
	public String getDescription() {
		return incidentDetail;
	}
	public void setDescription(String description) {
		this.incidentDetail = description;
	}
	public String getActionType() {
		return actionType;
	}
	public void setActionType(String actionType) {
		this.actionType = actionType;
	}
	public String getActionTypeOther() {
		return actionTypeOther;
	}
	public void setActionTypeOther(String actionTypeOther) {
		this.actionTypeOther = actionTypeOther;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getLighting() {
		return lighting;
	}
	public void setLighting(String lighting) {
		this.lighting = lighting;
	}
	public String getTemperature() {
		return temperature;
	}
	public void setTemperature(String temperature) {
		this.temperature = temperature;
	}
	public String getNoise() {
		return noise;
	}
	public void setNoise(String noise) {
		this.noise = noise;
	}
	public String getSurfaceType() {
		return surfaceType;
	}
	public void setSurfaceType(String surfaceType) {
		this.surfaceType = surfaceType;
	}
	public String getSurfaceCondition() {
		return surfaceCondition;
	}
	public void setSurfaceCondition(String surfaceCondition) {
		this.surfaceCondition = surfaceCondition;
	}
	public Boolean getIsActivityStandardProcess() {
		return isActivityStandardProcess;
	}
	public void setIsActivityStandardProcess(Boolean isActivityStandardProcess) {
		this.isActivityStandardProcess = isActivityStandardProcess;
	}
	public String getStillageType() {
		return stillageType;
	}
	public void setStillageType(String stillageType) {
		this.stillageType = stillageType;
	}
	public Float getEstimatedCost() {
		return estimatedCost;
	}
	public void setEstimatedCost(Float estimatedCost) {
		this.estimatedCost = estimatedCost;
	}
	public String getImmediateCause() {
		return immediateCause;
	}
	public void setImmediateCause(String immediateCause) {
		this.immediateCause = immediateCause;
	}
	public String getKeyPoints() {
		return keyPoints;
	}
	public void setKeyPoints(String keyPoints) {
		this.keyPoints = keyPoints;
	}
	public String getMainMessage() {
		return mainMessage;
	}
	public void setMainMessage(String mainMessage) {
		this.mainMessage = mainMessage;
	}
	public String getQuestion1Text() {
		return question1Text;
	}
	public void setQuestion1Text(String question1Text) {
		this.question1Text = question1Text;
	}
	public String getQuestion1Answer() {
		return question1Answer;
	}
	public void setQuestion1Answer(String question1Answer) {
		this.question1Answer = question1Answer;
	}
	public String getQuestion2Text() {
		return question2Text;
	}
	public void setQuestion2Text(String question2Text) {
		this.question2Text = question2Text;
	}
	public String getQuestion2Answer() {
		return question2Answer;
	}
	public void setQuestion2Answer(String question2Answer) {
		this.question2Answer = question2Answer;
	}
	public String getQuestion3Text() {
		return question3Text;
	}
	public void setQuestion3Text(String question3Text) {
		this.question3Text = question3Text;
	}
	public String getQuestion3Answer() {
		return question3Answer;
	}
	public void setQuestion3Answer(String question3Answer) {
		this.question3Answer = question3Answer;
	}
	public String getQuestion4Text() {
		return question4Text;
	}
	public void setQuestion4Text(String question4Text) {
		this.question4Text = question4Text;
	}
	public String getQuestion4Answer() {
		return question4Answer;
	}
	public void setQuestion4Answer(String question4Answer) {
		this.question4Answer = question4Answer;
	}
	public Long getCurrApproverId() {
		return currApproverId;
	}
	public void setCurrApproverId(Long currApproverId) {
		this.currApproverId = currApproverId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Long getRequestorId() {
		return requestorId;
	}
	public void setRequestorId(Long requestorId) {
		this.requestorId = requestorId;
	}
	public IncidentLicenseModel getPersonLicense(String personName){
		for(IncidentLicenseModel temp :licenseDetails){
			if(temp.getPersonName()!=null && temp.getPersonName().equals(personName)){
				return temp;
			}
		}
		return null;
	}
	
	
}
