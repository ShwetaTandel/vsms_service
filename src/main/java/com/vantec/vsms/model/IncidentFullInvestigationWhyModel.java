package com.vantec.vsms.model;

import java.util.ArrayList;
import java.util.List;

public class IncidentFullInvestigationWhyModel {
	
	private Long id;
	private String why1;
	private String why2;
	private String why3;
	private String why4;
	private String why5;
	private String rootCauseA;
	private String rootCauseB;
	private String rootCauseC;
	private List<ActionModel>  counterMeasures = new ArrayList<ActionModel>();
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getWhy1() {
		return why1;
	}
	public void setWhy1(String why1) {
		this.why1 = why1;
	}
	public String getWhy2() {
		return why2;
	}
	public void setWhy2(String why2) {
		this.why2 = why2;
	}
	public String getWhy3() {
		return why3;
	}
	public void setWhy3(String why3) {
		this.why3 = why3;
	}
	public String getWhy4() {
		return why4;
	}
	public void setWhy4(String why4) {
		this.why4 = why4;
	}
	public String getWhy5() {
		return why5;
	}
	public void setWhy5(String why5) {
		this.why5 = why5;
	}
	public String getRootCauseA() {
		return rootCauseA;
	}
	public void setRootCauseA(String rootCauseA) {
		this.rootCauseA = rootCauseA;
	}
	public String getRootCauseB() {
		return rootCauseB;
	}
	public void setRootCauseB(String rootCauseB) {
		this.rootCauseB = rootCauseB;
	}
	public String getRootCauseC() {
		return rootCauseC;
	}
	public void setRootCauseC(String rootCauseC) {
		this.rootCauseC = rootCauseC;
	}
	public List<ActionModel> getCounterMeasures() {
		return counterMeasures;
	}
	public void setCounterMeasures(List<ActionModel> counterMeasures) {
		this.counterMeasures = counterMeasures;
	}
	

	
	

}
