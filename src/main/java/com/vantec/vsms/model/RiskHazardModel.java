package com.vantec.vsms.model;

import java.util.ArrayList;
import java.util.List;

public class RiskHazardModel {

	
	
	private Long hazardId;
	private Long graId;
	private Integer graVersion;
	private String hazardDetails;
	private String implementedWho;
	private String implementedWhen;
	private String personsAtRisk;
	private String existingControls;
	private Integer riskRatingS;
	private Integer riskRatingL;
	private Integer newRiskRatingS;
	private Integer newRiskRatingL;
	private Long userId;
	private Boolean isFurtherActionsRequired;
	private List<ActionModel>  furtherActions = new ArrayList<ActionModel>();
	private RiskAssessmentModel raModel;
	
	public RiskAssessmentModel getRaModel() {
		return raModel;
	}

	public void setRaModel(RiskAssessmentModel raModel) {
		this.raModel = raModel;
	}

	public Boolean getIsFurtherActionsRequired() {
		return isFurtherActionsRequired;
	}

	public void setIsFurtherActionsRequired(Boolean isFurtherActionsRequired) {
		this.isFurtherActionsRequired = isFurtherActionsRequired;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	

	public Integer getGraVersion() {
		return graVersion;
	}

	public void setGraVersion(Integer graVersion) {
		this.graVersion = graVersion;
	}

	public Long getHazardId() {
		return hazardId;
	}

	public void setHazardId(Long hazardId) {
		this.hazardId = hazardId;
	}

	public Long getGraId() {
		return graId;
	}

	public void setGraId(Long graId) {
		this.graId = graId;
	}

	public String getHazardDetails() {
		return hazardDetails;
	}

	public void setHazardDetails(String hazardDetails) {
		this.hazardDetails = hazardDetails;
	}

	public String getImplementedWho() {
		return implementedWho;
	}

	public void setImplementedWho(String implementedWho) {
		this.implementedWho = implementedWho;
	}

	public String getImplementedWhen() {
		return implementedWhen;
	}

	public void setImplementedWhen(String implementedWhen) {
		this.implementedWhen = implementedWhen;
	}

	public String getPersonsAtRisk() {
		return personsAtRisk;
	}

	public void setPersonsAtRisk(String personsAtRisk) {
		this.personsAtRisk = personsAtRisk;
	}

	public String getExistingControls() {
		return existingControls;
	}

	public void setExistingControls(String existingControls) {
		this.existingControls = existingControls;
	}

	public Integer getRiskRatingS() {
		return riskRatingS;
	}

	public void setRiskRatingS(Integer riskRatingS) {
		this.riskRatingS = riskRatingS;
	}

	public Integer getRiskRatingL() {
		return riskRatingL;
	}

	public void setRiskRatingL(Integer riskRatingL) {
		this.riskRatingL = riskRatingL;
	}

	public Integer getNewRiskRatingS() {
		return newRiskRatingS;
	}

	public void setNewRiskRatingS(Integer newRiskRatingS) {
		this.newRiskRatingS = newRiskRatingS;
	}

	public Integer getNewRiskRatingL() {
		return newRiskRatingL;
	}

	public void setNewRiskRatingL(Integer newRiskRatingL) {
		this.newRiskRatingL = newRiskRatingL;
	}

	public List<ActionModel> getFurtherActions() {
		return furtherActions;
	}

	public void setFurtherActions(List<ActionModel> furtherActions) {
		this.furtherActions = furtherActions;
	}
	
	
	

}
