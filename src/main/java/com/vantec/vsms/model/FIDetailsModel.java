package com.vantec.vsms.model;

import java.util.ArrayList;
import java.util.List;

public class FIDetailsModel {
	
	private Long id;
	private String eventSummary;
	private Boolean isSSOWSuitable;
	private String ssowComments;
	private Boolean isRASuitable;
	private String raComments;
	private Boolean isMHETrainingRequired;
	private String personConcentraion;
	private String personExperience;
	private String personTraining;
	private String machineCondition;
	private String machineMaintenance;
	private String machineFacility;
	private String methodSSOW;
	private String methodRuling;
	private String methodRiskAssessment;
	private String environmentFloor;
	private String environmentLighting;
	private String environmentCondition;
	private String vantecWayDecision;
	private String vantecWayDecisionTree;
	private String vantecWayComments;
	private String investigationTeamNames;
	private String investigationTeamPostion;
	
	public String getInvestigationTeamNames() {
		return investigationTeamNames;
	}
	public void setInvestigationTeamNames(String investigationTeamNames) {
		this.investigationTeamNames = investigationTeamNames;
	}

	public String getInvestigationTeamPostion() {
		return investigationTeamPostion;
	}
	public void setInvestigationTeamPostion(String investigationTeamPostion) {
		this.investigationTeamPostion = investigationTeamPostion;
	}
	public String getVantecWayComments() {
		return vantecWayComments;
	}
	public void setVantecWayComments(String vantecWayComments) {
		this.vantecWayComments = vantecWayComments;
	}
	public String getVantecWayDecision() {
		return vantecWayDecision;
	}
	public void setVantecWayDecision(String vantecWayDecision) {
		this.vantecWayDecision = vantecWayDecision;
	}
	public String getVantecWayDecisionTree() {
		return vantecWayDecisionTree;
	}
	public void setVantecWayDecisionTree(String vantecWayDecisionTree) {
		this.vantecWayDecisionTree = vantecWayDecisionTree;
	}
	private List<IncidentFullInvestigationWhyModel>  whyModels = new ArrayList<IncidentFullInvestigationWhyModel>();
	private List<ActionModel>  otherCounterMeasures = new ArrayList<ActionModel>();
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public List<ActionModel> getOtherCounterMeasures() {
		return otherCounterMeasures;
	}
	public void setOtherCounterMeasures(List<ActionModel> counterMeasures) {
		this.otherCounterMeasures = counterMeasures;
	}
	public List<IncidentFullInvestigationWhyModel> getWhyModels() {
		return whyModels;
	}
	public void setWhyModels(List<IncidentFullInvestigationWhyModel> whyModels) {
		this.whyModels = whyModels;
	}
	public String getEventSummary() {
		return eventSummary;
	}
	public void setEventSummary(String eventSummary) {
		this.eventSummary = eventSummary;
	}
	public Boolean getIsSSOWSuitable() {
		return isSSOWSuitable;
	}
	public void setIsSSOWSuitable(Boolean isSSOWSuitable) {
		this.isSSOWSuitable = isSSOWSuitable;
	}
	public String getSsowComments() {
		return ssowComments;
	}
	public void setSsowComments(String ssowComments) {
		this.ssowComments = ssowComments;
	}
	public Boolean getIsRASuitable() {
		return isRASuitable;
	}
	public void setIsRASuitable(Boolean isRASuitable) {
		this.isRASuitable = isRASuitable;
	}
	public String getRaComments() {
		return raComments;
	}
	public void setRaComments(String raComments) {
		this.raComments = raComments;
	}
	public Boolean getIsMHETrainingRequired() {
		return isMHETrainingRequired;
	}
	public void setIsMHETrainingRequired(Boolean isMHETrainingRequired) {
		this.isMHETrainingRequired = isMHETrainingRequired;
	}
	public String getPersonConcentraion() {
		return personConcentraion;
	}
	public void setPersonConcentraion(String personConcentraion) {
		this.personConcentraion = personConcentraion;
	}
	public String getPersonExperience() {
		return personExperience;
	}
	public void setPersonExperience(String personExperience) {
		this.personExperience = personExperience;
	}
	public String getPersonTraining() {
		return personTraining;
	}
	public void setPersonTraining(String personTraining) {
		this.personTraining = personTraining;
	}
	public String getMachineCondition() {
		return machineCondition;
	}
	public void setMachineCondition(String machineCondition) {
		this.machineCondition = machineCondition;
	}
	public String getMachineMaintenance() {
		return machineMaintenance;
	}
	public void setMachineMaintenance(String machineMaintenance) {
		this.machineMaintenance = machineMaintenance;
	}
	public String getMachineFacility() {
		return machineFacility;
	}
	public void setMachineFacility(String machineFacility) {
		this.machineFacility = machineFacility;
	}
	public String getMethodSSOW() {
		return methodSSOW;
	}
	public void setMethodSSOW(String methodSSOW) {
		this.methodSSOW = methodSSOW;
	}
	public String getMethodRuling() {
		return methodRuling;
	}
	public void setMethodRuling(String methodRuling) {
		this.methodRuling = methodRuling;
	}
	public String getMethodRiskAssessment() {
		return methodRiskAssessment;
	}
	public void setMethodRiskAssessment(String methodRiskAssessment) {
		this.methodRiskAssessment = methodRiskAssessment;
	}
	public String getEnvironmentFloor() {
		return environmentFloor;
	}
	public void setEnvironmentFloor(String environmentFloor) {
		this.environmentFloor = environmentFloor;
	}
	public String getEnvironmentLighting() {
		return environmentLighting;
	}
	public void setEnvironmentLighting(String environmentLighting) {
		this.environmentLighting = environmentLighting;
	}
	public String getEnvironmentCondition() {
		return environmentCondition;
	}
	public void setEnvironmentCondition(String environmentCondition) {
		this.environmentCondition = environmentCondition;
	}

}
