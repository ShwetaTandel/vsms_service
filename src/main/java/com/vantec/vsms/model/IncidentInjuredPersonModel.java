package com.vantec.vsms.model;

import java.util.Date;

public class IncidentInjuredPersonModel {
	
	
	private Long injuredPersonId;
	private String injuredPersonName;
	private Date hseReportedDate;
	private String treatmentAndDetails;
	private Boolean isRIDDOR;
	private String riddorReference;
	private Integer daysLost;
	public Long getInjuredPersonId() {
		return injuredPersonId;
	}
	public void setInjuredPersonId(Long injuredPersonId) {
		this.injuredPersonId = injuredPersonId;
	}
	public String getInjuredPersonName() {
		return injuredPersonName;
	}
	public void setInjuredPersonName(String injuredPersonName) {
		this.injuredPersonName = injuredPersonName;
	}
	public Date getHseReportedDate() {
		return hseReportedDate;
	}
	public void setHseReportedDate(Date hseReportedDate) {
		this.hseReportedDate = hseReportedDate;
	}
	public String getTreatmentAndDetails() {
		return treatmentAndDetails;
	}
	public void setTreatmentAndDetails(String treatmentAndDetails) {
		this.treatmentAndDetails = treatmentAndDetails;
	}
	public Boolean getIsRIDDOR() {
		return isRIDDOR;
	}
	public void setIsRIDDOR(Boolean isRIDDOR) {
		this.isRIDDOR = isRIDDOR;
	}
	public String getRiddorReference() {
		return riddorReference;
	}
	public void setRiddorReference(String riddorReference) {
		this.riddorReference = riddorReference;
	}
	public Integer getDaysLost() {
		return daysLost;
	}
	public void setDaysLost(Integer daysLost) {
		this.daysLost = daysLost;
	}
}
