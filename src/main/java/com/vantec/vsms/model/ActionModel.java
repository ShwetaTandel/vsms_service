package com.vantec.vsms.model;

import java.util.Date;

public class ActionModel {

	private Long id;
	private String action;
	private Long actionSite;
	
	private String actionType;
	private String actionSubType;
	private Long actionOwner;
	private Date deadline;
	private Date estimatedCompletionDate;
	private Long incidentWhyId;
	private Long reportId;
	private String counterMeasureNumber;
	private Date createdAt;
	private String createdBy;
	private Date updatedAt;
	private String updatedBy;
	private Long createdByUserId;
	private String status = "";
	private String filter;
	private String comments;
	private String actionSource;
	private boolean addHistory;
	
	
	
	
	public Long getReportId() {
		return reportId;
	}
	public void setReportId(Long reportId) {
		this.reportId = reportId;
	}
	public String getActionSource() {
		return actionSource;
	}
	public void setActionSource(String actionSource) {
		this.actionSource = actionSource;
	}
	public boolean isAddHistory() {
		return addHistory;
	}
	public void setAddHistory(boolean addHistory) {
		this.addHistory = addHistory;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	
	public String getFilter() {
		return filter;
	}
	public void setFilter(String filter) {
		this.filter = filter;
	}
	public Date getEstimatedCompletionDate() {
		return estimatedCompletionDate;
	}
	public void setEstimatedCompletionDate(Date estimatedCompletionDate) {
		this.estimatedCompletionDate = estimatedCompletionDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getActionSubType() {
		return actionSubType;
	}
	public void setActionSubType(String actionSubType) {
		this.actionSubType = actionSubType;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getActionType() {
		return actionType;
	}
	public void setActionType(String actionType) {
		this.actionType = actionType;
	}
	public Date getDeadline() {
		return deadline;
	}
	public void setDeadline(Date deadline) {
		this.deadline = deadline;
	}
	public Long getIncidentWhyId() {
		return incidentWhyId;
	}
	public void setIncidentWhyId(Long incidentWhyId) {
		this.incidentWhyId = incidentWhyId;
	}
	public String getCounterMeasureNumber() {
		return counterMeasureNumber;
	}
	public void setCounterMeasureNumber(String counterMeasureNumber) {
		this.counterMeasureNumber = counterMeasureNumber;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public Long getActionSite() {
		return actionSite;
	}
	public void setActionSite(Long actionSite) {
		this.actionSite = actionSite;
	}
	public Long getActionOwner() {
		return actionOwner;
	}
	public void setActionOwner(Long actionOwner) {
		this.actionOwner = actionOwner;
	}
	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Long getCreatedByUserId() {
		return createdByUserId;
	}
	public void setCreatedByUserId(Long createdByUserId) {
		this.createdByUserId = createdByUserId;
	}
	
	
}
