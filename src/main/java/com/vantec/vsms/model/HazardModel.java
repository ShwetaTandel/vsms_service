package com.vantec.vsms.model;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

public class HazardModel {

	private String filter;
	private Long hazardNumber;
	private Long siteId;
	private String location;
	private String locationOther;
	private Date hazardDateTime;
	private String hazardDescription;
	private String makeItSafe;
	private String makeItSafeWhyWhat;
	private String anyWitness;
	private String witnessName;
	private String improvementSuggestions;
	private String actOrCondition;
	private String riskCategory;
	private String hazardComments;
	private String hazardActions;
	private String furtherActionsNeeded;
	private String reporterName;
	private String reporterDepartment;
	private Long currApproverId;
	private String status;
	private Long userId;
	
	private String ipAddress;
	private String securityToken;
	
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public String getSecurityToken() {
		return securityToken;
	}
	public void setSecurityToken(String securityToken) {
		this.securityToken = securityToken;
	}
	private List<ActionModel>  counterMeasures = new ArrayList<ActionModel>();
	
	
	public List<ActionModel> getCounterMeasures() {
		return counterMeasures;
	}
	public void setCounterMeasures(List<ActionModel> counterMeasures) {
		this.counterMeasures = counterMeasures;
	}
	public String getActOrCondition() {
		return actOrCondition;
	}
	public void setActOrCondition(String actOrCondition) {
		this.actOrCondition = actOrCondition;
	}
	public String getRiskCategory() {
		return riskCategory;
	}
	public void setRiskCategory(String riskCaetgory) {
		this.riskCategory = riskCaetgory;
	}
	public String getHazardComments() {
		return hazardComments;
	}
	public void setHazardComments(String hazardComments) {
		this.hazardComments = hazardComments;
	}
	public String getHazardActions() {
		return hazardActions;
	}
	public void setHazardActions(String hazardActions) {
		this.hazardActions = hazardActions;
	}
	public String getFurtherActionsNeeded() {
		return furtherActionsNeeded;
	}
	public void setFurtherActionsNeeded(String furtherActionsNeeded) {
		this.furtherActionsNeeded = furtherActionsNeeded;
	}
	public String getMakeItSafe() {
		return makeItSafe;
	}
	public void setMakeItSafe(String makeItSafe) {
		this.makeItSafe = makeItSafe;
	}
	public String getMakeItSafeWhyWhat() {
		return makeItSafeWhyWhat;
	}
	public void setMakeItSafeWhyWhat(String makeItSafeWhyWhat) {
		this.makeItSafeWhyWhat = makeItSafeWhyWhat;
	}
	public String getFilter() {
		return filter;
	}
	public void setFilter(String filter) {
		this.filter = filter;
	}
	public Long getHazardNumber() {
		return hazardNumber;
	}
	public void setHazardNumber(Long hazardNumber) {
		this.hazardNumber = hazardNumber;
	}
	public Long getSiteId() {
		return siteId;
	}
	public void setSiteId(Long siteId) {
		this.siteId = siteId;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getLocationOther() {
		return locationOther;
	}
	public void setLocationOther(String locationOther) {
		this.locationOther = locationOther;
	}
	public Date getHazardDateTime() {
		return hazardDateTime;
	}
	public void setHazardDateTime(Date hazardDateTime) {
		this.hazardDateTime = hazardDateTime;
	}
	public String getHazardDescription() {
		return hazardDescription;
	}
	public void setHazardDescription(String hazardDescription) {
		this.hazardDescription = hazardDescription;
	}
	
	public String getAnyWitness() {
		return anyWitness;
	}
	public void setAnyWitness(String anyWitness) {
		this.anyWitness = anyWitness;
	}
	public String getWitnessName() {
		return witnessName;
	}
	public void setWitnessName(String witnessName) {
		this.witnessName = witnessName;
	}
	public String getImprovementSuggestions() {
		return improvementSuggestions;
	}
	public void setImprovementSuggestions(String improvementSuggestions) {
		this.improvementSuggestions = improvementSuggestions;
	}
	public String getReporterName() {
		return reporterName;
	}
	public void setReporterName(String reporterName) {
		this.reporterName = reporterName;
	}
	public String getReporterDepartment() {
		return reporterDepartment;
	}
	public void setReporterDepartment(String reporterDepartment) {
		this.reporterDepartment = reporterDepartment;
	}
	public Long getCurrApproverId() {
		return currApproverId;
	}
	public void setCurrApproverId(Long currApproverId) {
		this.currApproverId = currApproverId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}

}
