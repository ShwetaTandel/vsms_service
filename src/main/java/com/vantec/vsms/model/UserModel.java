package com.vantec.vsms.model;

import java.util.ArrayList;
import java.util.List;

public class UserModel {

	private Long userId;
	private String firstName;
	private String lastName;
	private String emailId;
	private Boolean isApprover;
	private Boolean isActionOwner;
	private String managerName;
	private List<Long> siteList = new ArrayList<Long>();
	private Long requestorId;
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public Boolean getIsApprover() {
		return isApprover;
	}
	public void setIsApprover(Boolean isApprover) {
		this.isApprover = isApprover;
	}
	public List<Long> getSiteList() {
		return siteList;
	}
	public void setSiteList(List<Long> siteList) {
		this.siteList = siteList;
	}
	public Long getRequestorId() {
		return requestorId;
	}
	public void setRequestorId(Long requestorId) {
		this.requestorId = requestorId;
	}
	public Boolean getIsActionOwner() {
		return isActionOwner;
	}
	public void setIsActionOwner(Boolean isActionOwner) {
		this.isActionOwner = isActionOwner;
	}
	public String getManagerName() {
		return managerName;
	}
	public void setManagerName(String managerName) {
		this.managerName = managerName;
	}

	

}
