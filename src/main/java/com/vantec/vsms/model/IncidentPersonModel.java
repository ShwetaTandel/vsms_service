package com.vantec.vsms.model;

import java.util.Date;

public class IncidentPersonModel {
	
	private Long personId;
	private String personName;
	private Date personDOB;
	private Date employmentstartDate;
	private Date timeOnJobStartDate;
	private String employmentStatus;
	private Boolean anyPrevIncidents;
	private String prevIncidentDateReference;
	private Boolean isVehicleInvolved;
	private String vehicleType;
	private String vehicleTypeOther;
	private String vehicleRegNo;
	private Boolean wasPersonremovedFromTruck;
	private Date licenseExpDate;
	private Boolean isLicenseValid;
	private Boolean isInjuryInvolved;
	private String locationOfInjury;
	private String treatmentAdministered;
	private String ppeWorn;
	private Boolean wasPPEOfReqLevel;
	private String treatementDetails;
	private Boolean daTestRequired;
	private String daTestNotReqReason;
	private Boolean daTestAgreed;
	private String daTestedBy;
	private Boolean daPassed;
	private Date createdAt;
	private String createdBy;
	private Date updatedAt;
	private String updatedBy;
	private Long creatorUserId;
	private String firstAiderName;
	

	public String getFirstAiderName() {
		return firstAiderName;
	}
	public void setFirstAiderName(String firstAiderName) {
		this.firstAiderName = firstAiderName;
	}
	public String getVehicleTypeOther() {
		return vehicleTypeOther;
	}
	public void setVehicleTypeOther(String vehicleTypeOther) {
		this.vehicleTypeOther = vehicleTypeOther;
	}
	public String getDaTestNotReqReason() {
		return daTestNotReqReason;
	}
	public void setDaTestNotReqReason(String daTestNotReqReason) {
		this.daTestNotReqReason = daTestNotReqReason;
	}
	public Boolean getDaTestAgreed() {
		return daTestAgreed;
	}
	public void setDaTestAgreed(Boolean daTestAgreed) {
		this.daTestAgreed = daTestAgreed;
	}
	public String getDaTestedBy() {
		return daTestedBy;
	}
	public void setDaTestedBy(String daTestedBy) {
		this.daTestedBy = daTestedBy;
	}
	public Boolean getIsInjuryInvolved() {
		return isInjuryInvolved;
	}
	public void setIsInjuryInvolved(Boolean isInjuryInvolved) {
		this.isInjuryInvolved = isInjuryInvolved;
	}
	public Long getPersonId() {
		return personId;
	}
	public void setPersonId(Long personId) {
		this.personId = personId;
	}
	public String getPersonName() {
		return personName;
	}
	public void setPersonName(String personName) {
		this.personName = personName;
	}
	public Date getPersonDOB() {
		return personDOB;
	}
	public void setPersonDOB(Date personDOB) {
		this.personDOB = personDOB;
	}
	public Date getEmploymentstartDate() {
		return employmentstartDate;
	}
	public void setEmploymentstartDate(Date employmentstartDate) {
		this.employmentstartDate = employmentstartDate;
	}
	public Date getTimeOnJobStartDate() {
		return timeOnJobStartDate;
	}
	public void setTimeOnJobStartDate(Date timeOnJobStartDate) {
		this.timeOnJobStartDate = timeOnJobStartDate;
	}
	public String getEmploymentStatus() {
		return employmentStatus;
	}
	public void setEmploymentStatus(String employmentStatus) {
		this.employmentStatus = employmentStatus;
	}
	public Boolean getAnyPrevIncidents() {
		return anyPrevIncidents;
	}
	public void setAnyPrevIncidents(Boolean anyPrevIncidents) {
		this.anyPrevIncidents = anyPrevIncidents;
	}
	public String getPrevIncidentDateReference() {
		return prevIncidentDateReference;
	}
	public void setPrevIncidentDateReference(String prevIncidentDateReference) {
		this.prevIncidentDateReference = prevIncidentDateReference;
	}
	public Boolean getIsVehicleInvolved() {
		return isVehicleInvolved;
	}
	public void setIsVehicleInvolved(Boolean isVehicleInvolved) {
		this.isVehicleInvolved = isVehicleInvolved;
	}
	public String getVehicleType() {
		return vehicleType;
	}
	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}
	public String getVehicleRegNo() {
		return vehicleRegNo;
	}
	public void setVehicleRegNo(String vehicleRegNo) {
		this.vehicleRegNo = vehicleRegNo;
	}
	public Boolean getWasPersonremovedFromTruck() {
		return wasPersonremovedFromTruck;
	}
	public void setWasPersonremovedFromTruck(Boolean wasPersonremovedFromTruck) {
		this.wasPersonremovedFromTruck = wasPersonremovedFromTruck;
	}
	public Date getLicenseExpDate() {
		return licenseExpDate;
	}
	public void setLicenseExpDate(Date licenseExpDate) {
		this.licenseExpDate = licenseExpDate;
	}
	public Boolean getIsLicenseValid() {
		return isLicenseValid;
	}
	public void setIsLicenseValid(Boolean isLicenseValid) {
		this.isLicenseValid = isLicenseValid;
	}
	public String getLocationOfInjury() {
		return locationOfInjury;
	}
	public void setLocationOfInjury(String locationOfInjury) {
		this.locationOfInjury = locationOfInjury;
	}
	public String getTreatmentAdministered() {
		return treatmentAdministered;
	}
	public void setTreatmentAdministered(String treatmentAdministered) {
		this.treatmentAdministered = treatmentAdministered;
	}
	public String getPpeWorn() {
		return ppeWorn;
	}
	public void setPpeWorn(String ppeWorn) {
		this.ppeWorn = ppeWorn;
	}
	public Boolean getWasPPEOfReqLevel() {
		return wasPPEOfReqLevel;
	}
	public void setWasPPEOfReqLevel(Boolean wasPPEOfReqLevel) {
		this.wasPPEOfReqLevel = wasPPEOfReqLevel;
	}
	public String getTreatementDetails() {
		return treatementDetails;
	}
	public void setTreatementDetails(String treatementDetails) {
		this.treatementDetails = treatementDetails;
	}
	public Boolean getDaTestRequired() {
		return daTestRequired;
	}
	public void setDaTestRequired(Boolean daTestRequired) {
		this.daTestRequired = daTestRequired;
	}
	public Boolean getDaPassed() {
		return daPassed;
	}
	public void setDaPassed(Boolean daPassed) {
		this.daPassed = daPassed;
	}
	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Long getCreatorUserId() {
		return creatorUserId;
	}
	public void setCreatorUserId(Long creatorUserId) {
		this.creatorUserId = creatorUserId;
	}
	
	

}
