package com.vantec.vsms.controller;

import java.text.ParseException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.vantec.vsms.model.ActionModel;
import com.vantec.vsms.model.ApproverModel;
import com.vantec.vsms.model.HazardModel;
import com.vantec.vsms.model.IncidentModel;
import com.vantec.vsms.model.RiskAssessmentModel;
import com.vantec.vsms.model.RiskHazardModel;
import com.vantec.vsms.model.SiteModel;
import com.vantec.vsms.model.UserModel;
import com.vantec.vsms.service.VSMSService;

@RestController
@RequestMapping("/")
public class VSMSController {
	
	private static Logger logger = LoggerFactory.getLogger(VSMSController.class);
	@Autowired
	VSMSService vsmsService;
	
    @RequestMapping(value = "/changeDateTime", method = RequestMethod.GET)
	public ResponseEntity<String> changeDateTime(@RequestParam Long incidentNumber) throws ParseException {
    	vsmsService.updateDeadlineAndTime(incidentNumber);
			return new ResponseEntity<String>(vsmsService.testService(), HttpStatus.OK);
	}
    
    @RequestMapping(value = "/saveIncident", method = RequestMethod.POST)
	public ResponseEntity<String> saveIncident(@RequestBody IncidentModel request) throws ParseException {
    	logger.info("saveIncident called..");
			return new ResponseEntity<String>(vsmsService.saveIncident(request), HttpStatus.OK);
	}
    @RequestMapping(value = "/saveUser", method = RequestMethod.POST)
    public ResponseEntity<String> saveUser(@RequestBody UserModel userReq) throws ParseException {
    	logger.info("saveUser called..");
		return new ResponseEntity<String>(vsmsService.saveUser(userReq), HttpStatus.OK);
    }
    @RequestMapping(value = "/addApprovers", method = RequestMethod.POST)
    public ResponseEntity<String> addApprovers (@RequestBody ApproverModel request) throws ParseException {
    	logger.info("addApprovers called..");
		return new ResponseEntity<String>(vsmsService.addApprovers(request), HttpStatus.OK);
    }
    @RequestMapping(value = "/addRecipients", method = RequestMethod.POST)
    public ResponseEntity<String> addRecipients (@RequestBody ApproverModel request) throws ParseException {
    	logger.info("addRecipients called..");
		return new ResponseEntity<String>(vsmsService.addRecipients(request), HttpStatus.OK);
    }

    @RequestMapping(value = "/saveSite", method = RequestMethod.POST)
    public ResponseEntity<String> saveSite(@RequestBody SiteModel siteReq) throws ParseException {
    	logger.info("saveSite called..");
		return new ResponseEntity<String>(vsmsService.saveSite(siteReq), HttpStatus.OK);
    }
    @RequestMapping(value = "/rejectIncident", method = RequestMethod.GET)
	public ResponseEntity<String> rejectIncident(@RequestParam Long incidentNumber,@RequestParam Long approverId ,@RequestParam String comments, @RequestParam Long prevApproverId) throws ParseException {
    	logger.info("rejectIncident called..");
			return new ResponseEntity<String>(vsmsService.rejectIncident(incidentNumber, approverId, comments, prevApproverId), HttpStatus.OK);
	}
    
    @RequestMapping(value = "/deleteIncident", method = RequestMethod.GET)
	public ResponseEntity<String> deleteIncident(@RequestParam Long incidentNumber,@RequestParam Long requestorId ,@RequestParam String comments ) throws ParseException {
    	logger.info("deleteIncident called..");
			return new ResponseEntity<String>(vsmsService.deleteIncident(incidentNumber, requestorId, comments), HttpStatus.OK);
	}
    
    @RequestMapping(value = "/deleteHazard", method = RequestMethod.GET)
	public ResponseEntity<String> deleteHazard(@RequestParam Long hazardNumber,@RequestParam Long requestorId ,@RequestParam String comments ) throws ParseException {
    	logger.info("deleteHazard called..");
			return new ResponseEntity<String>(vsmsService.deleteHazard(hazardNumber, requestorId, comments), HttpStatus.OK);
	}

    @RequestMapping(value = "/saveAction", method = RequestMethod.POST)
	public ResponseEntity<String> saveAction(@RequestBody ActionModel request) throws ParseException {
    	logger.info("saveAction called..");
			return new ResponseEntity<String>(vsmsService.saveAction(request), HttpStatus.OK);
	}
    @RequestMapping(value = "/changeApprover", method = RequestMethod.POST)
	public ResponseEntity<String> changeApprover(@RequestBody ApproverModel request) throws ParseException {
    	logger.info("changeApprover called..");
			return new ResponseEntity<String>(vsmsService.changeApprover(request), HttpStatus.OK);
	}
    
    @RequestMapping(value = "/saveHazardSecure", method = RequestMethod.POST)
	public ResponseEntity<String> saveHazardSecure(@RequestBody HazardModel request) throws ParseException {
    	logger.info("saveHazardSecure called..");
    	if(vsmsService.authenticateToken(request.getIpAddress(), request.getSiteId())){
    		return new ResponseEntity<String>(vsmsService.saveHazard(request), HttpStatus.OK);
    	}else{
    		return new ResponseEntity<String>("Security Error", HttpStatus.OK);
    	}
    	
	}
    
    @RequestMapping(value = "/saveHazard", method = RequestMethod.POST)
	public ResponseEntity<String> saveHazard(@RequestBody HazardModel request) throws ParseException {
    	logger.info("saveHazard called..");
			return new ResponseEntity<String>(vsmsService.saveHazard(request), HttpStatus.OK);
	}
    @RequestMapping(value = "/saveRiskHazard", method = RequestMethod.POST)
	public ResponseEntity<String> saveRiskHazard(@RequestBody RiskHazardModel request) throws ParseException {
    	logger.info("saveRiskHazard called..");
			return new ResponseEntity<String>(vsmsService.saveRiskHazard(request), HttpStatus.OK);
	}
    @RequestMapping(value = "/saveRiskAssessment", method = RequestMethod.POST)
	public ResponseEntity<String> saveRiskAssessment(@RequestBody RiskAssessmentModel request) throws ParseException {
    	logger.info("saveRiskAssessment called..");
			return new ResponseEntity<String>(vsmsService.saveRiskAssessment(request), HttpStatus.OK);
	}
    
    @RequestMapping(value = "/deleteRiskHazard", method = RequestMethod.GET)
	public ResponseEntity<String> deleteRiskHazard(@RequestParam Long hazardId,@RequestParam Long requestorId ) throws ParseException {
    	logger.info("deleteRiskHazard called..");
			return new ResponseEntity<String>(vsmsService.deleteRiskHazard(hazardId, requestorId), HttpStatus.OK);
	}
    
    @RequestMapping(value = "/newVersionRiskAssessment", method = RequestMethod.GET)
	public ResponseEntity<String> newVersionRiskAssessment(@RequestParam Long graId,@RequestParam Long requestorId ) throws ParseException {
    	logger.info("newVersionRiskAssessment called..");
			return new ResponseEntity<String>(vsmsService.newVersionRiskAssessment(graId, requestorId), HttpStatus.OK);
	}
    
    @RequestMapping(value = "/deleteRiskAssessment", method = RequestMethod.GET)
	public ResponseEntity<String> deleteRiskAssessment(@RequestParam Long graId,@RequestParam Long requestorId ) throws ParseException {
    	logger.info("deleteRiskAssessment called..");
			return new ResponseEntity<String>(vsmsService.deleteRiskAssessment(graId, requestorId), HttpStatus.OK);
	}

}
