package com.vantec.vsms.api;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.event.ContextStoppedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
@ComponentScan(basePackages = { "com.vantec.vsms.*"})
@EntityScan(basePackages = { "com.vantec.vsms.entity"})
@EnableJpaRepositories("com.vantec.vsms.repository")
public class SpringBootRestApiApp {
	private static Logger logger = LoggerFactory.getLogger(SpringBootRestApiApp.class);

	public static void main(String[] args) {
		
			try{
				SpringApplication.run(SpringBootRestApiApp.class, args);
			}catch(Error e){
				System.out.println("There was an error");
			}
			catch(Exception e){
				System.out.println("There was an exception" + e);
			}
		
	}

	public static void printLoggerLevel() {
		logger.trace("TRACE");
		logger.info("INFO");
		logger.debug("DEBUG");
		logger.warn("WARN");
		logger.error("ERROR");
	}
	@EventListener
    public void onStartup(ApplicationReadyEvent event) {
    	logger.info("starting..............................");
    }
    @EventListener
    public void onShutdown(ContextStoppedEvent event) {
    	logger.info("closing..............................");
    }
    @PostConstruct
    public void startupApplication() {
    	logger.info("starting service ..............................");
    }
    @PreDestroy
    public void shutdownApplication() {
    	logger.info("closing service ..............................");
    }
}
