package com.vantec.vsms.util;
import java.security.Key;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import com.sun.org.apache.xml.internal.security.utils.Base64;

public class SecurityToken {


    public static void main(String[] args) throws Exception {
        String data = "::120230325";
        final String enc = getEncrypted(data);
        System.out.println("Encrypted : " + enc);
        System.out.println("Decrypted : " + getDecrypted("PjOaOnmBaOzn2SvOr08jgA== "));
    }
//
//    public static void encrypt(String password) throws Exception {
//        byte[] keyData = ("ABC").getBytes();
//        SecretKeySpec secretKeySpec = new SecretKeySpec(keyData, "Blowfish");
//        Cipher cipher = Cipher.getInstance("Blowfish");
//        cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec);
//        byte[] hasil = cipher.doFinal(password.getBytes());
//        System.out.println(new BASE64Encoder().encode(hasil));
//    }
//
//    public static String decrypt(String string) throws Exception {
//        byte[] keyData = ("ABC").getBytes();
//        SecretKeySpec secretKeySpec = new SecretKeySpec(keyData, "Blowfish");
//        Cipher cipher = Cipher.getInstance("blowfish/ecb/nopadding");
//        cipher.init(Cipher.DECRYPT_MODE, secretKeySpec);
//        byte[] hasil = cipher.doFinal(new BASE64Decoder().decodeBuffer(string));
//        System.out.println(new String(hasil));
//        return new String(hasil).trim();
//    }
    
    private static final String ALGORITHM = "AES";

    private static final byte[] SALT = "tHeApAcHe6410111".getBytes();// THE KEY MUST BE SAME
    //private static final String X = DarKnight.class.getSimpleName();

    static String getEncrypted(String plainText) {

        if (plainText == null) {
            return null;
        }

        Key salt = getSalt();

        try {
            Cipher cipher = Cipher.getInstance(ALGORITHM);
            cipher.init(Cipher.ENCRYPT_MODE, salt);
            byte[] encodedValue = cipher.doFinal(plainText.getBytes());
            return Base64.encode(encodedValue);
        } catch (Exception e) {
            e.printStackTrace();
        }

        throw new IllegalArgumentException("Failed to encrypt data");
    }

    public static String getDecrypted(String encodedText) {

        if (encodedText == null) {
            return null;
        }

        Key salt = getSalt();
        try {
            Cipher cipher = Cipher.getInstance(ALGORITHM);
            cipher.init(Cipher.DECRYPT_MODE, salt);
            byte[] decodedValue = Base64.decode(encodedText);
            byte[] decValue = cipher.doFinal(decodedValue);
            return new String(decValue);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    static Key getSalt() {
        return new SecretKeySpec(SALT, ALGORITHM);
    }


}