package com.vantec.vsms.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vantec.vsms.entity.Incident;
import com.vantec.vsms.entity.IncidentDeadlines;

public class IncidentHelper {
	private static Logger logger = LoggerFactory.getLogger(IncidentHelper.class);
	
	public static void main(String args[]) throws ParseException{
		
	
	Incident inc = new Incident();
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	inc.setIncidentDateTime(sdf.parse("2021-11-17 13:40:00"));
	getDeadLines(inc);
		
		
	}

	
	private static void getDeadLines(Incident savedIncident){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		IncidentDeadlines deadlines = new IncidentDeadlines();
		deadlines.setIncidentDateTime(savedIncident.getIncidentDateTime());
		deadlines.setIncidentNumber(savedIncident.getIncidentNumber());
		final int SUN = 1, MON = 2, TUE = 3, WED = 4, THU = 5, FRI = 6, SAT = 7;
		Calendar incidentDate = Calendar.getInstance();
		incidentDate.setTime(savedIncident.getIncidentDateTime());
		int dayOfWeek = incidentDate.get(Calendar.DAY_OF_WEEK);
		logger.info("Incident date time is " + sdf.format(incidentDate.getTime()));
		switch (dayOfWeek) {
		case SUN:
			incidentDate.add(Calendar.HOUR_OF_DAY, 8);
			logger.info("SUBMIT reminder after " +sdf.format(incidentDate.getTime())); //8
			
			incidentDate.add(Calendar.HOUR_OF_DAY, 16);
			logger.info("SUBMIT escalation after " + sdf.format(incidentDate.getTime()));//24
			
			incidentDate.add(Calendar.HOUR_OF_DAY, 12);
			logger.info("FI submit reminder  after " + sdf.format(incidentDate.getTime()));//36
			
			incidentDate.add(Calendar.HOUR_OF_DAY, 36);
			logger.info("FI submit escalation is " + sdf.format(incidentDate.getTime()));//72
			
			incidentDate.add(Calendar.HOUR_OF_DAY, 24);
			logger.info("GM approval reminder after " + sdf.format(incidentDate.getTime()));//96
			
			incidentDate.add(Calendar.HOUR_OF_DAY, 72+24+24);
			logger.info("GM approval  escalation  after  " + sdf.format(incidentDate.getTime()));//168
			break;
		case MON:
			incidentDate.add(Calendar.HOUR_OF_DAY, 8);
			logger.info("SUBMIT reminder after" + sdf.format(incidentDate.getTime())); //8
			
			incidentDate.add(Calendar.HOUR_OF_DAY, 16);
			logger.info("SUBMIT escalation after " + sdf.format(incidentDate.getTime()));//24
			
			incidentDate.add(Calendar.HOUR_OF_DAY, 12);
			logger.info("FI submit reminder  after " + sdf.format(incidentDate.getTime()));//36
			
			incidentDate.add(Calendar.HOUR_OF_DAY, 36);
			logger.info("FI submit escalation is " + sdf.format(incidentDate.getTime()));//72
			
			incidentDate.add(Calendar.HOUR_OF_DAY, 24);
			logger.info("GM approval reminder after " + sdf.format(incidentDate.getTime()));//96
			
			incidentDate.add(Calendar.HOUR_OF_DAY, 72+24+24);
			logger.info("GM approval  escalation  after  " + sdf.format(incidentDate.getTime()));//168
			break;
		case TUE:
			incidentDate.add(Calendar.HOUR_OF_DAY, 8);
			logger.info("SUBMIT reminder after" + sdf.format(incidentDate.getTime())); //8
			
			incidentDate.add(Calendar.HOUR_OF_DAY, 16);
			logger.info("SUBMIT escalation after " + sdf.format(incidentDate.getTime()));//24
			
			incidentDate.add(Calendar.HOUR_OF_DAY, 12);
			logger.info("FI submit reminder  after " + sdf.format(incidentDate.getTime()));//36
			
			incidentDate.add(Calendar.HOUR_OF_DAY, 36);
			logger.info("FI submit escalation is " + sdf.format(incidentDate.getTime()));//72
			
			incidentDate.add(Calendar.HOUR_OF_DAY, 24+24+24);
			logger.info("GM approval reminder after " + sdf.format(incidentDate.getTime()));//96
			
			incidentDate.add(Calendar.HOUR_OF_DAY, 72);
			logger.info("GM approval  escalation  after  " + sdf.format(incidentDate.getTime()));//168
			break;

		case WED:
			incidentDate.add(Calendar.HOUR_OF_DAY, 8);
			logger.info("SUBMIT reminder after" + sdf.format(incidentDate.getTime())); //8
			
			incidentDate.add(Calendar.HOUR_OF_DAY, 16);
			logger.info("SUBMIT escalation after " + sdf.format(incidentDate.getTime()));//24
			
			incidentDate.add(Calendar.HOUR_OF_DAY, 12);
			logger.info("FI submit reminder  after " + sdf.format(incidentDate.getTime()));//36
			
			incidentDate.add(Calendar.HOUR_OF_DAY, 36+24+24);
			logger.info("FI submit escalation is " + sdf.format(incidentDate.getTime()));//72
			
			incidentDate.add(Calendar.HOUR_OF_DAY, 24);
			logger.info("GM approval reminder after " + sdf.format(incidentDate.getTime()));//96
			
			incidentDate.add(Calendar.HOUR_OF_DAY, 72);
			logger.info("GM approval  escalation  after  " + sdf.format(incidentDate.getTime()));//168
			break;

		case THU:
			incidentDate.add(Calendar.HOUR_OF_DAY, 8);
			logger.info("SUBMIT reminder after " + sdf.format(incidentDate.getTime())); //8
			
			incidentDate.add(Calendar.HOUR_OF_DAY, 16);
			logger.info("SUBMIT escalation after " + sdf.format(incidentDate.getTime()));//24
			
			incidentDate.add(Calendar.HOUR_OF_DAY, 12+24+24);
			logger.info("FI submit reminder  after " + sdf.format(incidentDate.getTime()));//36
			
			incidentDate.add(Calendar.HOUR_OF_DAY, 36);
			logger.info("FI submit escalation is " + sdf.format(incidentDate.getTime()));//72
			
			incidentDate.add(Calendar.HOUR_OF_DAY, 24);
			logger.info("GM approval reminder after " + sdf.format(incidentDate.getTime()));//96
			
			incidentDate.add(Calendar.HOUR_OF_DAY, 72+24+24);
			logger.info("GM approval  escalation  after  " + sdf.format(incidentDate.getTime()));//168
			break;

		case FRI:
			incidentDate.add(Calendar.HOUR_OF_DAY, 8);
			logger.info("SUBMIT reminder after " + sdf.format(incidentDate.getTime())); //8
			
			incidentDate.add(Calendar.HOUR_OF_DAY, 16+24+24);
			logger.info("SUBMIT escalation after " + sdf.format(incidentDate.getTime()));//24
			
			incidentDate.add(Calendar.HOUR_OF_DAY, 12);
			logger.info("FI submit reminder  after " + sdf.format(incidentDate.getTime()));//36
			
			incidentDate.add(Calendar.HOUR_OF_DAY, 36);
			logger.info("FI submit escalation is " + sdf.format(incidentDate.getTime()));//72
			
			incidentDate.add(Calendar.HOUR_OF_DAY, 24);
			logger.info("GM approval reminder after " + sdf.format(incidentDate.getTime()));//96
			
			incidentDate.add(Calendar.HOUR_OF_DAY, 72+24+24);
			logger.info("GM approval  escalation  after  " + sdf.format(incidentDate.getTime()));//168
			break;
		case SAT:
			incidentDate.add(Calendar.HOUR_OF_DAY, 8);
			logger.info("SUBMIT reminder after " + sdf.format(incidentDate.getTime())); //8
			
			incidentDate.add(Calendar.HOUR_OF_DAY, 16+24);
			logger.info("SUBMIT escalation after " + sdf.format(incidentDate.getTime()));//24
			
			incidentDate.add(Calendar.HOUR_OF_DAY, 12);
			logger.info("FI submit reminder  after " + sdf.format(incidentDate.getTime()));//36
			
			incidentDate.add(Calendar.HOUR_OF_DAY, 36);
			logger.info("FI submit escalation is " + sdf.format(incidentDate.getTime()));//72
			
			incidentDate.add(Calendar.HOUR_OF_DAY, 24);
			logger.info("GM approval reminder after " + sdf.format(incidentDate.getTime()));//96
			
			incidentDate.add(Calendar.HOUR_OF_DAY, 72+24+24);
			logger.info("GM approval  escalation  after  " + sdf.format(incidentDate.getTime()));//168
			break;

		}
		//incidentDeadlinesRepo.save(deadlines);
		
	}
}
