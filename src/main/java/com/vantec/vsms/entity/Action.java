package com.vantec.vsms.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;




@Entity
@Table(name = "actions")
public class Action implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "report_id")
	private Long reportId;
	
	
	@Column(name = "action")
	private String action;

	
	@Column(name = "action_site")
	private Long actionSite;
	
	@Column(name = "action_type")
	private String actionType;

	@Column(name = "action_sub_type")
	private String actionSubType;

	
	@Column(name = "action_owner")
	private Long actionOwner;
	
	@Column(name = "status")
	private String status;
	
	@Column(name = "comments")
	private String comments;

	
	@Column(name = "deadline")
	private Date deadline;
	
	@Column(name = "estimated_completion_date")
	private Date estimatedCompletionDate;

	
	@Column(name = "report_sub_id")
	private Long reportSubId;
	
	@Column(name = "counter_measure_number")
	private String counterMeasureNumber;
	
	@Column(name = "assigned_by")
	private String assignedBy;
	
	@Column(name = "assigned_at")
	private Date assignedAt;


	@Column(name = "updated_at")
	private Date updatedAt;

	@Column(name = "updated_by")
	private String updatedBy;

	@Column(name = "updated_by_user_id")
	private Long updatedByUserId;
	
	@Column(name = "action_source")
	private String actionSource;

	public Action() {
	}

	public Long getReportId() {
		return reportId;
	}

	public void setReportId(Long reportId) {
		this.reportId = reportId;
	}

	public String getActionSource() {
		return actionSource;
	}

	public void setActionSource(String actionSource) {
		this.actionSource = actionSource;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Date getEstimatedCompletionDate() {
		return estimatedCompletionDate;
	}

	public void setEstimatedCompletionDate(Date estimatedCompletionDate) {
		this.estimatedCompletionDate = estimatedCompletionDate;
	}

	public String getStatus() {
		return status;
	}

	public Date getAssignedAt() {
		return assignedAt;
	}

	public void setAssignedAt(Date assignedAt) {
		this.assignedAt = assignedAt;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getActionSubType() {
		return actionSubType;
	}

	public void setActionSubType(String actionSubType) {
		this.actionSubType = actionSubType;
	}

	public String getActionType() {
		return actionType;
	}

	public void setActionType(String actionType) {
		this.actionType = actionType;
	}

	public Date getDeadline() {
		return deadline;
	}

	public void setDeadline(Date deadline) {
		this.deadline = deadline;
	}



	public Long getReportSubId() {
		return reportSubId;
	}

	public void setReportSubId(Long reportSubId) {
		this.reportSubId = reportSubId;
	}

	public String getCounterMeasureNumber() {
		return counterMeasureNumber;
	}

	public void setCounterMeasureNumber(String counterMeasureNumber) {
		this.counterMeasureNumber = counterMeasureNumber;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public Long getActionSite() {
		return actionSite;
	}

	public void setActionSite(Long actionSite) {
		this.actionSite = actionSite;
	}

	public Long getActionOwner() {
		return actionOwner;
	}

	public void setActionOwner(Long actionOwner) {
		this.actionOwner = actionOwner;
	}

	
	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Long getUpdatedByUserId() {
		return updatedByUserId;
	}

	public void setUpdatedByUserId(Long updatedByUserId) {
		this.updatedByUserId = updatedByUserId;
	}

	public String getAssignedBy() {
		return assignedBy;
	}

	public void setAssignedBy(String assignedBy) {
		this.assignedBy = assignedBy;
	}



}
