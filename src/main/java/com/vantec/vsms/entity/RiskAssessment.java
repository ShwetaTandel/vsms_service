package com.vantec.vsms.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "risk_assessment")
public class RiskAssessment implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "gra_id")
	private Long graId;
	

	@Column(name = "ref_gra_id")
	private Long refGraId;

	
	@Column(name = "site_id")
	private Long siteId;

	@Column(name = "owner")
	private String owner;

	@Column(name = "gra_date")
	private Date graDate;

	@Column(name = "review_date")
	private Date reviewDate;
	
	@Column(name = "area_assessed")
	private String areaAssessed;

	@Column(name = "assessor")
	private String assessor;

	@Column(name = "curr_approver_id")
	private Long currApproverId;


	@Column(name = "status")
	private String status;

	@Column(name = "created_at")
	private Date createdAt;

	@Column(name = "created_by")
	private String createdBy;

	@Column(name = "updated_at")
	private Date updatedAt;

	@Column(name = "updated_by")
	private String updatedBy;
	
	@Column(name = "is_archived")
	private Boolean isArchived;

	public RiskAssessment() {
	}

	public Boolean getIsArchived() {
		return isArchived;
	}

	public void setIsArchived(Boolean isArchived) {
		this.isArchived = isArchived;
	}

	public Long getGraId() {
		return graId;
	}

	public void setGraId(Long graId) {
		this.graId = graId;
	}

	
	public Long getSiteId() {
		return siteId;
	}

	public void setSiteId(Long siteId) {
		this.siteId = siteId;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public Date getGraDate() {
		return graDate;
	}

	public void setGraDate(Date graDate) {
		this.graDate = graDate;
	}

	public Date getReviewDate() {
		return reviewDate;
	}

	public void setReviewDate(Date reviewDate) {
		this.reviewDate = reviewDate;
	}

	public String getAreaAssessed() {
		return areaAssessed;
	}

	public void setAreaAssessed(String areaAssessed) {
		this.areaAssessed = areaAssessed;
	}

	public String getAssessor() {
		return assessor;
	}

	public void setAssessor(String assessor) {
		this.assessor = assessor;
	}

	public Long getCurrApproverId() {
		return currApproverId;
	}

	public void setCurrApproverId(Long currApproverId) {
		this.currApproverId = currApproverId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Long getRefGraId() {
		return refGraId;
	}

	public void setRefGraId(Long refGraId) {
		this.refGraId = refGraId;
	}



	

}
