package com.vantec.vsms.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "incident_full_investigation_details")
public class IncidentFullInvestigationDetail implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "incident_number", referencedColumnName = "incident_number")
	private Incident incident;
	
	@Column(name = "event_summary")
	private String eventSummary;
	
	@Column(name = "is_ssow_suitable")
	private Boolean isSSOWSuitable;
	
	@Column(name = "ssow_comments")
	private String ssowComments;
	
	@Column(name = "is_ra_suitable")
	private Boolean isRASuitable;
	
	@Column(name = "ra_comments")
	private String raComments;
	
	@Column(name = "is_mhe_training_required")
	private Boolean isMHETrainingRequired;
	
	@Column(name = "person_concentraion")
	private String personConcentraion;
	
	@Column(name = "person_experience")
	private String personExperience;
	
	@Column(name = "person_training")
	private String personTraining;
	
	@Column(name = "machine_condition")
	private String machineCondition;

	@Column(name = "machine_maintenance")
	private String machineMaintenance;
	
	@Column(name = "machine_facility")
	private String machineFacility;

	@Column(name = "method_ssow")
	private String methodSSOW;
	
	@Column(name = "method_ruling")
	private String methodRuling;

	@Column(name = "method_risk_assessment")
	private String methodRiskAssessment;
	
	@Column(name = "environment_floor")
	private String environmentFloor;
	
	@Column(name = "environment_lighting")
	private String environmentLighting;

	@Column(name = "environmentCondition")
	private String environmentCondition;
	
	@Column(name = "vantecWayDecision")
	private String vantecWayDecision;
	
	@Column(name = "vantecWayComments")
	private String vantecWayComments;

	
	@Column(name = "vantecWayDecisionTree")
	private String vantecWayDecisionTree;
	
	@Column(name = "investigationTeamNames")
	private String investigationTeamNames;
	
	
	@Column(name = "updated_at")
	private Date updatedAt;
	
	@Column(name = "updated_by")
	private String updatedBy;
	
	
	
	public String getInvestigationTeamNames() {
		return investigationTeamNames;
	}

	public void setInvestigationTeamNames(String investigationTeamNames) {
		this.investigationTeamNames = investigationTeamNames;
	}


	public String getVantecWayDecisionTree() {
		return vantecWayDecisionTree;
	}

	public void setVantecWayDecisionTree(String vantecWayDecisionTree) {
		this.vantecWayDecisionTree = vantecWayDecisionTree;
	}

	public String getVantecWayDecision() {
		return vantecWayDecision;
	}

	public void setVantecWayDecision(String vantecWayDecision) {
		this.vantecWayDecision = vantecWayDecision;
	}

	public IncidentFullInvestigationDetail() {
	}

	public String getEventSummary() {
		return eventSummary;
	}

	public void setEventSummary(String eventSummary) {
		this.eventSummary = eventSummary;
	}

	public String getVantecWayComments() {
		return vantecWayComments;
	}

	public void setVantecWayComments(String vantecWayComments) {
		this.vantecWayComments = vantecWayComments;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Incident getIncident() {
		return incident;
	}

	public void setIncident(Incident incident) {
		this.incident = incident;
	}

	public Boolean getIsSSOWSuitable() {
		return isSSOWSuitable;
	}

	public void setIsSSOWSuitable(Boolean isSSOWSuitable) {
		this.isSSOWSuitable = isSSOWSuitable;
	}

	public String getSsowComments() {
		return ssowComments;
	}

	public void setSsowComments(String ssowComments) {
		this.ssowComments = ssowComments;
	}

	public Boolean getIsRASuitable() {
		return isRASuitable;
	}

	public void setIsRASuitable(Boolean isRASuitable) {
		this.isRASuitable = isRASuitable;
	}

	public String getRaComments() {
		return raComments;
	}

	public void setRaComments(String raComments) {
		this.raComments = raComments;
	}

	public Boolean getIsMHETrainingRequired() {
		return isMHETrainingRequired;
	}

	public void setIsMHETrainingRequired(Boolean isMHETrainingRequired) {
		this.isMHETrainingRequired = isMHETrainingRequired;
	}

	public String getPersonConcentraion() {
		return personConcentraion;
	}

	public void setPersonConcentraion(String personConcentraion) {
		this.personConcentraion = personConcentraion;
	}

	public String getPersonExperience() {
		return personExperience;
	}

	public void setPersonExperience(String personExperience) {
		this.personExperience = personExperience;
	}

	public String getPersonTraining() {
		return personTraining;
	}

	public void setPersonTraining(String personTraining) {
		this.personTraining = personTraining;
	}

	public String getMachineCondition() {
		return machineCondition;
	}

	public void setMachineCondition(String machineCondition) {
		this.machineCondition = machineCondition;
	}

	public String getMachineMaintenance() {
		return machineMaintenance;
	}

	public void setMachineMaintenance(String machineMaintenance) {
		this.machineMaintenance = machineMaintenance;
	}

	public String getMachineFacility() {
		return machineFacility;
	}

	public void setMachineFacility(String machineFacility) {
		this.machineFacility = machineFacility;
	}

	public String getMethodSSOW() {
		return methodSSOW;
	}

	public void setMethodSSOW(String methodSSOW) {
		this.methodSSOW = methodSSOW;
	}

	public String getMethodRuling() {
		return methodRuling;
	}

	public void setMethodRuling(String methodRuling) {
		this.methodRuling = methodRuling;
	}

	public String getMethodRiskAssessment() {
		return methodRiskAssessment;
	}

	public void setMethodRiskAssessment(String methodRiskAssessment) {
		this.methodRiskAssessment = methodRiskAssessment;
	}

	public String getEnvironmentFloor() {
		return environmentFloor;
	}

	public void setEnvironmentFloor(String environmentFloor) {
		this.environmentFloor = environmentFloor;
	}

	public String getEnvironmentLighting() {
		return environmentLighting;
	}

	public void setEnvironmentLighting(String environmentLighting) {
		this.environmentLighting = environmentLighting;
	}

	public String getEnvironmentCondition() {
		return environmentCondition;
	}

	public void setEnvironmentCondition(String environmentCondition) {
		this.environmentCondition = environmentCondition;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	
}
