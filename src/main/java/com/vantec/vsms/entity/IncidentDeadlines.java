package com.vantec.vsms.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "incident_deadlines")
public class IncidentDeadlines implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	
	@Column(name = "incident_number")
	private Long incidentNumber;
	
	@Column(name = "incident_datetime")
	private Date incidentDateTime;
	
	@Column(name = "submit_reminder_by")
	private Date submitReminderBy;


	@Column(name = "submit_by")
	private Date submitBy;

	@Column(name = "fi_reminder")
	private Date fiReminder;

	@Column(name = "fi_submit_by")
	private Date fiSubmitBy;

	@Column(name = "gm_approval_reminder")
	private Date gmApprovalReminder;

	@Column(name = "gm_approval_by")
	private Date gmApprovalBy;

	
	public IncidentDeadlines() {
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public Long getIncidentNumber() {
		return incidentNumber;
	}


	public void setIncidentNumber(Long incidentNumber) {
		this.incidentNumber = incidentNumber;
	}


	public Date getIncidentDateTime() {
		return incidentDateTime;
	}


	public void setIncidentDateTime(Date incidentDateTime) {
		this.incidentDateTime = incidentDateTime;
	}


	public Date getSubmitReminderBy() {
		return submitReminderBy;
	}


	public void setSubmitReminderBy(Date submitReminderBy) {
		this.submitReminderBy = submitReminderBy;
	}


	public Date getSubmitBy() {
		return submitBy;
	}


	public void setSubmitBy(Date submitBy) {
		this.submitBy = submitBy;
	}


	public Date getFiReminder() {
		return fiReminder;
	}


	public void setFiReminder(Date fiReminder) {
		this.fiReminder = fiReminder;
	}


	public Date getFiSubmitBy() {
		return fiSubmitBy;
	}


	public void setFiSubmitBy(Date fiSubmitBy) {
		this.fiSubmitBy = fiSubmitBy;
	}


	public Date getGmApprovalReminder() {
		return gmApprovalReminder;
	}


	public void setGmApprovalReminder(Date gmApprovalReminder) {
		this.gmApprovalReminder = gmApprovalReminder;
	}


	public Date getGmApprovalBy() {
		return gmApprovalBy;
	}


	public void setGmApprovalBy(Date gmApprovalBy) {
		this.gmApprovalBy = gmApprovalBy;
	}


	

}
