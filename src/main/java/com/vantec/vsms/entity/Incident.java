package com.vantec.vsms.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name = "incident")
public class Incident implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "incident_number")
	private Long incidentNumber;
	
	@Column(name = "site_id")
	private Long siteId;
	
	@Column(name = "location")
	private String location;

	@Column(name = "location_other")
	private String locationOther;
	
	@Column(name = "incident_date")
	private Date incidentDateTime;
	
	@Column(name = "incident_type")
	private String incidentType;
	
	@Column(name = "event_severity")
	private String eventSeverity;
	
	@Column(name = "incident_brief")
	private String incidentBrief;
	
	@Column(name = "incident_detail")
	private String incidentDetail;
	
	@Column(name = "action_type")
	private String actionType;
	
	@Column(name = "action_type_other")
	private String actionTypeOther;
	
	@Column(name = "area")
	private String area;
	
	public String getIncidentBrief() {
		return incidentBrief;
	}


	public void setIncidentBrief(String incidentBrief) {
		this.incidentBrief = incidentBrief;
	}


	@Column(name = "lighting")
	private String lighting;
	
	@Column(name = "temperature")
	private String temperature;
	
	@Column(name = "noise")
	private String noise;
	
	@Column(name = "surface_type")
	private String surfaceType;
	
	@Column(name = "surface_condition")
	private String surfaceCondition;
	
	@Column(name = "is_person_involved")
	private Boolean isPersonInvolved;

	
	@Column(name = "is_activity_standard_process")
	private Boolean isActivityStandardProcess;
	
	@Column(name = "stillage_type")
	private String stillageType;
	
	@Column(name = "estimated_cost")
	private Float estimatedCost;
	

	@Column(name = "estimated_cost_other")
	private Float estimatedCostOther;
	
	public String getEventSeverity() {
		return eventSeverity;
	}


	public void setEventSeverity(String eventSeverity) {
		this.eventSeverity = eventSeverity;
	}


	public Float getEstimatedCostOther() {
		return estimatedCostOther;
	}


	public void setEstimatedCostOther(Float estimatedCostOther) {
		this.estimatedCostOther = estimatedCostOther;
	}


	@Column(name = "immediate_cause")
	private String immediateCause;
	
	@Column(name = "key_points")
	private String keyPoints;
	
	@Column(name = "main_message")
	private String mainMessage;
	

	@Column(name = "question1_text")
	private String question1Text;
	
	@Column(name = "question1_answer")
	private String question1Answer;
	
	@Column(name = "question2_text")
	private String question2Text;
	
	@Column(name = "question2_answer")
	private String question2Answer;

	@Column(name = "question3_text")
	private String question3Text;
	
	@Column(name = "question3_answer")
	private String question3Answer;

	@Column(name = "question4_text")
	private String question4Text;
	
	@Column(name = "question4_answer")
	private String question4Answer;
	
	@Column(name = "curr_approver_id")
	private Long currApproverId;
	
	@Column(name = "category")
	private String category;
	
	@Column(name = "category_level")
	private String categoryLevel;

	@Column(name = "type")
	private String type;

	@Column(name = "act_or_condition")
	private String actOrCondition;
	
	@Column(name = "hs_summary")
	private String hsSummary;
	
	@Column(name = "classification")
	private String classification;

	@Column(name = "is_dangerous_occurence")
	private Boolean isDangerousOccurence;

	@Column(name = "status")
	private String status;

	
	@Column(name = "created_at")
	private Date createdAt;
	
	@Column(name = "created_by")
	private String createdBy;
	
	@Column(name = "updated_at")
	private Date updatedAt;
	
	@Column(name = "updated_by")
	private String updatedBy;
	
	@Column(name = "creator_user_id")
	private Long creatorUserId;
	
	@Column(name = "gra_id")
	private Long graId;
		
	@OneToMany(mappedBy = "incident")
	private List<IncidentPersonDetail> personInvolved = new ArrayList<IncidentPersonDetail>();
	
	@OneToMany(mappedBy = "incident")
	private List<IncidentInjuredPersonDetail> injuredPersonDeails = new ArrayList<IncidentInjuredPersonDetail>();
	

	
	public Long getGraId() {
		return graId;
	}


	public void setGraId(Long graId) {
		this.graId = graId;
	}


	public Incident() {
	}


	public String getClassification() {
		return classification;
	}


	public void setClassification(String classification) {
		this.classification = classification;
	}


	public String getHsSummary() {
		return hsSummary;
	}


	public void setHsSummary(String hsSummary) {
		this.hsSummary = hsSummary;
	}


	public List<IncidentInjuredPersonDetail> getInjuredPersonDeails() {
		return injuredPersonDeails;
	}


	public void setInjuredPersonDeails(List<IncidentInjuredPersonDetail> injuredPersonDeails) {
		this.injuredPersonDeails = injuredPersonDeails;
	}


	public String getCategory() {
		return category;
	}


	public void setCategory(String category) {
		this.category = category;
	}


	public String getCategoryLevel() {
		return categoryLevel;
	}


	public void setCategoryLevel(String categoryLevel) {
		this.categoryLevel = categoryLevel;
	}


	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}


	public String getActOrCondition() {
		return actOrCondition;
	}


	public void setActOrCondition(String actOrCondition) {
		this.actOrCondition = actOrCondition;
	}


	public Boolean getIsDangerousOccurence() {
		return isDangerousOccurence;
	}


	public void setIsDangerousOccurence(Boolean isDangerousOccurence) {
		this.isDangerousOccurence = isDangerousOccurence;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	public Boolean getIsPersonInvolved() {
		return isPersonInvolved;
	}


	public void setIsPersonInvolved(Boolean isPersonInvolved) {
		this.isPersonInvolved = isPersonInvolved;
	}


	public String getIncidentDetail() {
		return incidentDetail;
	}


	public void setIncidentDetail(String incidentDetail) {
		this.incidentDetail = incidentDetail;
	}

	public List<IncidentPersonDetail> getPersonInvolved() {
		return personInvolved;
	}


	public void setPersonInvolved(List<IncidentPersonDetail> personInvolved) {
		this.personInvolved = personInvolved;
	}


	public Long getCreatorUserId() {
		return creatorUserId;
	}


	public void setCreatorUserId(Long creatorUserId) {
		this.creatorUserId = creatorUserId;
	}


	public Long getIncidentNumber() {
		return incidentNumber;
	}


	public void setIncidentNumber(Long incidentNumber) {
		this.incidentNumber = incidentNumber;
	}


	public Long getSiteId() {
		return siteId;
	}


	public void setSiteId(Long siteId) {
		this.siteId = siteId;
	}


	public String getLocation() {
		return location;
	}


	public void setLocation(String location) {
		this.location = location;
	}


	public String getLocationOther() {
		return locationOther;
	}


	public void setLocationOther(String locationOther) {
		this.locationOther = locationOther;
	}


	public Date getIncidentDateTime() {
		return incidentDateTime;
	}


	public void setIncidentDateTime(Date incidentDateTime) {
		this.incidentDateTime = incidentDateTime;
	}


	public String getIncidentType() {
		return incidentType;
	}


	public void setIncidentType(String incidentType) {
		this.incidentType = incidentType;
	}


	public String getIncidenDetail() {
		return incidentDetail;
	}


	public void setIncidenDetail(String incidentDetail) {
		this.incidentDetail = incidentDetail;
	}


	public String getActionType() {
		return actionType;
	}


	public void setActionType(String actionType) {
		this.actionType = actionType;
	}


	public String getActionTypeOther() {
		return actionTypeOther;
	}


	public void setActionTypeOther(String actionTypeOther) {
		this.actionTypeOther = actionTypeOther;
	}


	public String getArea() {
		return area;
	}


	public void setArea(String area) {
		this.area = area;
	}


	public String getLighting() {
		return lighting;
	}


	public void setLighting(String lighting) {
		this.lighting = lighting;
	}


	public String getTemperature() {
		return temperature;
	}


	public void setTemperature(String temperature) {
		this.temperature = temperature;
	}


	public String getNoise() {
		return noise;
	}


	public void setNoise(String noise) {
		this.noise = noise;
	}


	public String getSurfaceType() {
		return surfaceType;
	}


	public void setSurfaceType(String surfaceType) {
		this.surfaceType = surfaceType;
	}


	public String getSurfaceCondition() {
		return surfaceCondition;
	}


	public void setSurfaceCondition(String surfaceCondition) {
		this.surfaceCondition = surfaceCondition;
	}


	public Boolean getIsActivityStandardProcess() {
		return isActivityStandardProcess;
	}


	public void setIsActivityStandardProcess(Boolean isActivityStandardProcess) {
		this.isActivityStandardProcess = isActivityStandardProcess;
	}


	public String getStillageType() {
		return stillageType;
	}


	public void setStillageType(String stillageType) {
		this.stillageType = stillageType;
	}


	public Float getEstimatedCost() {
		return estimatedCost;
	}


	public void setEstimatedCost(Float estimatedCost) {
		this.estimatedCost = estimatedCost;
	}


	public String getImmediateCause() {
		return immediateCause;
	}


	public void setImmediateCause(String immediateCause) {
		this.immediateCause = immediateCause;
	}


	public String getKeyPoints() {
		return keyPoints;
	}


	public void setKeyPoints(String keyPoints) {
		this.keyPoints = keyPoints;
	}


	public String getMainMessage() {
		return mainMessage;
	}


	public void setMainMessage(String mainMessage) {
		this.mainMessage = mainMessage;
	}


	public String getQuestion1Text() {
		return question1Text;
	}


	public void setQuestion1Text(String question1Text) {
		this.question1Text = question1Text;
	}


	public String getQuestion1Answer() {
		return question1Answer;
	}


	public void setQuestion1Answer(String question1Answer) {
		this.question1Answer = question1Answer;
	}


	public String getQuestion2Text() {
		return question2Text;
	}


	public void setQuestion2Text(String question2Text) {
		this.question2Text = question2Text;
	}


	public String getQuestion2Answer() {
		return question2Answer;
	}


	public void setQuestion2Answer(String question2Answer) {
		this.question2Answer = question2Answer;
	}


	public String getQuestion3Text() {
		return question3Text;
	}


	public void setQuestion3Text(String question3Text) {
		this.question3Text = question3Text;
	}


	public String getQuestion3Answer() {
		return question3Answer;
	}


	public void setQuestion3Answer(String question3Answer) {
		this.question3Answer = question3Answer;
	}


	public String getQuestion4Text() {
		return question4Text;
	}


	public void setQuestion4Text(String question4Text) {
		this.question4Text = question4Text;
	}


	public String getQuestion4Answer() {
		return question4Answer;
	}


	public void setQuestion4Answer(String question4Answer) {
		this.question4Answer = question4Answer;
	}


	public Long getCurrApproverId() {
		return currApproverId;
	}


	public void setCurrApproverId(Long currApproverId) {
		this.currApproverId = currApproverId;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public Date getCreatedAt() {
		return createdAt;
	}


	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}


	public String getCreatedBy() {
		return createdBy;
	}


	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}


	public Date getUpdatedAt() {
		return updatedAt;
	}


	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}


	public String getUpdatedBy() {
		return updatedBy;
	}


	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	

}
