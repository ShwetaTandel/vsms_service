package com.vantec.vsms.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "hazard")
public class Hazard implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "hazard_number")
	private Long hazardNumber;

	@Column(name = "site_id")
	private Long siteId;

	@Column(name = "location")
	private String location;

	@Column(name = "location_other")
	private String locationOther;

	@Column(name = "hazard_date")
	private Date hazardDateTime;

	@Column(name = "hazardDescription")
	private String hazardDescription;

	@Column(name = "have_done_anything_to_make_it_safe")
	private String haveDoneAnythingToMakeItSafe;

	@Column(name = "act_or_condition")
	private String actOrCondition;

	@Column(name = "risk_category")
	private String riskCategory;

	@Column(name = "hazard_comments")
	private String hazardComments;

	@Column(name = "hazard_actions")
	private String hazardActions;
	
	@Column(name = "further_action_needed")
	private String furtherActionNeeded;

	@Column(name = "what_is_done")
	private String whatIsDone;

	@Column(name = "why_not_done")
	private String whyNotDone;

	@Column(name = "any_witness")
	private String anyWitness;

	@Column(name = "witness_name")
	private String witnessName;

	@Column(name = "improvement_suggestions")
	private String improvementSuggestions;

	@Column(name = "reporter_name")
	private String reporterName;

	@Column(name = "reporter_department")
	private String reporterDepartment;

	@Column(name = "curr_approver_id")
	private Long currApproverId;

	@Column(name = "status")
	private String status;

	@Column(name = "created_at")
	private Date createdAt;

	@Column(name = "created_by")
	private String createdBy;

	@Column(name = "updated_at")
	private Date updatedAt;

	@Column(name = "updated_by")
	private String updatedBy;

	@Column(name = "creator_user_id")
	private Long creatorUserId;

	public Hazard() {
	}

	public String getActOrCondition() {
		return actOrCondition;
	}

	public void setActOrCondition(String actOrCondition) {
		this.actOrCondition = actOrCondition;
	}

	public String getRiskCategory() {
		return riskCategory;
	}

	public void setRiskCategory(String riskCategory) {
		this.riskCategory = riskCategory;
	}

	public String getHazardComments() {
		return hazardComments;
	}

	public void setHazardComments(String hazardComments) {
		this.hazardComments = hazardComments;
	}

	public String getHazardActions() {
		return hazardActions;
	}

	public void setHazardActions(String hazardActions) {
		this.hazardActions = hazardActions;
	}

	public String getFurtherActionNeeded() {
		return furtherActionNeeded;
	}

	public void setFurtherActionNeeded(String furtherActionNeeded) {
		this.furtherActionNeeded = furtherActionNeeded;
	}

	public Long getHazardNumber() {
		return hazardNumber;
	}

	public void setHazardNumber(Long hazardNumber) {
		this.hazardNumber = hazardNumber;
	}

	public Long getSiteId() {
		return siteId;
	}

	public void setSiteId(Long siteId) {
		this.siteId = siteId;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getLocationOther() {
		return locationOther;
	}

	public void setLocationOther(String locationOther) {
		this.locationOther = locationOther;
	}

	public Date getHazardDateTime() {
		return hazardDateTime;
	}

	public void setHazardDateTime(Date hazardDateTime) {
		this.hazardDateTime = hazardDateTime;
	}

	public String getHazardDescription() {
		return hazardDescription;
	}

	public void setHazardDescription(String hazardDescription) {
		this.hazardDescription = hazardDescription;
	}

	public String getHaveDoneAnythingToMakeItSafe() {
		return haveDoneAnythingToMakeItSafe;
	}

	public void setHaveDoneAnythingToMakeItSafe(String haveDoneAnythingToMakeItSafe) {
		this.haveDoneAnythingToMakeItSafe = haveDoneAnythingToMakeItSafe;
	}

	public String getWhatIsDone() {
		return whatIsDone;
	}

	public void setWhatIsDone(String whatIsDone) {
		this.whatIsDone = whatIsDone;
	}

	public String getWhyNotDone() {
		return whyNotDone;
	}

	public void setWhyNotDone(String whyNotDone) {
		this.whyNotDone = whyNotDone;
	}

	public String getAnyWitness() {
		return anyWitness;
	}

	public void setAnyWitness(String anyWitness) {
		this.anyWitness = anyWitness;
	}

	public String getWitnessName() {
		return witnessName;
	}

	public void setWitnessName(String witnessName) {
		this.witnessName = witnessName;
	}

	public String getImprovementSuggestions() {
		return improvementSuggestions;
	}

	public void setImprovementSuggestions(String improvementSuggestions) {
		this.improvementSuggestions = improvementSuggestions;
	}

	public String getReporterName() {
		return reporterName;
	}

	public void setReporterName(String reporterName) {
		this.reporterName = reporterName;
	}

	public String getReporterDepartment() {
		return reporterDepartment;
	}

	public void setReporterDepartment(String reporterDepartment) {
		this.reporterDepartment = reporterDepartment;
	}

	public Long getCurrApproverId() {
		return currApproverId;
	}

	public void setCurrApproverId(Long currApproverId) {
		this.currApproverId = currApproverId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Long getCreatorUserId() {
		return creatorUserId;
	}

	public void setCreatorUserId(Long creatorUserId) {
		this.creatorUserId = creatorUserId;
	}

}
