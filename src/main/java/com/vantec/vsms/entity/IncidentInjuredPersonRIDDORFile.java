package com.vantec.vsms.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;




@Entity
@Table(name = "incident_injured_person_riddor_files")
public class IncidentInjuredPersonRIDDORFile implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "incident_number")
	private Long incidentNumber;

	@Column(name = "injured_person_id")
	private Long injuredPersonId;
	
	@Column(name = "file_name")
	private String fileName;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIncidentNumber() {
		return incidentNumber;
	}

	public void setIncidentNumber(Long incidentNumber) {
		this.incidentNumber = incidentNumber;
	}

	

	public Long getInjuredPersonId() {
		return injuredPersonId;
	}

	public void setInjuredPersonId(Long injuredPersonId) {
		this.injuredPersonId = injuredPersonId;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
}
