package com.vantec.vsms.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "incident_injured_person_details")
public class IncidentInjuredPersonDetail implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long injuredPersonId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "incident_number", referencedColumnName = "incident_number")
	private Incident incident;
	
	@Column(name = "injured_person_name")
	private String injuredPersonName;

	@Column(name = "hse_reported_date")
	private Date hseReportedDate;
	
	@Column(name = "further_details_and_treatment")
	private String detailsAndTreatment;
	
	@Column(name = "isRIDDOR")
	private Boolean isRIDDOR;
	
	@Column(name = "days_lost")
	private Integer daysLost;
	
	@Column(name = "riddor_reference")
	private String riddorReference;
	
	
	@Column(name = "updated_at")
	private Date updatedAt;
	
	@Column(name = "updated_by")
	private String updatedBy;
	
	@Column(name = "updated_by_user_id")
	private Long updatedByUserId;
	
	public IncidentInjuredPersonDetail() {
	}

	public Incident getIncident() {
		return incident;
	}
	public Long getInjuredPersonId() {
		return injuredPersonId;
	}

	public void setInjuredPersonId(Long injuredPersonId) {
		this.injuredPersonId = injuredPersonId;
	}

	public String getInjuredPersonName() {
		return injuredPersonName;
	}

	public void setInjuredPersonName(String injuredPersonName) {
		this.injuredPersonName = injuredPersonName;
	}

	public Date getHseReportedDate() {
		return hseReportedDate;
	}

	public void setHseReportedDate(Date hseReportedDate) {
		this.hseReportedDate = hseReportedDate;
	}

	public String getDetailsAndTreatment() {
		return detailsAndTreatment;
	}

	public void setDetailsAndTreatment(String detailsAndTreatment) {
		this.detailsAndTreatment = detailsAndTreatment;
	}

	public Boolean getIsRIDDOR() {
		return isRIDDOR;
	}

	public void setIsRIDDOR(Boolean isRIDDOR) {
		this.isRIDDOR = isRIDDOR;
	}

	public Integer getDaysLost() {
		return daysLost;
	}

	public void setDaysLost(Integer daysLost) {
		this.daysLost = daysLost;
	}

	public String getRiddorReference() {
		return riddorReference;
	}

	public void setRiddorReference(String riddorReference) {
		this.riddorReference = riddorReference;
	}

	public void setIncident(Incident incident) {
		this.incident = incident;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Long getUpdatedByUserId() {
		return updatedByUserId;
	}

	public void setUpdatedByUserId(Long updatedByUserId) {
		this.updatedByUserId = updatedByUserId;
	}

	

}
