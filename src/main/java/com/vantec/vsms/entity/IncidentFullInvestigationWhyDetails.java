package com.vantec.vsms.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "incident_full_investigation_why_details")
public class IncidentFullInvestigationWhyDetails implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "incident_number", referencedColumnName = "incident_number")
	private Incident incident;
	
	
	@Column(name = "why_1")
	private String why1;
	
	@Column(name = "why_2")
	private String why2;
	
	@Column(name = "why_3")
	private String why3;
	
	@Column(name = "why_4")
	private String why4;

	@Column(name = "why_5")
	private String why5;
	
	@Column(name = "root_cause_a")
	private String rootCauseA;

	@Column(name = "root_cause_b")
	private String rootCauseB;
	
	@Column(name = "root_cause_c")
	private String rootCauseC;

	public IncidentFullInvestigationWhyDetails() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Incident getIncident() {
		return incident;
	}

	public void setIncident(Incident incident) {
		this.incident = incident;
	}

	public String getWhy1() {
		return why1;
	}

	public void setWhy1(String why1) {
		this.why1 = why1;
	}

	public String getWhy2() {
		return why2;
	}

	public void setWhy2(String why2) {
		this.why2 = why2;
	}

	public String getWhy3() {
		return why3;
	}

	public void setWhy3(String why3) {
		this.why3 = why3;
	}

	public String getWhy4() {
		return why4;
	}

	public void setWhy4(String why4) {
		this.why4 = why4;
	}

	public String getWhy5() {
		return why5;
	}

	public void setWhy5(String why5) {
		this.why5 = why5;
	}

	public String getRootCauseA() {
		return rootCauseA;
	}

	public void setRootCauseA(String rootCauseA) {
		this.rootCauseA = rootCauseA;
	}

	public String getRootCauseB() {
		return rootCauseB;
	}

	public void setRootCauseB(String rootCauseB) {
		this.rootCauseB = rootCauseB;
	}

	public String getRootCauseC() {
		return rootCauseC;
	}

	public void setRootCauseC(String rootCauseC) {
		this.rootCauseC = rootCauseC;
	}


}
