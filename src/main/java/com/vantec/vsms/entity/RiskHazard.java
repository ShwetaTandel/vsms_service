package com.vantec.vsms.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "risk_hazards")
public class RiskHazard implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "gra_id")
	private Long graId;
	

	@Column(name = "hazard_detail")
	private String hazardDetail;

	@Column(name = "persons_at_risk")
	private String personsAtRisk;

	@Column(name = "is_futher_actions_required")
	private Boolean  isFurtherActionRequired;
	
	@Column(name = "risk_rating_s")
	private Integer riskRatingS;
	
	@Column(name = "risk_rating_l")
	private Integer riskRatingL;
	@Column(name = "new_risk_rating_s")
	private Integer newRiskRatingS;
	@Column(name = "new_risk_rating_l")
	private Integer newRiskRatingL;

	@Column(name = "implemented_who")
	private String implementedWho;

	@Column(name = "implemented_when")
	private String implementedWhen;
	@Column(name = "existing_controls")
	private String existingControls;


	@Column(name = "created_at")
	private Date createdAt;

	@Column(name = "created_by")
	private String createdBy;

	@Column(name = "updated_at")
	private Date updatedAt;

	@Column(name = "updated_by")
	private String updatedBy;


	public RiskHazard() {
	}

	public Boolean getIsFurtherActionRequired() {
		return isFurtherActionRequired;
	}

	public void setIsFurtherActionRequired(Boolean isFurtherActionRequired) {
		this.isFurtherActionRequired = isFurtherActionRequired;
	}

	public String getExistingControls() {
		return existingControls;
	}

	public void setExistingControls(String existingControls) {
		this.existingControls = existingControls;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getGraId() {
		return graId;
	}

	public void setGraId(Long graId) {
		this.graId = graId;
	}


	public String getHazardDetail() {
		return hazardDetail;
	}

	public void setHazardDetail(String hazardDetail) {
		this.hazardDetail = hazardDetail;
	}

	public String getPersonsAtRisk() {
		return personsAtRisk;
	}

	public void setPersonsAtRisk(String personsAtRisk) {
		this.personsAtRisk = personsAtRisk;
	}

	
	public Integer getRiskRatingS() {
		return riskRatingS;
	}

	public void setRiskRatingS(Integer riskRatingS) {
		this.riskRatingS = riskRatingS;
	}

	public Integer getRiskRatingL() {
		return riskRatingL;
	}

	public void setRiskRatingL(Integer riskRatingL) {
		this.riskRatingL = riskRatingL;
	}

	public Integer getNewRiskRatingS() {
		return newRiskRatingS;
	}

	public void setNewRiskRatingS(Integer newRiskRatingS) {
		this.newRiskRatingS = newRiskRatingS;
	}

	public Integer getNewRiskRatingL() {
		return newRiskRatingL;
	}

	public void setNewRiskRatingL(Integer newRiskRatingL) {
		this.newRiskRatingL = newRiskRatingL;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getImplementedWho() {
		return implementedWho;
	}

	public void setImplementedWho(String implementedWho) {
		this.implementedWho = implementedWho;
	}

	public String getImplementedWhen() {
		return implementedWhen;
	}

	public void setImplementedWhen(String implementedWhen) {
		this.implementedWhen = implementedWhen;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}



}
