package com.vantec.vsms.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;




@Entity
@Table(name = "action_history")
public class ActionHistory implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "report_id")
	private Long reportId;
	
	@Column(name = "action_number")
	private Long actionNumber;

	
	@Column(name = "status")
	private String status;
	
	@Column(name = "comments")
	private String comments;

	@Column(name = "updated_at")
	private Date updatedAt;

	@Column(name = "updated_by")
	private String lastUpdatedBy;

	@Column(name = "updated_by_user_id")
	private Long updatedByUserId;
	
	@Column(name = "action_owner_id")
	private Long actionOwnerId;

	@Column(name = "action_source")
	private String actionSource;
	
	public ActionHistory() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getActionOwnerId() {
		return actionOwnerId;
	}

	public void setActionOwnerId(Long actionOwnerId) {
		this.actionOwnerId = actionOwnerId;
	}



	
	public Long getReportId() {
		return reportId;
	}

	public void setReportId(Long reportId) {
		this.reportId = reportId;
	}

	public String getActionSource() {
		return actionSource;
	}

	public void setActionSource(String actionSource) {
		this.actionSource = actionSource;
	}

	public Long getActionNumber() {
		return actionNumber;
	}

	public void setActionNumber(Long actionNumber) {
		this.actionNumber = actionNumber;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Long getUpdatedByUserId() {
		return updatedByUserId;
	}

	public void setUpdatedByUserId(Long updatedByUserId) {
		this.updatedByUserId = updatedByUserId;
	}


}
