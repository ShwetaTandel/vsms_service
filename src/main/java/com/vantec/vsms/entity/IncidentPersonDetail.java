package com.vantec.vsms.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "incident_person_details")
public class IncidentPersonDetail implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long personDetailId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "incident_number", referencedColumnName = "incident_number")
	private Incident incident;
	
	@Column(name = "person_name")
	private String personName;

	@Column(name = "person_dob")
	private Date personDOB;
	
	@Column(name = "employment_start_date")
	private Date employmentstartDate;
	
	@Column(name = "time_on_job_start_date")
	private Date timeOnJobStartDate;

	
	@Column(name = "employment_status")
	private String employmentStatus;
	
	@Column(name = "any_previous_incidents")
	private Boolean anyPrevIncidents;
	
	@Column(name = "prev_incident_date_reference")
	private String prevIncidentDateReference;
	
	@Column(name = "is_vehicle_involved")
	private Boolean isVehicleInvolved;
	
	@Column(name = "vehicle_type")
	private String vehicleType;
	
	@Column(name = "vehicle_type_other")
	private String vehicleTypeOther;
	
	@Column(name = "vehicle_reg_no")
	private String vehicleRegNo;
	
	@Column(name = "was_person_removed_from_truck")
	private Boolean wasPersonremovedFromTruck;
	
	@Column(name = "license_exp_date")
	private Date licenseExpDate;
	
	@Column(name = "is_licencse_valid")
	private Boolean isLicenseValid;

	@Column(name = "is_injury_involved")
	private Boolean isInjuryInvolved;

	
	@Column(name = "location_of_injury")
	private String locationOfInjury;
	
	@Column(name = "treatment_administered")
	private String treatmentAdministered;
	
	@Column(name = "ppe_worn")
	private String ppeWorn;
	
	@Column(name = "was_ppe_req_level")
	private Boolean wasPPEOfReqLevel;
	
	@Column(name = "treatment_details")
	private String treatementDetails;
	
	@Column(name = "first_aider_name")
	private String firstAiderName;
	
	@Column(name = "da_test_required")
	private Boolean daTestRequired;
	
	@Column(name = "da_test_not_req_reason")
	private String daTestNotReqReason;

	
	@Column(name = "da_test_agreed")
	private Boolean daTestAgreed;
	
	@Column(name = "da_tested_by")
	private String daTestedBy;
	
	@Column(name = "da_passed")
	private Boolean daPassed;
	
	@Column(name = "updated_at")
	private Date updatedAt;
	
	@Column(name = "updated_by")
	private String updatedBy;
	
	@Column(name = "updated_by_user_id")
	private Long updatedByUserId;
	
	public IncidentPersonDetail() {
	}

	public Incident getIncident() {
		return incident;
	}
	

	public String getVehicleTypeOther() {
		return vehicleTypeOther;
	}

	public void setVehicleTypeOther(String vehicleTypeOther) {
		this.vehicleTypeOther = vehicleTypeOther;
	}

	public String getDaTestNotReqReason() {
		return daTestNotReqReason;
	}

	public void setDaTestNotReqReason(String daTestNotReqReason) {
		this.daTestNotReqReason = daTestNotReqReason;
	}

	public Boolean getDaTestAgreed() {
		return daTestAgreed;
	}

	public void setDaTestAgreed(Boolean daTestAgreed) {
		this.daTestAgreed = daTestAgreed;
	}

	public String getDaTestedBy() {
		return daTestedBy;
	}

	public void setDaTestedBy(String daTestedBy) {
		this.daTestedBy = daTestedBy;
	}

	public Boolean getIsInjuryInvolved() {
		return isInjuryInvolved;
	}

	public void setIsInjuryInvolved(Boolean isInjuryInvolved) {
		this.isInjuryInvolved = isInjuryInvolved;
	}

	public void setIncident(Incident incident) {
		this.incident = incident;
	}

	public Long getPersonDetailId() {
		return personDetailId;
	}

	public void setPersonDetailId(Long personDetailId) {
		this.personDetailId = personDetailId;
	}

	public String getPersonName() {
		return personName;
	}

	public void setPersonName(String personName) {
		this.personName = personName;
	}

	public Date getPersonDOB() {
		return personDOB;
	}

	public void setPersonDOB(Date personDOB) {
		this.personDOB = personDOB;
	}

	public Date getEmploymentstartDate() {
		return employmentstartDate;
	}

	public void setEmploymentstartDate(Date employmentstartDate) {
		this.employmentstartDate = employmentstartDate;
	}

	public Date getTimeOnJobStartDate() {
		return timeOnJobStartDate;
	}

	public void setTimeOnJobStartDate(Date timeOnJobStartDate) {
		this.timeOnJobStartDate = timeOnJobStartDate;
	}

	public String getEmploymentStatus() {
		return employmentStatus;
	}

	public void setEmploymentStatus(String employmentStatus) {
		this.employmentStatus = employmentStatus;
	}

	public Boolean getAnyPrevIncidents() {
		return anyPrevIncidents;
	}

	public void setAnyPrevIncidents(Boolean anyPrevIncidents) {
		this.anyPrevIncidents = anyPrevIncidents;
	}

	public String getPrevIncidentDateReference() {
		return prevIncidentDateReference;
	}

	public void setPrevIncidentDateReference(String prevIncidentDateReference) {
		this.prevIncidentDateReference = prevIncidentDateReference;
	}

	public Boolean getIsVehicleInvolved() {
		return isVehicleInvolved;
	}

	public void setIsVehicleInvolved(Boolean isVehicleInvolved) {
		this.isVehicleInvolved = isVehicleInvolved;
	}

	public String getVehicleType() {
		return vehicleType;
	}

	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}

	public String getVehicleRegNo() {
		return vehicleRegNo;
	}

	public void setVehicleRegNo(String vehicleRegNo) {
		this.vehicleRegNo = vehicleRegNo;
	}

	public Boolean getWasPersonremovedFromTruck() {
		return wasPersonremovedFromTruck;
	}

	public void setWasPersonremovedFromTruck(Boolean wasPersonremovedFromTruck) {
		this.wasPersonremovedFromTruck = wasPersonremovedFromTruck;
	}

	public Date getLicenseExpDate() {
		return licenseExpDate;
	}

	public void setLicenseExpDate(Date licenseExpDate) {
		this.licenseExpDate = licenseExpDate;
	}

	public Boolean getIsLicenseValid() {
		return isLicenseValid;
	}

	public void setIsLicenseValid(Boolean isLicenseValid) {
		this.isLicenseValid = isLicenseValid;
	}

	public String getLocationOfInjury() {
		return locationOfInjury;
	}

	public void setLocationOfInjury(String locationOfInjury) {
		this.locationOfInjury = locationOfInjury;
	}

	public String getTreatmentAdministered() {
		return treatmentAdministered;
	}

	public void setTreatmentAdministered(String treatmentAdministered) {
		this.treatmentAdministered = treatmentAdministered;
	}

	public String getPpeWorn() {
		return ppeWorn;
	}

	public void setPpeWorn(String ppeWorn) {
		this.ppeWorn = ppeWorn;
	}

	public Boolean getWasPPEOfReqLevel() {
		return wasPPEOfReqLevel;
	}

	public void setWasPPEOfReqLevel(Boolean wasPPEOfReqLevel) {
		this.wasPPEOfReqLevel = wasPPEOfReqLevel;
	}

	public String getTreatementDetails() {
		return treatementDetails;
	}

	public void setTreatementDetails(String treatementDetails) {
		this.treatementDetails = treatementDetails;
	}

	public Boolean getDaTestRequired() {
		return daTestRequired;
	}

	public void setDaTestRequired(Boolean daTestRequired) {
		this.daTestRequired = daTestRequired;
	}

	public Boolean getDaPassed() {
		return daPassed;
	}

	public void setDaPassed(Boolean daPassed) {
		this.daPassed = daPassed;
	}

	
	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Long getUpdatedByUserId() {
		return updatedByUserId;
	}

	public void setUpdatedByUserId(Long updatedByUserId) {
		this.updatedByUserId = updatedByUserId;
	}

	public String getFirstAiderName() {
		return firstAiderName;
	}

	public void setFirstAiderName(String firstAiderName) {
		this.firstAiderName = firstAiderName;
	}

	

}
