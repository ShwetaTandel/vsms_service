import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class TestDeadlines {

	public static void main(String args[]) throws ParseException {

		final int SUN = 1, MON = 2, TUE = 3, WED = 4, THU = 5, FRI = 6, SAT = 7;
		Calendar incidentDate = Calendar.getInstance();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		;
		String dateInString = "2020-02-10T18:30:07";
		Date date = formatter.parse(dateInString);
		incidentDate.setTime(date);
		int dayOfWeek = incidentDate.get(Calendar.DAY_OF_WEEK);
		System.out.println("Incident date time is " + incidentDate.getTime());
		switch (dayOfWeek) {
		case SUN:
			incidentDate.add(Calendar.HOUR_OF_DAY, 8);
			System.out.println("SUBMIT reminder after " + incidentDate.getTime()); //8
			incidentDate.add(Calendar.HOUR_OF_DAY, 16);
			System.out.println("SUBMIT escalation after " + incidentDate.getTime());//24
			incidentDate.add(Calendar.HOUR_OF_DAY, 12);
			System.out.println("FI submit reminder  after " + incidentDate.getTime());//36
			incidentDate.add(Calendar.HOUR_OF_DAY, 36);
			System.out.println("FI submit escalation is " + incidentDate.getTime());//72
			incidentDate.add(Calendar.HOUR_OF_DAY, 24);
			System.out.println("GM approval reminder after " + incidentDate.getTime());//96
			incidentDate.add(Calendar.HOUR_OF_DAY, 72+24+24);
			System.out.println("GM approval  escalation  after  " + incidentDate.getTime());//168
			break;
		case MON:
			incidentDate.add(Calendar.HOUR_OF_DAY, 8);
			System.out.println("SUBMIT reminder after" + incidentDate.getTime()); //8
			incidentDate.add(Calendar.HOUR_OF_DAY, 16);
			System.out.println("SUBMIT escalation after " + incidentDate.getTime());//24
			incidentDate.add(Calendar.HOUR_OF_DAY, 12);
			System.out.println("FI submit reminder  after " + incidentDate.getTime());//36
			incidentDate.add(Calendar.HOUR_OF_DAY, 36);
			System.out.println("FI submit escalation is " + incidentDate.getTime());//72
			incidentDate.add(Calendar.HOUR_OF_DAY, 24);
			System.out.println("GM approval reminder after " + incidentDate.getTime());//96
			incidentDate.add(Calendar.HOUR_OF_DAY, 72+24+24);
			System.out.println("GM approval  escalation  after  " + incidentDate.getTime());//168
			break;
		case TUE:
			incidentDate.add(Calendar.HOUR_OF_DAY, 8);
			System.out.println("SUBMIT reminder after" + incidentDate.getTime()); //8
			incidentDate.add(Calendar.HOUR_OF_DAY, 16);
			System.out.println("SUBMIT escalation after " + incidentDate.getTime());//24
			incidentDate.add(Calendar.HOUR_OF_DAY, 12);
			System.out.println("FI submit reminder  after " + incidentDate.getTime());//36
			incidentDate.add(Calendar.HOUR_OF_DAY, 36);
			System.out.println("FI submit escalation is " + incidentDate.getTime());//72
			incidentDate.add(Calendar.HOUR_OF_DAY, 24+24+24);
			System.out.println("GM approval reminder after " + incidentDate.getTime());//96
			incidentDate.add(Calendar.HOUR_OF_DAY, 72);
			System.out.println("GM approval  escalation  after  " + incidentDate.getTime());//168
			break;

		case WED:
			incidentDate.add(Calendar.HOUR_OF_DAY, 8);
			System.out.println("SUBMIT reminder after" + incidentDate.getTime()); //8
			incidentDate.add(Calendar.HOUR_OF_DAY, 16);
			System.out.println("SUBMIT escalation after " + incidentDate.getTime());//24
			incidentDate.add(Calendar.HOUR_OF_DAY, 12);
			System.out.println("FI submit reminder  after " + incidentDate.getTime());//36
			incidentDate.add(Calendar.HOUR_OF_DAY, 36+24+24);
			System.out.println("FI submit escalation is " + incidentDate.getTime());//72
			incidentDate.add(Calendar.HOUR_OF_DAY, 24);
			System.out.println("GM approval reminder after " + incidentDate.getTime());//96
			incidentDate.add(Calendar.HOUR_OF_DAY, 72);
			System.out.println("GM approval  escalation  after  " + incidentDate.getTime());//168
			break;

		case THU:
			incidentDate.add(Calendar.HOUR_OF_DAY, 8);
			System.out.println("SUBMIT reminder after " + incidentDate.getTime()); //8
			incidentDate.add(Calendar.HOUR_OF_DAY, 16);
			System.out.println("SUBMIT escalation after " + incidentDate.getTime());//24
			incidentDate.add(Calendar.HOUR_OF_DAY, 12+24+24);
			System.out.println("FI submit reminder  after " + incidentDate.getTime());//36
			incidentDate.add(Calendar.HOUR_OF_DAY, 36);
			System.out.println("FI submit escalation is " + incidentDate.getTime());//72
			incidentDate.add(Calendar.HOUR_OF_DAY, 24);
			System.out.println("GM approval reminder after " + incidentDate.getTime());//96
			incidentDate.add(Calendar.HOUR_OF_DAY, 72+24+24);
			System.out.println("GM approval  escalation  after  " + incidentDate.getTime());//168
			break;

		case FRI:
			incidentDate.add(Calendar.HOUR_OF_DAY, 8);
			System.out.println("SUBMIT reminder after " + incidentDate.getTime()); //8
			incidentDate.add(Calendar.HOUR_OF_DAY, 16+24+24);
			System.out.println("SUBMIT escalation after " + incidentDate.getTime());//24
			incidentDate.add(Calendar.HOUR_OF_DAY, 12);
			System.out.println("FI submit reminder  after " + incidentDate.getTime());//36
			incidentDate.add(Calendar.HOUR_OF_DAY, 36);
			System.out.println("FI submit escalation is " + incidentDate.getTime());//72
			incidentDate.add(Calendar.HOUR_OF_DAY, 24);
			System.out.println("GM approval reminder after " + incidentDate.getTime());//96
			incidentDate.add(Calendar.HOUR_OF_DAY, 72+24+24);
			System.out.println("GM approval  escalation  after  " + incidentDate.getTime());//168
			break;
		case SAT:
			incidentDate.add(Calendar.HOUR_OF_DAY, 8);
			System.out.println("SUBMIT reminder after " + incidentDate.getTime()); //8
			incidentDate.add(Calendar.HOUR_OF_DAY, 16+24);
			System.out.println("SUBMIT escalation after " + incidentDate.getTime());//24
			incidentDate.add(Calendar.HOUR_OF_DAY, 12);
			System.out.println("FI submit reminder  after " + incidentDate.getTime());//36
			incidentDate.add(Calendar.HOUR_OF_DAY, 36);
			System.out.println("FI submit escalation is " + incidentDate.getTime());//72
			incidentDate.add(Calendar.HOUR_OF_DAY, 24);
			System.out.println("GM approval reminder after " + incidentDate.getTime());//96
			incidentDate.add(Calendar.HOUR_OF_DAY, 72+24+24);
			System.out.println("GM approval  escalation  after  " + incidentDate.getTime());//168
			break;

		}

	}

}
